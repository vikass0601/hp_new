<?php
error_reporting(E_ALL);
require_once 'core/init.php';
// include "functions/function.php";
if( !empty( Input::get('action') )){

    switch ( Input::get('action') ) {
        case 'login':
            if ( Input::exists() ) {
                  //  if( Token::check( Input::get( 'token' ) ) ) {
                 
                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                        'username' => array('require' =>true),
                                        'password'=> array( 'require' => true )));
                                    
                                    if( $validation->passed() ) {
                                        //log user in
                                        $user = new User();
                                        $login = $user->login( Input::get( 'username' ), Input::get( 'password' ) );
                                        if( $login ) {  echo 1; } else { echo "Sorry Login failed"; }
                                    } else {
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                    }
                   // }
            
            }                                
            break;
            
//        case 'logout' :
//            if ( Input::exists() ) {
//
//                                       $user = new User();
//                                       if( $user->logout()){
//                                           echo 1;
//                                          
//                                       } else { echo 0;}
//                                      
//                                }
//            break;
        case 'register'  :
                        if ( Input::exists() ) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    'username' => array(
                                                    'require' => true,
                                                    'unique' => 'users1'
                                                     ),
                                                    'password' => array(
                                                        'require' => true,
                                                        'matches' =>'password_again'
                                                     ), 
                                                    'password_again' => array(
                                                        'require' => true
                                                    ),
                                                    'fname' => array(
                                                       'require' => true 
                                                     ),
                                                    'lname' => array(
                                                       'require' => true 
                                                     ),
                                                    'contact' => array(
                                                        'require' =>true  
                                                    ),
//                                                    'address' => array(
//                                                         'require' =>true  
//                                                    ),
                                                    'add_street' => array(
                                                       'require' => true 
                                                     ),
                                                     'add_area' => array(
                                                       'require' => true 
                                                     ),
                                                     'add_landmark' => array(
                                                       'require' => true 
                                                     ),
//                                                     'add_city' => array(
//                                                       'require' => true 
//                                                     ),
                                                    'add_zipcode' => array(
                                                       'require' => true 
                                                     ),
                                                    'gender' => array(
                                                         'require' =>true
                                                    ),
                                                    'dob'=> array(
                                                         'require' =>true
                                                  )
                                                    
                                    )); 

                                   if( true == $validation->passed()){
                                       
                                       $user = new User();
                                       $salt = Hash::salt(32);
                                       $fullName = Input::get('fname'). " ".Input::get('mname')." ".Input::get('lname');
                                       try {
                                       $user_status = $user->createUser( array( 
                                                   'username' => Input::get('username'),
                                                   'password' => Hash::make(Input::get('password'), $salt),
                                                    'salt' => $salt,
                                                    'name' => $fullName,
                                                    'joined' => date( 'Y-m-d H:i:s' ),
                                                    'group' => 1
                                                   ));
                                       
                                       $lastInserId = json_decode( $user->_db->getLastInsertId(), true );
                                       
                                       if( $user_status ){
                                                try{
                                                 $registeration_info = array( 
                                                    'login_id' => $lastInserId['id'],
                                                    'fname' => Input::get('fname'),
                                                    'mname' => Input::get('mname'),
                                                    'lname' => Input::get('lname'),
                                                    'contact' => Input::get('contact'),
                                                    'add_street' => Input::get( 'add_street' ),
                                                    'add_area' => Input::get( 'add_area' ),
                                                    'add_landmark' => Input::get( 'add_landmark' ),
                                                    'add_city' => Input::get( 'add_city' ),
                                                    'add_zipcode' => Input::get( 'add_zipcode' ),
                                                    //'address' => Input::get('address'),
                                                    'gender' => Input::get( 'gender' ),
                                                    'dob' => Input::get( 'dob' ),
                                                    'age' => Input::get('age'),
						                            'created_by' => 0,
                                                    'created_date' => date("Y-m-d h:i:s"),
                                                    'updated_by' => 0,//Session::get( Config::get( 'session/session_name' ) ),
                                                    'updated_date' => date("Y-m-d h:i:s")
                                                );
												$bResponse = registerUser( $registeration_info, $lastInserId['id'] );
												
                                                if( $bResponse ){
                                                    echo 1;
                                                } else {
													echo 0;
												} 
                                                 
                                                } catch ( Exception $e ){
                                                    echo $e
;                                                }
                                          
                                        }
                                   } catch ( Exception $e ){
                                        echo $e;
                                    
                                    } 
                                   } else {
                                     
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                   
                }//input exists ends here  
            break;
            
        case 'upd': $user = new User();
                    if (Input::exists()) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    'username' => array(
                                                        'require' => true
                                                       
                                                     ),
                                                    'password' => array(
                                                        'require' => true
                                                        

                                                    ),
                                                    'password_again' => array(
                                                        'require' => true
                                                    ),
                                                    'name' => array(
                                                       'require' => true 
                                                        
                                                    )
                                    )); 

                                   if( $validation->passed() ){
                                       $user = new User();
                                       try{
                                          $user_status = $user->updateUser( Input::get('id'), array( 
                                                   'username' => Input::get('username'),
                                                   'password' => Input::get('password'),
                                                   'name' => Input::get('name')
                                                   ));

                                            if( $user_status ){
                                               echo 1;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
                    
            
        break;
        
        case 'del': $user =new User();
                  if (Input::exists()) {
                                            $user = new User();
                                                try{
                                                   $user_status = $user->deleteUser( Input::get('id'));

                                                     if( $user_status ){
                                                        echo 1;
                                                     }

                                                } catch ( Exception $e ) {
                                                    echo $e;
                                                }
                    }
            break;
            
        case 'getuser': $user =new User();
                            if (Input::exists()) {
                                                      $user->_db->get('users1',array( 'id', '=', Input::get('id') ));
                                                      $data = ( array )$user->_db->results();// print_r($data);
                                                      echo json_encode($data[0]); 
                              }
        break;
           
        case 'deleteuser': $user =new User();
                           $html ='';
                  if (Input::exists()) {
                              $html =  "<div class='container'><table class='table table-bordered'>
                                        <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Password</th>
                                        <th>Name</th>
                                        </tr>";
                              $user->_db->fetchData( "SELECT * FROM users1 WHERE id=".Input::get('delete_id') );
                              $row = $user->_db->results();
                                for($i=0; $i< $user->_db->count();$i++ ) {

                                      $html.='<tr><td>'.$row[$i]['id'].'</td>
                                       <td>'.$row[$i]['username'].'</td>
                                       <td>'.$row[$i]['password'].'</td>
                                       <td>'.$row[$i]['name'].'</td>
                                       </table></div><div class="container"><p>';
                                      
                                }
               
        if( Input::get( 'delete_id' ) ) {                       
            $html .='<form method="post" id="delUser" action="userController.php">
                    <input type="hidden" name="id" value="'.Input::get('delete_id').'" />
                    <button class="btn btn-large btn-primary" type="button" name="btn-del" id="btn-del">
                    <i class="glyphicon glyphicon-trash"></i> &nbsp; YES</button>
                    <a href="user.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>
                    </form></p></div> ';
         } else {
             $html.='<a href="user.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp;</p></div> Back to index</a>';
         }
                                
          
         $html.='<script>
            $("#btn-del").click(function(){
                    var form=$("#delUser");
                    $.ajax({
                            type:"POST",
                            url:form.attr("action"),
                            data:form.serialize()+"&action=del",//only input
                            success: function(response){
                                if( response == 1 ){
                                    $(\'#msg\').html(\' <div class="alert alert-success"><strong>Success!</strong> record was deleted... </div>\');
                                    window.location = "user.php?page_no='. Input::get('page_no') .'";
                                   // $(\'#msg\').show();
                                } else {
                                    $(\'#msg\').html(\'<div class="alert alert-danger"><strong>Problem!</strong>\'+ response +\'... </div>\');
                                    //$(\'#msg\').show();

                                }
                            }
                            });
            });
            </script>';
         }
         echo $html;
                    
           break;   
           
        case 'pg':   $user = new User();
            $page_no = (!empty( Input::get('page_no') ))? Input::get('page_no'): 1;
            echo '<table class="table table-bordered table-responsive">
                    <tr>
                    <tr>
                    <th>#</th>
                    <th>User Name</th>
                    <th>Password</th>
                    <th>Name</th>
                    <th colspan="2" align="center">Actions</th>
                    </tr>';
                     $user->viewUsers($page_no);       
    
             break; 
         
        case 'viewprofile':
                             $user =new User();
                            if (Input::exists()) {
                                                      //$user->_db->get('users1',array( 'id', '=', Input::get('id') ));
                                                      $data = $user->_db->runQuery("
                                                              SELECT
                                                                   us.`fname`,
                                                                   us.mname,
                                                                   us.lname,
                                                                   us.`contact`,
                                                                   us.add_street,
                                                                   us.`add_area`,
                                                                   us.`add_landmark`,
                                                                   us.`add_city`,
                                                                   us.`add_zipcode`,
                                                                   us.`gender`,
                                                                   us.`dob`,
                                                                   us.`age`,
                                                                   us1.username
                                                             FROM
                                                                `users` us
                                                             LEFT JOIN
                                                                users1 us1 ON us1.id = us.login_id 
                                                             WHERE 
                                                                us.login_id =".Input::get('id')); 
                                                      //echo ($data);die;
                                                      echo json_encode($data[0]); 
                              }
        break;
        
         case 'updprofile':
                             $user =new User();
                            if (Input::exists()) {
                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
//                                                    'username' => array(
//                                                    'require' => true,
//                                                    'unique' => 'users1'
//                                                     ),
//                                                    'password' => array(
//                                                        'require' => true,
//                                                        'matches' =>'password_again'
//                                                     ), 
//                                                    'password_again' => array(
//                                                        'require' => true
//                                                    ),
                                                    'fname' => array(
                                                       'require' => true 
                                                     ),
                                                    'lname' => array(
                                                       'require' => true 
                                                     ),
                                                    'contact' => array(
                                                        'require' =>true  
                                                    ),
//                                                    'address' => array(
//                                                         'require' =>true  
//                                                    ),
                                                    'add_street' => array(
                                                       'require' => true 
                                                     ),
                                                     'add_area' => array(
                                                       'require' => true 
                                                     ),
                                                     'add_landmark' => array(
                                                       'require' => true 
                                                     ),
//                                                     'add_city' => array(
//                                                       'require' => true 
//                                                     ),
                                                    'add_zipcode' => array(
                                                       'require' => true 
                                                     ),
                                                    'gender' => array(
                                                         'require' =>true
                                                    ),
                                                    'dob'=> array(
                                                         'require' =>true
                                                  )
                                                    
                                    ));  
                              }
                              
                               if( $validation->passed() ){
                                       $user = new User();
                                       
                                           try{ 
                                                $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                                    
                                                 $update =  $user->updateUser( $data[0]['id'], array( 
                                                    'fname' => Input::get('fname'),
                                                    'mname' => Input::get('mname'),
                                                    'lname' => Input::get('lname'),
                                                    'contact' => Input::get('contact'),
                                                    'add_street' => Input::get( 'add_street' ),
                                                    'add_area' => Input::get( 'add_area' ),
                                                    'add_landmark' => Input::get( 'add_landmark' ),
                                                    'add_city' => Input::get( 'add_city' ),
                                                    'add_zipcode' => Input::get( 'add_zipcode' ),
                                                    //'address' => Input::get('address'),
                                                    'gender' => Input::get( 'gender' ),
                                                    'dob' => Input::get( 'dob' ),
                                                    'age' => Input::get('age'),
                                                    'updated_by' => Session::get( $user->_sessionName ),
                                                    'updated_date' => date("Y-m-d h:i:s")
                                                ));
                                                 
                                                if( $update ){
                                                    echo  1;
                                                } 
                                                 
                                                } catch ( Exception $e ){
                                                    echo $e
;                                                }
                                          
                                            }else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
        break;
        
        case 'addapp' : $app = new Appointment();

                    if (Input::exists()) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    'test' => array(
                                                        'require' => true
                                                     ),
                                                    'location' => array(
                                                        'require' => true
                                                    ),
                                                    'appointmentDate' => array(
                                                        'require' => true
                                                    ),
                                                    'appointmentTime' => array(
                                                       'require' => true 
                                                    )
                                    )); 

                                   if( $validation->passed() ){
                                    
                                       $user = new User();
                                       try{
                                          $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                         print_r(array( 
                                                   'user_id' => $data[0]['id'],
                                                   'loc_id' => Input::get('location'),
                                                   'lab_id' => "1",
                                                   'date' => Input::get( 'appointmentDate' ),
                                                   'slot_id' => "1",
                                                   'created_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'created_date' => date("Y-m-d h:i:s"),
                                                   'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'updated_date' => date("Y-m-d h:i:s"),
                                                   'is_deleted' => 0,
												   'status_id' => 1
                                                  ));
                                $test_status = $app->createAppointment( array( 
                                                   'user_id' => $data[0]['id'],
                                                   'loc_id' => Input::get('location'),
                                                   'lab_id' => "1",
                                                   'date' => Input::get( 'appointmentDate' ),
                                                   'slot_id' => "1",
                                                   'created_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'created_date' => date("Y-m-d h:i:s"),
                                                   'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'updated_date' => date("Y-m-d h:i:s"),
                                                   'is_deleted' => Session::get( Config::get( 'session/session_name' ) ),
												   'status_id' => 1
                                                  ));
												  
                                            if( $test_status ){
                                               $lastInserId = json_decode($app->_db->getLastInsertId(), true);
                                                
                                               $test_status = $app->createAppointmentTest(array( 
                                                   'appointment_id' => $lastInserId['id'],
                                                   'test_id' => Input::get('test'),
                                                   
                                                  ));
                                               
                                                if($test_status){
                                                    echo $lastInserId['id'];
                                                 }
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
        break;                        
        
        case 'appointHistory' :
                    if (Input::exists()) {
                        
                         $user = new User();
                          try{
                                   $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                          

                                $report_status = $user->loadAppointmentHistory( $data[0]['id'] );

                                /**
                                * Generation of history
                                */     
                                
                                 $report_html = '<table class="table table-bordered table-responsive">
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Lab Name</th>
                                                                    <th>Test Name</th>
                                                                    <th>Test Charge</th>
                                                                    <th>Date</th>
                                                                    <th>Status</th>
                                                                     <th colspan="2" align="center">Actions</th>
                                                               </tr>';
               
                                            if( true == !empty( $report_status )  ){
                              
                                              
                                               foreach( $report_status as $key => $value ) {
                                                         $report_html .= "<tr>";
                                                         $report_html .= '<td>'. $report_status[$key]['name'] .'</td>';
                                                         $report_html .= '<td>'. $report_status[$key]['lab_name'] .'</td>';    
                                                         $report_html .= '<td>'. $report_status[$key]['test_name'] .'</td>';
                                                         $report_html .= '<td>'. $report_status[$key]['test_charge'] .'</td>';
                                                         $report_html .= '<td>'. substr($report_status[$key]['date'], 0, -8) .'</td>';
                                                         $report_html .= '<td>'. $report_status[$key]['status'] .'</td>';
                                                         $report_html .= '<td align="center">'
                                                                 . '<a href="javascript:void(0);" class="js-editappointment" data-value="action=getapp&id='.$report_status[$key]['id']. '"><i class="glyphicon glyphicon-edit"></i></a>
                                                                        </td>';
                                                         $report_html .= "</tr>";
                                            }
                                            
                                               $report_html .= "</table>";
                                               echo $report_html;
                                            } else { 

                                               $report_html.= '<tr>
                                                                <td colspan="7" class="danger"> No Record Found .....     </td>
                                                              </tr>';
                                                $report_html.= '</table>';
                                            
                                              echo $report_html;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }
                                            
                        
                    }
             break;
        case 'addappother' : $app = new Appointment();
            
                     if (Input::exists()) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    'r_test' => array(
                                                        'require' => true
                                                     ),
                                                    'r_location' => array(
                                                        'require' => true
                                                    ),
                                                    'r_appointmentDate' => array(
                                                        'require' => true
                                                    ),
                                                    'pAppTime' => array(
                                                       'require' => true 
                                                    )
                                    )); 

                                   if( $validation->passed() ){
                                    
                                       $user = new User();
                                       try{
                                          $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                          
                                        $test_status = $app->createAppointment( array( 
                                                   'user_id' => $data[0]['id'],
                                                   'loc_id' => Input::get('r_location'),
                                                   'lab_id' => "1",
                                                   'date' => Input::get( 'appointmentDate' ),
                                                   'slot_id' => "1",
                                                   'created_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'created_date' => date("Y-m-d h:i:s"),
                                                   'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'updated_date' => date("Y-m-d h:i:s")
//                                                   'is_deleted' => NULL
                                        ));
                                            if( $test_status ){
                                                
                                                $lastInserId = json_decode($app->_db->getLastInsertId(), true);
                                               
                                                $test_status = $app->createAppointmentTest(array( 
                                                   'appointment_id' => $lastInserId['id'],
                                                   'test_id' => Input::get('r_test'),
                                                   
                                                  ));
//                                                echo "shjdghjsd".$test_status;die;
                                                if( $test_status ){
                                                         $r_reltion = Input::get('pRelation');
                                                         $r_name = Input::get('pName');
                                                         $r_email = Input::get('pEmail');
                                                         $r_phone = Input::get('pContact');
                                                         $r_gender = Input::get('pGender');
                                                         $r_doctor = Input::get('pDocName');
//                                                         $r_address = Input::get('pAddress');
                                                         $add_street = Input::get('add_street');
                                                         $add_area = Input::get('add_area');
                                                         $add_landmark = Input::get('add_landmark');
                                                         $add_city = Input::get('add_city');
                                                         $add_zipcode = Input::get('add_zipcode');
                                                         $r_address = $add_street.' '.$add_area.' '.$add_landmark.' '.$add_city.' '.$add_zipcode;
                                                         $r_dob = Input::get('pDob');
                                                         $r_age = Input::get('pAge');                                               
                                                         $created_by = Session::get( Config::get( 'session/session_name' ) );
                                                         $created_date = date("Y-m-d h:i:s");
                                                         $updated_by = Session::get( Config::get( 'session/session_name' ) );
                                                         $updated_date = date("Y-m-d h:i:s");
                                                         $is_deleted = 0;

                                                  $sql = "INSERT INTO 
                                                                    `relations`(`id`, `appointment_id`, `relation`, `r_name`, `r_email`,
                                                                                `r_phone`, `r_gender`, `r_doctor`, `r_address`, `add_street`,
                                                                                `add_area`, `add_landmark`, `add_city`, `add_zipcode`, `r_dob`,
                                                                                `r_age`, `created_by`, `created_date`, `updted_by`,
                                                                                `updated_date`, `is_valid`) 
                                                          VALUES (NULL,'{$lastInserId['id']}','{$r_reltion}','{$r_name}','{$r_email}','{$r_phone}',
                                                                  '{$r_gender}','{$r_doctor}','{$r_address}','{$add_street}','{$add_area}',
                                                                  '{$add_landmark}','{$add_city}','{$add_zipcode}','{$r_dob}','{$r_age}','{$created_by}',
                                                                  '{$created_date}','{$updated_by}','{$updated_date}','{$is_deleted}')";
                                                
                                                 $result = $app->_db->runQuery( $sql );
//                                                  print_r($result);die;
                                                    echo $lastInserId['id'];
                                               }
                                             
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
                    
        break; 
		         
		
		
        default: 
        break;
    
}
}





