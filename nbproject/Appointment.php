<?php
error_reporting(E_ALL);
require_once 'core/init.php';
class Appointment {

    public $_db;
    
    public function __construct() {
      $this->_db = DB::getInstnace();    
    }
    
    public function createAppointment( $fields = array() ) {
        if( !$this->_db->insert( 'appointments', $fields ) ){
            throw new Exception( $this->_db->error );
            return false;
        }
        
        return true;
    }

    public function createAppointmentTest( $fields = array() ) {

        if( !$this->_db->insert( 'appointment_tests', $fields ) ){
            throw new Exception( 'problem in inserting' );
            return false;
        }
        
        return true;
    }
    
    public function createAppointmentOther($fields = array()){
        if( !$this->_db->insert( 'relation', $fields ) ){
            throw new Exception( 'problem in inserting' );
            return false;
        }
        
        return true;
    }


    public function updateAppointment( $id ,$fields = array() ) {
        if( !$this->_db->update( 'appointments', $id, $fields ) ) {
                throw new Exception('problem in update');
                return false;
        }
        
        return true;
        
    }
    
    public function deleteAppointment( $id ){
        if( !$this->_db->delete( 'appointments', array( 'id','=',$id ) ) ) {
            throw new Exception(' in deletion of record.');
                return false;
        }
        return true;
    }

}

