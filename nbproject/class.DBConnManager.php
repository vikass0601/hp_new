<?php

/*!
 * @class DBConnManager
 * This class manager will establish an connection and offer you an connection.
 * It will also take care of closing the connection.
 * Desing on : 21-4-2015
 * Last update: 21-04-2015
 *
 */

/* $aConnStack variable for maintaining the list of open connection in an array*/
global $aConnStack;
$aConnStack = array();

/*
* CDBConnManager class for creating Mysql DB connection 
*/
class DBConnManager {
    // $sDBHost: holding host name (like:localhost)
    private $sDBHost;
	// $sDBUser: holding DataBase User name (like:root)
    private $sDBUser;
    /* $rDBConns: holding the DB Connection Object, 
    $iDBConnCount: holding the total number of open connection*/
	private $sDBPass,$sDBName,$rDBConns,$iDBConnCount;

    /*! constructor to establish the connection. If a database name is passed,
     * it will connect to that database, otherwise it connects to default database
     * (Defaults values are from DBConfig file)*/
    function __construct($sDBName = NULL)
    {
        if($sDBName === NULL){
            $sDBName = DEFAULT_DATABASE;
        }
        $this->sDBHost = DATABASE_HOST;
        $this->sDBUser = DATABASE_USER;
        $this->sDBPass = DATABASE_PASS;
        $this->sDBName = $sDBName;
        $this->rDBConns = array();
        $this->iDBConnCount = 0;
    }

    //! It will return the connection instance
    /*
    * Funtion create an Mysql DB connection via mysqli class
    */
    function getConnInstance()
    {
        global $aConnStack;

        //! If we don't have a connection available in connection stack, create a new one and give it
        
        if(count($aConnStack)==0){
             $this->rDBConns[$this->iDBConnCount] = new mysqli($this->sDBHost, $this->sDBUser, $this->sDBPass, $this->sDBName);
            if($this->rDBConns[$this->iDBConnCount]->connect_error){
                die($this->rDBConns[$this->iDBConnCount]->connect_error);
            }
            /*
            * Counter incresed to determine no of connection open. 
            */
            $this->iDBConnCount++;

            /*var_dump("Connection Created");
            var_dump(count($aConnStack));
            //var_dump('Conn Count: '.$this->iDBConnCount);
            */


            /*
            * $this->iDBConnCount-1 : because connection stored in arrary, hence require index value i.e. 0-for refering first connection
            */
            return $this->rDBConns[$this->iDBConnCount-1];    
            
        }
        else {
            //var_dump("Connection Reused");

            //! if we have a connection available, reuse it

            $existingConn = array_pop($aConnStack);
            $this->rDBConns[$this->iDBConnCount] = $existingConn;
            $this->iDBConnCount++;
            return $existingConn;
        }       
    }

    //! close all the connection when class is destructed
    function  __destruct()
    {
        //var_dump("Connection Destroyed");
        global $aConnStack;
        for($ii = 0; $ii< $this->iDBConnCount; $ii++){
            array_push($aConnStack, $this->rDBConns[$ii]);
        }    
    }
}

?>