<?php

class Categories {

    public $_db;
    
    public function __construct() {
      $this->_db = DB::getInstnace();    
    }
    
    public function createCategory( $fields = array() ) {

        if( !$this->_db->insert( 'test_categories', $fields ) ){
            throw new Exception( 'problem in inserting' );
            return false;
        }
        
        return true;
    }
    
    public function updateCategory( $id ,$fields = array() ) {
        if( !$this->_db->update( 'test_categories', $id, $fields ) ) {
                throw new Exception('problem in update');
                return false;
        }
        
        return true;
        
    }
    
    public function deleteCategory( $id ){
        if( !$this->_db->delete( 'test_categories', array( 'id','=',$id ) ) ) {
            throw new Exception(' in deletion of record.');
                return false;
        }
        return true;
    }
    
    public function find( $user = null ){
        if( $user ) {
            $field = ( is_numeric ( $user ) ) ? 'id' : 'email_id';
            $data = $this->_db->get( 'test_categories', array( $field , '=', $user ));
            
            if( $data->count() ){
                return $data->results();
            }
            return false;
        }
    }
    
    public function viewCategories($page_no) {
                $query = "SELECT * FROM test_categories";       
		$records_per_page=10;
                $newquery = $this->_db->paging("SELECT * FROM test_categories", $records_per_page, $page_no);
                $this->_db->fetchData($newquery);
                if($this->_db->count()>0)
		{
                $row = $this->_db->results();
		    for($i=0; $i< $this->_db->count();$i++ )
			{
                           	?>
                <tr>
                <td><?php print($row[$i]['id']); ?></td>
                <td><?php print($row[$i]['category_name']); ?></td>
               
                <td align="center">
                <a href="javascript:void(0);" class="js-edituser" data-value="action=getcategory&id=<?php print($row[$i]['id']); ?>" data-page_no="<?php echo ((!empty( $page_no ))?$page_no:1);?>"><i class="glyphicon glyphicon-edit"></i></a>
                </td>
                <td align="center">
                <a href="javascript:void(0)"  class="js-deleteuser" data-value="delete_id=<?php print($row[$i]['id']); ?>&action=deletecategory&page_no=<?php echo ((!empty( $page_no ))?$page_no:1);?>"><i class="glyphicon glyphicon-remove-circle"></i></a>
                </td>
                </tr>
                <?php
			}
		}
		else
		{
			?>
            <tr>
            <td>Nothing here...</td>
            </tr>
            <?php
		}

              
    } 
    
    public function pagingLink( $sql, $records_per_page = 1,$page_no = '', $redirect ){
        $self = $redirect;
        $this->_db->fetchData($sql);
	$total_no_of_records = $this->_db->count();
        if($total_no_of_records > 0)
		{
			?><ul class="pagination"><?php
			$total_no_of_pages=ceil($total_no_of_records/$records_per_page);
			$current_page=1;
			if(!empty( $page_no ))
			{
				$current_page= $page_no;
			}
			if($current_page!=1)
			{
				$previous =$current_page-1;
				echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=1' class='js-pageDisplay'>First</a></li>";
				echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$previous."' class='js-pageDisplay'>Previous</a></li>";
			}
			for($i=1;$i<=$total_no_of_pages;$i++)
			{
				if($i==$current_page)
				{
					echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$i."' style='color:red;' class='js-pageDisplay'>".$i."</a></li>";
				}
				else
				{
					echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$i."' class='js-pageDisplay'>".$i."</a></li>";
				}
			}
			if($current_page!=$total_no_of_pages)
			{
				$next=$current_page+1;
				echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$next."' class='js-pageDisplay'>Next</a></li>";
				echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$total_no_of_pages."' class='js-pageDisplay'>Last</a></li>";
			}
			?></ul><?php
		}
    }
    

}

