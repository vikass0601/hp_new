<?php
require_once 'core/init.php';
$user = new User();
//var_dump( Token::check( Input::get('token') ) );

?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div id="msg" class="container"></div>

<div class="container" id="div_add">
<a href="javascript:void(0);" class="btn btn-large btn-info" id="add"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add Records</a>
</div>

<div class="clearfix"></div><br />
<!--Main div where content get loaded-->
<div class="container" id="loadUser" name="loadUser">

</div>    
<!--Paging div will get content soon-->
<div class="container" id="pagination">
    <table class='table table-bordered table-responsive'>
    <tr>
        <td colspan="7" align="center">
            <div class="pagination-wrap">
                <?php 
                    $page_no = '';
                    $query = "SELECT * FROM users1";       
                    $records_per_page=3;
                    $redirect = "userController.php";
                    $user->pagingLink( $query, $records_per_page, $page_no ,$redirect );//$crud->paginglink($query,$records_per_page); 
                ?>
            </div>
        </td>
    </tr>
    </table>    
</div>

   
       

<!--Add user start from here-->
<div class="container" id="addUser" name="addUser">

<form method='post' id="form_add_user" action="userController.php">
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Username</td>
            <input type='hidden' name='id' id="id" class='form-control' value="">
            <td><input type='text' name='username' id='username' class='form-control' value=""required ></td>
        </tr>
 
        <tr>
            <td>Password</td>
            <td><input type='password' name='password' id='password' class='form-control' value="" required></td>
        </tr>
 
        <tr>
            <td>Password Again</td>
            <td><input type='password' name='password_again' id='password_again' class='form-control' value="" required></td>
        </tr>
 
        <tr>
            <td>Your Name</td>
            <td><input type='text' name='name' id='name' class='form-control' value="" required></td>
        <input type='hidden' name='token' id='token' class='form-control' value="<?php echo Token::generate();?>" >
        </tr>
 
        <tr>
       
        <td colspan="2">
            <div id="submituser">
               <button type="button" class="btn btn-primary" name="btn-save" id="submit">
                   <span class="glyphicon glyphicon-plus"></span> Create New Record
               </button>  
               <a href="user.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to index</a>
            </div>
            <div id="edituser">
                <button type="button" class="btn btn-primary" name="btn-update" id="btn-update">
    			<span class="glyphicon glyphicon-edit"></span>  Update this Record
                </button>
                <a href="user.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </div> 
        
        </td>
            
         
         
        </tr>
 
    </table>
</form>
</div>
<!--Add user ends here-->
<?php include_once 'footer.php'; ?>
<script>
    $(document).ready(function(){
       var page_no = 1; 
     $("#addUser").hide();
     $("#div_add").show();
     
       $.ajax({
                type:"POST",
                url:'userController.php',
                data:"action=pg",//only input
                success: function(response){
                    $("#loadUser").html(response); 
                }
            });
     
     
    /*************************
     * To add new user
     */
    $("#add").click( function() {
        $("#loadUser").hide(); $("#pagination").hide();
        $("#addUser").show();
        $("#div_add").hide();
        $("#edituser").hide();
        $('#form_add_user').trigger("reset");
        $("#edituser").hide();
    }); 
    /***************************
     * Submit new user data
     */      
    $("#submit").on( "click", function(){
        var form=$("#form_add_user");
        $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+"&action=add",//only input
                success: function(response){
                    if( response == 1 ){
                        $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });
        });
    
    
    /********
    * For updation of user to load current user content into form
     */
     $(document).on("click", ".js-edituser", function(){
         var $this = $(this);
         page_no = $this.data('page_no');
        
       $.ajax({
                type:"POST",
                url:'userController.php',
                data:$this.data('value'),//only input
                success: function(response){
                    var obj = $.parseJSON( response );
                    $("#id").val(obj.id);
                    $("#username").val(obj.username);
                    $("#password").val(obj.password);
                    $("#password_again").val(obj.password_again);
                    $("#name").val(obj.name);
                    $("#addUser").show();
                    $("#submituser").hide();
                    $("#edituser").show();
                    $("#loadUser").hide(); $("#pagination").hide();
                    $("#add").hide();
                    
                    
                }
            });
        
    });
    
    /******
    * Actual updation of content starts here
     */
    $("#btn-update").on("click", function(){
            var form=$("#form_add_user");
            $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize()+"&action=upd",//only input
                    success: function(response){
                        if( response == 1 ){
                            $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was updated successfully <a href="user.php?page_no='+ page_no +'">HOME</a>!</div>');
                            $('#msg').show();
                        } else {
                            $('#msg').html("<div class='alert alert-warning'><strong>SORRY!</strong> ERROR while updating record !</div>");
                            $('#msg').show();

                        }
                    }
            });
        });
    /******
    * updateis done here
     */
     
     /******
    * deletion is start from here 
     */ 
     $(document).on("click",".js-deleteuser", function(){
            $("#add").hide();
            $("#pagination").hide();
            $('#msg').html('<div class="alert alert-danger"><strong>Sure !</strong> to remove the following record ? </div>');
            var $this = $(this);
            page_no = $this.data('page_no');
                $.ajax({
                         type:"POST",
                         url:'userController.php',
                         data:$this.data('value'),//only input
                         success: function(response){
                             $("#loadUser").html(response);                  
                            }
                });

    });
    
    $(".js-pageDisplay").on("click",function(){
        $('.js-pageDisplay').removeClass("redColor");
        $(this).addClass("redColor");
        var $this = $(this);
         $.ajax({
                type:"POST",
                url:$this.data('url'),
                data:$this.data('param'),//only input
                success: function(response){
                     $("#loadUser").html(response);                  
                   }
            });
    });
 });
</script>
    