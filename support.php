<?php
include('base-header.php');
include('page-header.php');
?>
        <div class="classTopHeading">
        <div class="container">
            <div class="row">
            <div class="col-lg-12">
                <h1 class="">Support</h1>
            </div>
        </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
        <div id="page-wrappera" class="container classMTopMargin">
            
            <!-- /.row -->
          
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Stay Connected</h3>
                      <!-- <a class="twitter-timeline"  href="https://twitter.com/laxmikant" data-widget-id="363657744307343360">Tweets by @laxmikant</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
           -->
           <a class="twitter-timeline" href="https://twitter.com/HealthPleaseOff" data-widget-id="616325476097765376">Tweets by @HealthPleaseOff</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">What Is Health Please</h3>
                    <a class="embedly-card" href="http://labmedicineblog.com/">A blog for medical laboratory professionals</a>
<script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Ask your Query</h3>
                    
                    <div class="classSFormHolder">
                        <img src="images/3.jpg" width="100%"/>
                        <div class="classSupportForm">
                            <form id="idSupportPanal" class="classSupportPanal" action="" method="GET">
                              
                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                       <input type="text" class="form-control" id="idName" name="user[name]" placeholder="Full name*" required>           
                               </div> 
                              </div>

                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                       <input type="email" class="form-control" id="idEmail" name="user[email]" placeholder="User name - Email*" required>           
                               </div> 
                              </div>
                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-comment "></i></span>
                                       <textarea class="form-control" id="idQuery" name="user[query]" placeholder="Your Query*" style="height: 150px;" required> </textarea>
                               </div> 
                              </div>
                              <div class="row">
                                            
                                 <div class="col-lg-4">
                                    <button type="submit" class="classBtnSubmit btn btn-info" id="idSupportSubmit">Send</button>
                                 </div>
                              </div>  

                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div><br/><br/>
            <!-- /.row -->
        <!-- </div>
        
        </div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>
