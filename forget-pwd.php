<?php
include('base-header.php');
include('page-header.php');
logged_in_user_redirect();
require_once 'core/init.php';
?>
<div class="row">
  <div class="col-md-offset-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <form method='post' id="form_fgt_user_pwd" action="userController.php">
      <div id="msg_pwd"></div> 
      <input type="hidden" id="hide" value="">
      
        <div class="row">
            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label class="control-label" for="age">Email - id</label>
            <input type='text' name='fpwd_username' id='fpwd_username' class='form-control classProfileCustomControl' placeholder="Please enter your email address" value="" data="">
          </div>
        </div>
      <div id="pwd_update_section"></div>
       
        <div class="row">
            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <strong>*</strong> Fields are mandatory.
          </div>
        </div>
        <div class="row">
            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="edituser" class="">
              <button type="button" class="btn btn-primary" name="btn_fgt_pwd" id="btn_fgt_pwd">
                <span class="glyphicon glyphicon-edit"></span> Recover            
              </button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="index.php"  data-dismiss="modal" class="btn btn-large btn-danger"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </div> 
          </div>
        </div>
  </form>
</div>
</div>
<script> 
    $("#btn_fgt_pwd").on( "click", function() { 
        var form = $("#form_fgt_user_pwd");
        if( ''== $("#hide").val()) {
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:"action=recover&email="+$('#fpwd_username').val(),//only input
                success: function(response){
                    if( response == 1 ){
                        $('#msg_pwd').html("<div class='alert alert-danger'><strong>SORRY!</strong> couldn't find this email address </div>");
                        $('#msg_pwd').show();
                        form.reset();
                        //window.location.href = window.location.href;
                    } else {
                        $('#pwd_update_section').html( response );
                    }
                }
           });

        } 
    });
</script>
<?php
include('page-footer.php');
include('base-footer.php');
?>