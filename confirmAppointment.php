<?php
include('base-header.php');
include('page-header.php');
$aAllCat = getAllCategories();
$aAllTests = getAllTests();
$aAllLoc = getAllLocations();
error_reporting(E_ALL);
require_once 'core/init.php';
?>
<?php
  $user = new User();
  $data = $user->_db->runQuery("SELECT
                                       *,
                                       CONCAT_WS( '', fname,' ', mname, ' ', lname) as name
                                  FROM
                                    `users` 
                                  WHERE 
                                    users.login_id =".Session::get( $user->_sessionName ));
  
  $data1 = $user->_db->runQuery("SELECT
                                       *
                                  FROM
                                    `users1` 
                                  WHERE 
                                    users1.id =".Session::get( $user->_sessionName )); 
?>       



        
        <div class="classSliderHolder">
            <div class="container">
                
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                          <div class="radio classOSRBut" id="idOSRHolder">
                            <label><input type="radio" id="idBookYourself" name="bookApp" checked>Book Yourself</label>
                          </div>
                    </div>      
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                          <div class="radio classORBut" id="idORHolder">
                            <label><input type="radio" id="idBookOther" name="bookApp">Book for other</label>
                          </div>                
                    </div>                     
                </div>    
                <div class="row" id="idDivBookYourself">
                  <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12">
                      <?php include_once"bookAppYourself.php"; ?>
                  </div>    
                </div>
                <div class="row" id="idDivBookOther">
                    <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12">
                      <?php include_once"bookAppOther.php"; ?>
                    </div>  
                </div>
               
            </div>
        </div>
        
        <div id="page-wrappera" class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Click To Request A Check Up</h3>
                    <a href="#"><img src="images/1.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Importance Of Check Ups</h3>
                    <a href="#"><img src="images/2.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Learn More</h3>
                    <a href="#"><img src="images/3.jpg" width="100%"/></a>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <br/><br/><br/>
            <!-- /.row -->             
        </div>
        <!-- /#page-wrapper -->

<script>
 

   // $("#idAppDate").datepicker({
   //           dateFormat: "yy-mm-dd",
   //          maxDate: 0,
   //          changeMonth: true, 
   //          changeYear: true
   //          /*yearRange: '1900:' + new Date().getFullYear()*/
   //        });
      $("#pAppDate").datepicker({
       format: 'dd-mm-yyyy',
        startDate: '+1d',
        minDate:'1',
        maxDate:'+2m',
        viewMode: "months", 
        changeMonth: true,
         autoclose: true
    });

   $("#idAppDate").datepicker({
       format: 'dd-mm-yyyy',
        startDate: '+1d',
        minDate:'1',
        maxDate:'+2m',
        viewMode: "months", 
        changeMonth: true,
         autoclose: true
    });


 function generateDate( date ) {
            var tempDate = date.split( '/' );
            return tempDate[2] + '-' + tempDate[0] + '-' + tempDate[1];
          }
               $('#pDob').change(function(){
                   var tempAge = $(this).val();
                   var dob = new Date( tempAge );
                   var today = new Date();
                   var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                   $('#pAge').val(age);

              });   

               $('#dob').change(function(){
                   var tempAge = $(this).val();
                   var dob = new Date( tempAge );
                   var today = new Date();
                   var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                   alert(age);
                   $('#age').attr('value','');
                   $('#age').attr('value',age);

              });  

            $('#idSpanDob').click(function(){
                $(this).closest('.input-group').find('.hasDatepicker').focus();
            }).css('cursor','pointer');

              

             $(document).ready(function(){

             $("#idDivBookOther").addClass("hidden");   
             });  

             $("#idBookOther").click(function(){
                      $("#idDivBookYourself").addClass("hidden");
                      $("#idDivBookOther").removeClass("hidden");
                  // $("#idRelationDiv").html("<td>Relation</td><td><input type='text' name='relation' id='relation' class='form-control' value='' required></td>");
                  // $("#idUserName").html("");
                  // $("#idUserName").html("<td>Patient Name</td><td><input type='text' name='patientName' id='patientName' class='form-control' value='' required></td>");
             });  

             $("#idBookYourself").click(function(){
                      $( "#idDivBookYourself" ).removeClass( "hidden" );
                      $( "#idDivBookOther" ).addClass( "hidden" );
                   // $("#idRelationDiv").html("");
                   // $("#idUserName").html(" <td>Your Name</td><td><input type='text' name='name' id='name' class='form-control' value='<?php echo $data[0]['name']; ?>' required></td>");
             });

             $("#idBookAppYour").click( function() {
                 var appointmentForm = $("#formBookAppYour");
                 var appointment_date = generateDate( $( "#idAppDate" ).val() );
                 /*
                  * Validation of form goes here
                  */
                  var form = $("#formBookAppYour");
                  if ( false == form.valid() ) exit;
	
                 
                  $.ajax({
                        type:"POST",
                        url:appointmentForm.attr( "action" ),
                        data:appointmentForm.serialize()+"&appointmentDate="+ appointment_date +"&action=addapp",//only input
                        success: function(response){
        
                if( response ){
                    
                    //            $("#form_add_app").trigger("reset");
                               // window.location = 'thanks-appointment-done.php?appid='+response;
                            } else {
                                alert('try again!');
                            }
                        }
                    });  
            });
            //to generate date for database table
             function generateDate( date ) {
                 var tempDate = date.split( '/' );
                 return tempDate[2] + '-' + tempDate[0] + '-' + tempDate[1];
             } 

             $("#idBookAppOther").click(function(){
                 var appointmentForm=$("#formBookAppOther");
                 //to generate date for database table
                 var appointment_date = generateDate( $( "#app_date" ).val() );
                 
                 /*
                  * Validation of form goes here
                  */
                  var form = $("#formBookAppOther");
                  if ( false == form.valid() ) return;
	
                 
                  $.ajax({
                        type:"POST",
                        url:appointmentForm.attr("action"),
                        data:appointmentForm.serialize()+"&appointmentDate="+ appointment_date +"&action=addappother",//only input
                        success: function(response){
                            if( response ){
                                //$("#form_add_app").trigger("reset");
                                 window.location = 'thanks-appointment-done.php?appid='+response;
                            } else {
                                alert('try again!');
                            }
                        }
                    });  
            });
</script>
<?php
include('page-footer.php');
include('base-footer.php');
?>
