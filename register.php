<?php
require_once 'core/init.php';
$user = new User();
//var_dump(Token::check(Input::get('token')));

 ?>
<?php include_once 'loginHeader.php'; ?>
<div id="msg" class="container"></div>
<div class="container" id="addUser" name="addUser">

<form method='post' id="form_add_user" action="userController.php">
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Username or Email Id</td>
            <input type='hidden' name='id' id="id" class='form-control' value="">
            <td><input type='text' name='username' id='username' class='form-control' value=""required ></td>
        </tr>
 
        <tr>
            <td>Password</td>
            <td><input type='password' name='password' id='password' class='form-control' value="" required></td>
        </tr>
 
        <tr>
            <td>Password Again</td>
            <td><input type='password' name='password_again' id='password_again' class='form-control' value="" required></td>
        </tr>
 
        <tr>
            <td>Your Name</td>
            <td><input type='text' name='name' id='name' class='form-control' value="" required></td>
        <input type='hidden' name='token' id='token' class='form-control' value="<?php echo Token::generate();?>" >
        </tr>
        
        <tr>
            <td>Contact No</td>
            <td><input type='number' name='contact' id='contact' class='form-control' value="" required></td>
        </tr>
        
        <tr>
            <td>Address</td>
            <td> <textarea class="form-control" rows="5" id="address" name="address"></textarea></td>
        </tr>
        
        <tr>
            <td>Gender</td>
            <td>
             <div class="radio">
                 <label><input type="radio" name="gender" value="1" checked="checked">Male</label>
                <label><input type="radio" name="gender" value="0">Female</label>
             </div>
        </tr>
        
        <tr>
            <td>DOB</td>
            <td>
            <div class='input-group date' id='datetimepicker1'>
                    <input type='text' name="dob" id="dob"  class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>    
            </td>
        </tr>
        
        <tr>
            <td>Age</td>
            <td><input type='number' name='age' id='age' class='form-control' value="" required></td>
        </tr>
        
        <tr>
       
        <td colspan="2">
           
            <div id="edituser">
                <button type="submit" class="btn btn-primary" name="btn-update" id="register" name="register">
    			<span class="glyphicon glyphicon-edit"></span>  Register
                </button>
                <a href="login.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </div> 
        
        </td>
            
         
         
        </tr>
 
    </table>
</form>
</div>
<?php include_once 'footer.php'; ?>
<script>
    $(document).ready(function(){
   
    /***************************
     * Submit new user data
     */      
    $("#register").on( "click", function(){
        var form=$("#form_add_user");
        $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+'&action=signup',//only input
                success: function(response){
                    if( response == 1 ){
                      $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Registered successfully <a href="login.php">Login Now</a>!</div>');
                      $('#form_add_user').trigger("reset");
                      // window.location = 'lab.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });
        });
    
    
 });
</script>
