<?php

/*
Funtion get all category details
*/

function getAllCategories(){
	$oDBCon = new DB();  
	 $query = "SELECT * FROM test_categories";
	 $oDBCon->fetchData($query);
     if($oDBCon->count()>0)
		{
            $row = $oDBCon->results();
		    for($i=0; $i< $oDBCon->count();$i++ )
			{
				$oResult[$row[$i]['id']]=$row[$i]['category_name'];
			}
		}else{
			$oResult=false;
		}
         $oDBCon->closeConnection();       
	return $oResult;
}

/**
 * added by vikas to get status 
 */
function getAllStatus(){
	$oDBCon = new DB();  
	 $query = "SELECT * FROM appointment_status";
	 $oDBCon->fetchData($query);
     if($oDBCon->count()>0)
		{
            $row = $oDBCon->results();
		    for($i=0; $i< $oDBCon->count();$i++ )
			{	
				$oResult=$row;
			}
		}else{
			$oResult=false;
		}
        $oDBCon->closeConnection();
	return $oResult;
}


function getAllTests(){
	$oDBCon = new DB();  
	 $query = "SELECT * FROM tests";
	 $oDBCon->fetchData($query);
     if($oDBCon->count()>0)
		{
            $row = $oDBCon->results();
		    for($i=0; $i< $oDBCon->count();$i++ )
			{	
				$oResult=$row;
			}
		}else{
			$oResult=false;
		}
        $oDBCon->closeConnection();
	return $oResult;
}
function getAllLocations(){
	$oDBCon = new DB();  
	 $query = "SELECT * FROM locations";
	 $oDBCon->fetchData($query);
     if($oDBCon->count()>0)
		{
            $row = $oDBCon->results();
		    for($i=0; $i< $oDBCon->count();$i++ )
			{
				$oResult[$row[$i]['id']]=$row[$i]['location_name'];
			}
		}else{
			$oResult=false;
		}
        $oDBCon->closeConnection();
	return $oResult;
}

function getTestByCat($sCatId){
	$oDBCon = DB::getInstnace();  
	 $query = "SELECT * FROM test_categories";
	 fetchData($query);
     if($oDBCon->count()>0)
		{
            $row = $oDBCon->results();
		    for($i=0; $i< $oDBCon->count();$i++ )
			{
				$oResult[$row[$i]['id']]=$row[$i]['category_name'];
			}
		}else{
			$oResult=false;
		}
	return $oResult;
}




function getAllTestsById($testId){
	$oDBCon = new DB();  
	 $query = "SELECT * FROM tests WHERE id = {$testId}";
	 $oDBCon->fetchData($query);
     if($oDBCon->count()>0)
		{
            $row = $oDBCon->results();
		    for($i=0; $i< $oDBCon->count();$i++ )
			{	
				$oResult=$row;
			}
		}else{
			$oResult=false;
		}
	return $oResult;
}

function getAllTestsByIds( $testIds ){
	$oDBCon = new DB();  
	 $query = "SELECT * FROM tests WHERE id IN ( {$testIds} )";
	 $oDBCon->fetchData($query);
     if($oDBCon->count()>0)
		{
            $row = $oDBCon->results();
		    for($i=0; $i< $oDBCon->count();$i++ )
			{	
				$oResult=$row;
			}
		}else{
			$oResult=false;
		}
	return $oResult;
}

function getAllLocationsById($locId){
	$oDBCon = new DB();  
	 $query = "SELECT * FROM locations WHERE id = {$locId}";
	 $oDBCon->fetchData($query);
     if($oDBCon->count()>0)
		{
            $row = $oDBCon->results();
		    for($i=0; $i< $oDBCon->count();$i++ )
			{
				$oResult['location_name']=$row[0]['location_name'];
			}
		}else{
			$oResult=false;
		}
	return $oResult;

}


function registerUser($aUserData){

	$sql= "INSERT INTO `users1`(`id`, `username`, `password`, `salt`, `name`, `joined`, `group`) VALUES (NULL,'{$aUserData['username']}','{$aUserData['password']}','". $aUserData['salt'] ."', '{$aUserData['name']}','{$aUserData['joined']}','{$aUserData['group']}')";

	$oDBCon = new DB(); 
	$DBResponse = $oDBCon->getInstnace();
	$iResponse = $oDBCon->executeQuery($sql);

		if(!empty($iResponse))
		{

			$sql = "INSERT INTO `users`(`id`, `login_id`, `name`, `contact`, `address`, `gender`, `dob`, `age`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_deleted`) VALUES (NULL,'{$iResponse}','{$aUserData['name']}','{$aUserData['contact']}','{$aUserData['address']}','{$aUserData['gender']}','{$aUserData['dob']}','{$aUserData['age']}','{$aUserData['created_by']}','{$aUserData['created_date']}','{$aUserData['updated_by']}','{$aUserData['updated_date']}',0)";

			$iResponse = $oDBCon->executeQuery($sql);
			if($iResponse){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
}
function splitByDelimeter( $string, $delimeter ) {
    $myArray = explode( "$delimeter", $string );
    return $myArray;
}