<?php

    /*
     *  To sanitize data 
     * 
     * PLEASE DO NOT DELETE  escape() AS AT SOME PLACE WE HAVE USED THIS FUNCTION
     */

        function escape( $string ) {
            return htmlentities( strip_tags( $string ) , ENT_QUOTES, 'UTF-8');
        }

        function sanitize( $string ) {
            return htmlentities( strip_tags( $string ) , ENT_QUOTES, 'UTF-8');
        }

        function array_sanitize( &$item ) {
            $item = htmlentities( strip_tags( $item ) );
        }

        /*
         * Redirect user
         */
        function redirectTo( $page = 'index.php' ) {
                $host  = $_SERVER['HTTP_HOST'];
                $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');

                //echo "http://$host$uri/$extra";
                /*
                 * Redirect class is not working so temporarily using this 
                 * solution to redirect user
                 */
                header('Location:http://localhost/healthplease/index.php');
        //        echo '<script> window.location.href = "http://'.$host.$uri.'/'.$page.'"</script>';
                exit();
        }
        
        /*
         * To protect page if user is not logged in redirect itto 
         */
        function protect_page( $page = 'index.php' ) {
            $user = new User();
            if( false == $user->isLoggedIn() ){
                $host  = $_SERVER['HTTP_HOST'];
                $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                //$page = 'index.php';
                //echo "http://$host$uri/$extra";
                /*
                 * Redirect class is not working so temporarily using this 
                 * solution to redirect user
                 */
                echo '<script> window.location.href = "http://'.$host.$uri.'/'.$page.'"</script>';
                exit();
            }
        } 

        /*
         * redirect user if its logged_in eg: - sign-up.php should not get open if user is logged in
         */
        function logged_in_redirect( $page = 'index.php' ) {
            $user = new User();
            if( true == $user->isLoggedIn() ){
                $host  = $_SERVER['HTTP_HOST'];
                $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                //echo "http://$host$uri/$extra";
                /*
                 * Redirect class is not working so temporarily using this 
                 * solution to redirect user
                 */
                echo '<script> window.location.href = "http://'.$host.$uri.'/'.$page.'"</script>';
            }
        }

        function redirectUserIfPwdNotChanged() {
            $session = Session::exists( Config::get( 'session/session_name' ) );

               if( false == empty( $session ) ){
                $user = new User();
                $data = $user->_db->get( 'users1',array( 'id','=',Session::get( Config::get( 'session/session_name' ) ) ) );
                $user = $data->results();
                /*
                 * To get name of the current file called
                 */
                $current_file = explode( '/', $_SERVER['SCRIPT_NAME'] );
                $current_file = end( $current_file );

                /*
                 * If file is not general like below then redirect it to forget-pwd.php
                 */
                $fileToDislay = array('change-pwd-form.php', 'logout.php',  'contact.php', 'support.php', 'about.php');
                if( false == in_array( $current_file, $fileToDislay ) && 1 == $user[0]->password_recover ){
                    logged_in_redirect('change-pwd-form.php?force=1');
                    exit;
                }
              }
        }
?>