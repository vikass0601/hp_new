<?php
require_once 'core/init.php';
$user = new User();
//var_dump( Token::check( Input::get('token') ) );

?>
<?php include_once 'loginHeader.php'; ?>

<div class="clearfix"></div>

<div id="msg" class="container"></div>

<div class="clearfix"></div><br />
<!--Main div where content get loaded-->
   

<!--Add user start from here-->
<div class="container" id="addUser" name="addUser">

    <form method='post' id="form_add_user" action="userController.php">
     
        <table class='table table-bordered'>
     
            <tr>
                <td>Username</td>
                <td><input type='text' name='username' id='username' class='form-control' value=""required ></td>
            </tr>
     
            <tr>
                <td>Password</td>
                <td><input type='password' name='password' id='password' class='form-control' value="" required></td>
            </tr>
     
         
            <tr>
            <input type='hidden' name='token'  class='form-control' value="<?php echo Token::generate();?>" ></td>
            <td colspan="2">
                <div id="submituser">
                   <button type="button" class="btn btn-primary" name="btn-save" id="submit">
                       <span class="glyphicon glyphicon-plus"></span> Login
                   </button>  
                   <a href="register.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Register Me</a>
                </div>
                      
            </td>
                
             
             
            </tr>
     
        </table>
    </form>
</div>
<!--Add user ends here-->
<?php include_once 'footer.php'; ?>
<script>
    $(document).ready(function(){
   
    /***************************
     * Submit new user data
     */      
    $("#submit").on( "click", function(){
        var form=$("#form_add_user");
        $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+'&action=login',//only input
                success: function(response){
                    if( response == 1 ){
                       window.location = 'lab.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });
        });
    
    
 });
</script>
    