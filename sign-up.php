<?php
include('base-header.php');
include('page-header.php');
logged_in_redirect();
?>

    <!-- Signup form modal code start -->
    <div class="container" id="addUser" name="addUser">

      <div class="modala-header">
            <h1 class="modal-title text-left" id="exampleModalLabel">Signup</h1>
		        
      </div>
      <br/>
  	  <div class="error_container alert alert-danger">
  			<ol></ol>
  	  </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 control pull-right">
          <div id="msg"></div>
          <img src="images/sign-up.png" class="hidden-sm hidden-xs" style="width:80%;float:right;"/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 control"> 
          <div class="row">
            <form method='post' id="form_register" action="userController.php">
                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" id="idErrName">
                  <label class=" control-label" for="fname">First Name *</label>
                  <input type='text' name='fname' id='fname' class='form-control classCustomControl' placeholder="First Name" title="Please enter your First name !" data="">
                </div>

                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" id="">
                  <label class=" control-label" for="lname">Last Name *</label>
                  <input type='text' name='lname' id='lname' class='form-control classCustomControl' placeholder="Last Name" title="Please enter your Last name !" data=""></td>
                  <input type='hidden' name='mname' id='mname'  placeholder="Middle Name"class='form-control classCustomControl'>              
                </div>

                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12" id="">
                  <label class=" control-label" for="selectbasic">Username / Email *</label>
                  <input type='hidden' name='id' id="id" class='form-control classCustomControl' value="">
                  <div class="input-group"> <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div><input type='email' name='username' id='idUsername' placeholder="E-mail" class='form-control classCustomControl' title="Please enter your email address!" data=""></div>
                </div>
                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label class=" control-label" for="selectbasic">Mobile no. *</label>
                  <div  class="input-group"> <div class="input-group-addon">+91</div><input type='text' name='contact' id='contact' class='form-control classCustomControl' placeholder="Mobile Number" maxlength="10" size="10" title="Please provide Contact no !" data=""></div>
                </div>
                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label class=" control-label" for="selectbasic">Password *</label>
                  <input type='password' name='password' id='idPassword' class='form-control classCustomControl' placeholder="Password" title="Please enter your password !" data="">
                </div>
                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label class=" control-label" for="selectbasic">Confirm Password *</label>
                  <input type='password' name='password_again' id='password_again' class='form-control classCustomControl' placeholder="Confirm Password" title="Please enter password again field !" data="">
                </div>
                <div class="form-group col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                  <label class="control-label" for="add_street">Address *</label>
                  <div class="controls">                     
                      <input type="text" id="add_street" name="add_street" class="input-medium form-control classCustomControl" title="Please provide Street name !" name="add_street" data=""="" placeholder="Eg: Flat No, Bunglow No, Society name"/>
                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label class="control-label" for="add_landmark">Landmark*</label>
                  <div class="controls">
                    <input id="add_landmark" name="add_landmark" placeholder="Landmark" class="input-medium form-control classCustomControl" type="text">

                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label class="control-label" for="add_area">Area *</label>
                  <div class="controls">
                    <input id="add_area" name="add_area" placeholder="Eg: Viman Nagar, Kalynaninagar'" class="input-medium form-control classCustomControl" data=""="" title="Please provide area !" type="text">

                  </div>
                </div>

                <!-- Text input-->
                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label class="control-label" for="area_zipcode">Pincode *</label>
                  <div class="controls">
                      <input id="add_zipcode" name="add_zipcode" placeholder="Pincode" class="input-medium form-control classCustomControl" minlength="6" maxlength="6"  type="text" title="Please provide Pincode !" data="">

                  </div>
                </div>

                 <!-- Text input-->
                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label class="control-label" for="area_city">City *</label>
                  <div class="controls">
                      <input id="add_city" name="add_city" placeholder="City" class="input-medium form-control classCustomControl" data="" value="Pune" type="text" readonly="">

                  </div>
                </div>

                <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                   <label class="control-label" for="gender">Gender *</label>
                   <div class="radio">
                       <label><input type="radio" name="gender" value="M" checked="checked">Male</label>
                       <label> <input type="radio" name="gender" value="F">Female</label>
                   </div>
                 </div>

                 <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label class="control-label" for="area_zipcode">Date Of Birth *</label>
                    <div class='date controls' id='datetimepicker1'>
                      <div class="input-group "> <div class="input-group-addon "><i class="fa fa-calendar"></i></div><input type='text' name="dob" id="dob" placeholder="Date Of Birth" class="form-control classCustomControl classdob" /></div>
                    </div>
                 </div>
                 
                  <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label class="control-label" for="age">Age</label>
                    <input type='text' name='age' id='age' class='form-control classCustomControl' placeholder="Age" value="" data="" readonly>
                  </div>

                  <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <strong>*</strong> Fields are mandatory.
                  </div>

                  <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="edituser">
                      <button type="submit" class="btn btn-primary" name="btn-update" id="idRegisterSubmit" name="register">
                        <span class="glyphicon glyphicon-edit"></span> Register
                      </button>
                      <a href="index.php"  data-dismiss="modal" class="btn btn-large btn-danger"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
                    </div> 
                  </div>

            </form>
            
            <div class=""id="idLoaderDiv" style="display:none;" >
                <img src="images/loader.gif" id="idLoaderGif" class="classLoaderImg">
            </div>
          </div>
          <div class="modal-footer">
            <div class="form-group">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 control"> 
                      Already had a account
                  <a class="btn btn-info btn-xs loginid" id="" href="#"  data-keyboard="false" data-backdrop="false">
                  <!-- <a class="btn btn-info btn-xs" id="loginid" href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"> -->
                      Login
                  </a>
                 
              </div>
            </div>    
          </div>
        </div>

      </div>
    </div>
<!-- SignUp modal form code end-->

<script type="text/javascript">
 function generateDate( date ) {
            var tempDate = date.split( '/' );
            return tempDate[2] + '-' + tempDate[0] + '-' + tempDate[1];
          }
               $('#dob').change(function(){
                   var tempAge = $(this).val();
                   var dob = new Date( tempAge );
                   var today = new Date();
                   var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                   $('#age').val(age);

              }); 


</script>
<?php
include('page-footer.php');
include('base-footer.php');
?>
