<?php
error_reporting(E_ALL);
require_once 'core/init.php';

$user = new User();

$user->logout();

Redirect::to('index.php');