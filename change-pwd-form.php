<?php

    $force = '';
    if( true == isset( $_GET['force'] ) ) {
        $force = $_GET['force'] ;
        if( 1 == $force ){

             include('base-header.php');
             include('page-header.php');
             /*
              * if user is not logged in then redirect it to index.php
              * for change-pwd-form.php?force=1
              */

             protect_page();
         }
    }
?>
<div class="row">
  <div class="col-md-offset-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <form method='post' id="form_upd_user_pwd" action="userController.php">
      <div id="msg_pwd"></div>  
     <?php 
        if( 1 == $force  ){
            echo '<div id="change_pwd_msg" class="form-group alert alert-danger margin-top10"><strong>Please</strong> change your password first!</div>';
        } 
      ?>
      
    <input type='hidden' name='token' id='token' class='form-control' value="<?php echo Token::generate();?>" >
      
        <div class="row">
            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label class="control-label" for="age">Current Password</label>
            <input type='password' name='c_pwd' id='c_pwd' class='form-control classProfileCustomControl' placeholder="Current password" value="" data="">
          </div>
        </div>
        <div class="row">
            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label class=" control-label" for="selectbasic">New Password *</label>
            <input type='password' name='password' id='idPassword' class='form-control classProfileCustomControl' placeholder="Password" title="Please enter your password !" data="">
          </div>
        </div>
        <div class="row">
            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <label class=" control-label" for="selectbasic">Confirm Password *</label>
            <input type='password' name='password_again' id='password_again' class='form-control classProfileCustomControl' placeholder="Confirm Password" title="Please enter password again field !" data="">
          </div>
        </div>
        <div class="row">
            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <strong>*</strong> Fields are mandatory.
          </div>
        </div>
        <div class="row">
            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="edituser" class="">
              <button type="button" class="btn btn-primary" name="btn-upd-pwd" id="btn-upd-pwd">
                <span class="glyphicon glyphicon-edit"></span> Change Password            
              </button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="user-profile.php"  data-dismiss="modal" class="btn btn-large btn-danger"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </div> 
          </div>
        </div>
  </form>
</div>
</div>
<script> 
    $("#btn-upd-pwd").on( "click", function() { 
    var form = $("#form_upd_user_pwd");
         $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize()+"&action=null",//only input
                    success: function(response){
                        $('#change_pwd_msg').hide();
                        if( response == 1 ){
                            $('#msg_pwd').html('<div class="alert alert-info margin-top10"><strong>Your Password was updated successfully</strong> <a href="index.php">HOME</a>!</div>');
                            $('#msg_pwd').show();
                            form.reset();
                            //window.location.href = window.location.href;
                        } else {
                            $('#msg_pwd').html("<div class='alert alert-danger margin-top10'><strong>SORRY!</strong> ERROR while updating record !<br>"+ response  +"</div>");
                            $('#msg_pwd').show();
                            //$('#btn-upd').html('<span class="glyphicon glyphicon-edit"></span> Update');
                        }
                    }
            });
    });
</script>    
<?php
    if( 1 == $force ){
        include('page-footer.php');
        include('base-footer.php');
    }
?>