<?php
        include('base-header.php');
        include('page-header.php');

        $id = $_REQUEST['appid'];
        $user = new User();
        
        // To check wether appointment detail come from relations table or not  
        $strSql = 'SELECT count(*) as cnt FROM relations where appointment_id ='.$id;
        $Count = $user->_db->runQuery( $strSql );
       
       // if its 0 then it come from appointment
        if( 0 == $Count[0]['cnt'] ) {
        $strSql = 'SELECT 
                        app.id,
                        app.date,
                        ts.test_name,
                        ts.test_charge,
                        lb.lab_name,
                        CONCAT_WS( "", fname," ", mname, " ", lname) as name,
                        us.contact,
                        CONCAT_WS( "", us.add_street," ", us.add_landmark, " ", us.add_city, " ", us.add_zipcode) as address,
                        usr.username
                    FROM
                        `appointments` app
                    INNER JOIN 
                   	appointment_tests apt ON app.id = apt.appointment_id
                    INNER JOIN 
                   	tests ts ON ts.id = apt.test_id
                    INNER JOIN
                        users1 usr ON app.user_id = usr.id 
                    INNER JOIN
                        locations lc ON app.loc_id = lc.id 
                    INNER JOIN 
                        labs lb ON app.lab_id = lb.id 
                    LEFT JOIN 
                        users us ON us.login_id = usr.id
                   Where app.id = '. ( int )$id;
        
        $reportData = $user->_db->runQuery( $strSql );
       } else {
           $strSql = 'SELECT 
                        app.id,
                        app.date,
                        ts.test_name,
                        ts.test_charge,
                        lb.lab_name,
                        rs.r_name as name,
                        rs.r_phone as contact,
                        rs.r_address as address,
                        usr.username
                    FROM
                        `appointments` app
                    INNER JOIN 
                        appointment_tests apt ON app.id = apt.appointment_id
                    INNER JOIN 
                        tests ts ON ts.id = apt.test_id
                    INNER JOIN
                        users1 usr ON app.user_id = usr.id 
                    INNER JOIN
                        locations lc ON app.loc_id = lc.id 
                    INNER JOIN 
                        labs lb ON app.lab_id = lb.id 
                    LEFT JOIN 
                        relations rs ON rs.appointment_id = app.id
                   Where app.id = '. ( int )$id;
        
        $reportData = $user->_db->runQuery( $strSql );
           
       }
//       print_r($reportData);die;
      /**
       * to send email content to confirm_mail so that user have a confirmaion on his/her side
       */
       
       $emailContent = json_encode( $reportData, true );
       

//$reportData = 
?>
       <div class="classTopHeading">
        <div class="container">
           
            <div class="row">
                 <div class="col-md-12 alert alert-success"><strong>Congratulations, Your Appointment is booked! Our convenience team will reach on the requested date.</strong></div>
               
            <div class="col-lg-12">
                <!-- <div class="alert alert-info"><strong>!Confirmation Mail will send you shortly.</strong></div> -->
                <div class="" id="addUser" name="addUser">
                    <table class='table table-bordered'>
                        <tr>
                            <td colspan="2">Appointment Details</td>
                        </tr>
                        <tr>
                            <td>Appointment Id</td>
                            <td><?php echo $reportData[0]['id'];?></td>
                        </tr>
                        <tr>
                            <td>Test Information</td><td>
                        <?php  foreach ( $reportData as $key => $value ) {  ?>
                        <?php  echo ($key + 1) . ") " . $reportData[$key]['test_name']. " || rupees " . $reportData[$key]['test_charge']; echo "<br/>"; }?>  
                        </td>
                        </tr>
<!--                        <tr>
                            <td>Test Charge</td>
                            <td><?php //echo $reportData[0]['test_charge'];?></td>
                        </tr>-->
                         <tr>
                            <td>Appointment Date</td>
                            <td><?php echo substr( $reportData[0]['date'], 0, -8);?></td>
                        </tr>
                         <tr>
                            <td>Appointment Time</td>
                            <td> Between 7:00 am to 12:00pm </td>
                          </tr>
                        <tr>
                            <td>Lab Name</td>
                            <td><?php echo $reportData[0]['lab_name'];?></td>
                        </tr>
                        <tr>
                            <td>Name of patient</td>
                            <td><?php echo $reportData[0]['name'];?></td>
                        </tr>
                        <tr>
                            <td>Contact No</td>
                            <td><?php echo $reportData[0]['contact'];?></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><?php echo $reportData[0]['address'];?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
         <div class="row">
               
               
                <div class="col-md-12 alert alert-info">
                    At Healthplease your convenience is our priority. We will not only make sure that you get the services right but we will provide them with great comfort.<br/>
                    <br/>
                    We wish you a happy time here at Healthplease! In case you need help feel free to write us at <a href="mailto:help@healthplease.com" target="_blank">help@healthplease.com</a> or call us at <a href="tel:+91 8600080252" target="_blank">+91 8600080252</a>. We will be glad to help you.
                </div>
            </div>
            <!-- /.col-lg-12 -->
        
    </div>
        
    </div>
        <div id="page-wrappera" class="container">
            
            <!-- /.row -->
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="classHighLightBox">
                        
                    </div>
                </div>
            </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">We Are In Support</h3>
                    <div class="classSFormHolder">
                        <img src="images/1.jpg" width="100%"/>
                        <div class="classSupportForm">
                            <div class="classLabDetailWrapper">
                                <div class="classLabName"> Dhanwantary Path Lab </div>
                                <div class="classLabDetails">
                                    Dr. S.S Jagalpure  M.D. (Path) <br/>
                                    Geeta Complex Kasturaba Hgs Soc, Shop no-3, Behind Jakat Naka, Vishrantwadi Pune - 411015                               
                                </div>
                            </div>             

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Importance Of Check Ups</h3>
                    <a href="#"><img src="images/2.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Ask your Query</h3>
                    
                    <div class="classSFormHolder">
                        <img src="images/3.jpg" width="100%"/>
                        <div class="classSupportForm">
                            <form id="idSupportPanal" class="classSupportPanal" action="" method="GET">
                              
                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                       <input type="text" class="form-control" id="idName" name="user[name]" placeholder="Full name*" required>           
                               </div> 
                              </div>

                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                       <input type="email" class="form-control" id="idEmail" name="user[email]" placeholder="User name - Email*" required>           
                               </div> 
                              </div>
                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-comment "></i></span>
                                       <textarea class="form-control" id="idQuery" name="user[query]" placeholder="Your Query*" style="height: 150px;" required> </textarea>
                               </div> 
                              </div>
                              <div class="row">
                                            
                                 <div class="col-lg-4">
                                    <button type="submit" class="classBtnSubmit btn btn-info" id="idSupportSubmit">Send</button>
                                 </div>
                              </div>  

                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div><br/><br/>
            <!-- /.row -->
        <!-- </div>
        
        </div>
        <!-- /#page-wrapper -->
        <script type="text/javascript">
            $(document).ready(function(){
                       $.ajax({
                                url: "confirm_mail.php",
                                type: "POST",
                                data: {
                                    content : JSON.stringify( <?php echo $emailContent; ?> ),            
                                },
                                success: function(response){
                                                        if( response ){
                                                            alert("Appointment Booked.");
                                                              // window.location.href = 'index.php';
                                                        } else {
                                                        }
                                                }
                                });
            });
        </script>


<?php
include('page-footer.php');
include('base-footer.php');
?>
