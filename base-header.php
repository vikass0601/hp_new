<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HealthPlease | Your Convenience, Our Priority</title>

   <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <!-- Custom Fonts -->
    <link href="bootstrap/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/dialog.css" rel="stylesheet">
   
   <!--Adde by vikas for validation error custom messages on form 31-may-2015 -->
    <link href="css/internal/form-validation.css" rel="stylesheet">
	
      <script src="bootstrap/js/jquery-1.11.2.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <!-- Steps Plugin CSS -->
       <link href="css/smart_wizard.css" rel="stylesheet" type="text/css">
       <script type="text/javascript" src="js/jquery.smartWizard.js"></script>
            
   
 <!-- Bootstrap Core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">

    <!-- Code Added by vikas on 23-May-2015 -->
    <!-- Used to display combobox values runtime -->
    <link href="css/external/select2/select2.min.css" rel="stylesheet" ></link>
    <script src="js/external/select2/select2.min.js"></script>
    <!-- Code Additon ends here by vikas on 23-May-2015 -->
     <link href="css/style.css" rel="stylesheet"></link>
     <!-- Timeline CSS -->

<?php
require_once 'core/init.php';
?>    
</head>

<body>
