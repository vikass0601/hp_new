<?php

session_start();

$GLOBALS['config'] = array (
   'mysql' => array(
       'host' => 'localhost',
       'username' => 'root',
       'password' => '',
       'db' => 'healthplease'
   ),
   'remember' => array(
       'cookie_name' => 'hash',
       'cookie_expiry' => 604800
   ),
   'session'=> array(
        'session_name' => 'user',
        'token_name'    => 'token'
   )
);

function myAutoload( $class ) {
   require_once 'classes/' . $class . '.php';
}
spl_autoload_register('myAutoload');

require_once 'functions/sanitize.php';
require_once 'functions/function.php';
require_once 'functions/commonFunctions.php';

/***
 * Call to check is come from forget password
 */

if( true == isset( $_REQUEST['action'] ) && 'null' == Input::get( 'action' ) ){
  $_SESSION['RECOVER_PWD_EXECUTED'] = 0; 
}

/*
 *  To redirect user if password is not changed
 */

if( true == isset( $_SESSION['RECOVER_PWD_EXECUTED'] ) && 1 === $_SESSION['RECOVER_PWD_EXECUTED'] ) {
  redirectUserIfPwdNotChanged();
}   

