<?php

class User {
    
    public $_db;
    public $_data;
    public $_sessionName;
    public $_isLoggedIn;
    //public $_user;

    public function __construct( $user = null ) {
        $this->_db = DB::getInstnace();
        $this->_sessionName = Config::get( 'session/session_name' );
        
        if( !$user ){
            if( Session::exists( $this->_sessionName ) ){
                $user = Session::get( $this->_sessionName );
                
                if( $this->findUser( $user ) ){
                    $this->_isLoggedIn = true;
                } else {
                    $this->_isLoggedIn = false;
                    //process logout
                }
            }
        } else {
            $this->findUser( $user );
        }
        
     }
    
    public function createUser( $fields = array() ) {

        if( !$this->_db->insert( 'users1', $fields ) ){
            throw new Exception( 'problem in inserting' );
            return false;
        }
        
        return true;
    }
    
    public function registerUser( $fields = array() ) {

        if( !$this->_db->insert( 'users', $fields ) ){
            throw new Exception( 'problem in user information' );
            return false;
        }
        
        return true;
    }
    
    public function login( $username = null, $password = null ) {
        $user = $this->findUser( $username );
       
        if( $user ) {
            if($this->data()->password === Hash::make($password, $this->data()->salt) ) {
             Session::put( $this->_sessionName, $this->data()->id ) ;
             return true;
            }
            
        }
        return false;
    }
    
    public function logout(){
        Session::delete( $this->_sessionName );
        
    }
    
    public function updateUser( $id ,$fields = array() ) {
        if( !$this->_db->update( 'users', $id, $fields ) ) {
                throw new Exception('problem in update');
                return false;
        }
        
        return true;
        
    }
    
    public function deleteUser( $id ){
        if( !$this->_db->delete( 'users1', array( 'id','=',$id ) ) ) {
            throw new Exception(' in deletion of record.');
                return false;
        }
        return true;
    }
    
    public function findUser( $user = null ) {
        if( $user ) {
            $field = ( is_numeric ( $user ) ) ? 'id' : 'username';
            $data = $this->_db->get( 'users1', array( $field , '=', $user ));
            
            if( $data->count() ) {
                $this->_data = $data->first();
                return true;
            }
        }
    }
    public function find( $user = null ){
        if( $user ) {
            $field = ( is_numeric ( $user ) ) ? 'id' : 'email_id';
            $data = $this->_db->get( 'users1', array( $field , '=', $user ));
            
            if( $data->count() ){
                return $data->results();
            }
            return false;
        }
    }
    
    public function isLoggedIn(){
      return $this->_isLoggedIn;
    }
    
    public function viewUsers($page_no) {
                $query = "SELECT * FROM users1";       
		$records_per_page=10;
                $newquery = $this->_db->paging("SELECT * FROM users1", $records_per_page, $page_no);
                $this->_db->fetchData($newquery);
                if($this->_db->count()>0)
		{
                $row = $this->_db->results();
		    for($i=0; $i< $this->_db->count();$i++ )
			{
                           	?>
                <tr>
                <td><?php print($row[$i]['id']); ?></td>
                <td><?php print($row[$i]['username']); ?></td>
                <td><?php print($row[$i]['password']); ?></td>
                <td><?php print($row[$i]['name']); ?></td>
               
                <td align="center">
                <a href="javascript:void(0);" class="js-edituser" data-value="action=getuser&id=<?php print($row[$i]['id']); ?>" data-page_no="<?php echo ((!empty( $page_no ))?$page_no:1);?>"><i class="glyphicon glyphicon-edit"></i></a>
                </td>
                <td align="center">
                <a href="javascript:void(0)"  class="js-deleteuser" data-value="delete_id=<?php print($row[$i]['id']); ?>&action=deleteuser&page_no=<?php echo ((!empty( $page_no ))?$page_no:1);?>"><i class="glyphicon glyphicon-remove-circle"></i></a>
                </td>
                </tr>
                <?php
			}
		}
		else
		{
			?>
            <tr>
            <td>Nothing here...</td>
            </tr>
            <?php
		}

              
    } 
    
    public function data(){
        return $this->_data;
    }
    
    public function pagingLink( $sql, $records_per_page = 1,$page_no = '', $redirect ){
        $self = $redirect;
        $this->_db->fetchData($sql);
	$total_no_of_records = $this->_db->count();
        if($total_no_of_records > 0)
		{
			?><ul class="pagination"><?php
			$total_no_of_pages=ceil($total_no_of_records/$records_per_page);
			$current_page=1;
			if(!empty( $page_no ))
			{
				$current_page= $page_no;
			}
			if($current_page!=1)
			{
				$previous =$current_page-1;
				echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=1' class='js-pageDisplay'>First</a></li>";
				echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$previous."' class='js-pageDisplay'>Previous</a></li>";
			}
			for($i=1;$i<=$total_no_of_pages;$i++)
			{
				if($i==$current_page)
				{
					echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$i."' style='color:red;' class='js-pageDisplay'>".$i."</a></li>";
				}
				else
				{
					echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$i."' class='js-pageDisplay'>".$i."</a></li>";
				}
			}
			if($current_page!=$total_no_of_pages)
			{
				$next=$current_page+1;
				echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$next."' class='js-pageDisplay'>Next</a></li>";
				echo "<li><a href='javascript:void(0);' data-url='".$self."' data-param='action=pg&page_no=".$total_no_of_pages."' class='js-pageDisplay'>Last</a></li>";
			}
			?></ul><?php
		}
    }
    
    public function hasPermission( $key ) {

        $group = $this->_db->get( 'groups', array( 'id', '=', $this->data()->group ) );
        if( $group->count() ){
            $permissions = json_decode( $group->first()->permissions, true );
            
            if( $permissions[$key] == true  ){
                return true;
            }
          return false;  
        }
        
    }
    
    public function loadAppointmentHistory( $id ) {
         
        $where = " WHERE app.user_id = " . $id;  
        
                
            $strSql = " SELECT 
                        app.id,
                        app.loc_id,
                        rs.r_name as name,
                        lb.lab_name,  
                        ts.test_name,
                        apt.test_id,
                        ts.test_charge,
                        app.date,
                        -- rs.r_phone as contact,
                        -- rs.r_address as address,
                        aps.name as status,
                        usr.username
                        
                    FROM
                        `appointments` app
                    INNER JOIN 
                        appointment_tests apt ON app.id = apt.appointment_id
                    INNER JOIN 
                        tests ts ON ts.id = apt.test_id
                    INNER JOIN
                        users1 usr ON app.user_id = usr.id 
                    INNER JOIN
                        locations lc ON app.loc_id = lc.id 
                    INNER JOIN 
                        labs lb ON app.lab_id = lb.id 
                    LEFT JOIN 
                        relations rs ON rs.appointment_id = app.id
                    INNER  JOIN
                        appointment_status aps ON aps.id = app.status_id
                   $where
                   AND rs.r_name is Not Null
                   
                   UNION ALL
                   
                  SELECT 
                            app.id,
                            app.loc_id,
                            CONCAT_WS( '', usr.fname,' ', usr.mname, ' ', usr.lname) as name,
                            -- usr.contact,
                            -- us.address,
                            -- usr.gender,
                            lb.lab_name,
                            tst.test_name,
                            appt.test_id,
                            tst.test_charge,
                            app.date,
                            aps.name as status,
                            us.username
                       FROM 
                                `appointments` app
                            INNER JOIN 
                                  users usr ON app.user_id = usr.id 
                            INNER JOIN 
                                  locations lc ON app.loc_id = lc.id 
                            INNER JOIN 
                                  labs lb ON app.lab_id = lb.id 
                            LEFT JOIN 
                                  users1 us ON us.id = usr.login_id 
                            INNER JOIN 
                                  appointment_tests appt ON appt.appointment_id = app.id 
                            INNER JOIN 
                                  tests tst ON tst.id = appt.test_id 
                            INNER  JOIN
                                  appointment_status aps ON aps.id = app.status_id
                   $where
                   AND us.name is Not Null
                   AND app.id NOT IN ( SELECT 
                                            rs.appointment_id 
                                        FROM
                                            relations rs 
                                        JOIN 
                                            appointments app ON app.id = rs.appointment_id 
                                        WHERE
                                            app.user_id = $id )
                   "; 
        
        $result = $this->_db->runQuery( $strSql );
        
         if( true == empty( $result ) || true == !isset( $result ) ){
             return false;
        }
        return  $result;
    }
    
    public function loadRecentCheckups( $id ) {
         
        $where = " WHERE app.user_id = " .( int ) $id ." and app.status_id = 3" ;  
        
                
            $strSql = "  
                        SELECT 
                        app.id,
                        rs.r_name as name,
                        lb.lab_name,  
                        ts.test_name,
                        ts.test_charge,
                        app.date,
                        -- rs.r_phone as contact,
                        -- rs.r_address as address,
                        aps.name as status,
                        usr.username
                        
                    FROM
                        `appointments` app
                    INNER JOIN 
                        appointment_tests apt ON app.id = apt.appointment_id
                    INNER JOIN 
                        tests ts ON ts.id = apt.test_id
                    INNER JOIN
                        users1 usr ON app.user_id = usr.id 
                    INNER JOIN
                        locations lc ON app.loc_id = lc.id 
                    INNER JOIN 
                        labs lb ON app.lab_id = lb.id 
                    LEFT JOIN 
                        relations rs ON rs.appointment_id = app.id
                    INNER  JOIN
                        appointment_status aps ON aps.id = app.status_id
                   $where
                   AND rs.r_name is Not Null  
                   LIMIT 5
                   
                   UNION ALL
                   
                  SELECT 
                            app.id,
                            CONCAT_WS( '', usr.fname,' ', usr.mname, ' ', usr.lname) as name,
                            -- usr.contact,
                            -- us.address,
                            -- usr.gender,
                            lb.lab_name,
                            tst.test_name,
                            tst.test_charge,
                            app.date,
                            aps.name as status,
                            us.username
                       FROM 
                                `appointments` app
                            INNER JOIN 
                                  users usr ON app.user_id = usr.id 
                            INNER JOIN 
                                  locations lc ON app.loc_id = lc.id 
                            INNER JOIN 
                                  labs lb ON app.lab_id = lb.id 
                            LEFT JOIN 
                                  users1 us ON us.id = usr.login_id 
                            INNER JOIN 
                                  appointment_tests appt ON appt.appointment_id = app.id 
                            INNER JOIN 
                                  tests tst ON tst.id = appt.test_id 
                            INNER  JOIN
                                  appointment_status aps ON aps.id = app.status_id
                   $where
                   AND us.name is Not Null 
                   LIMIT 5
                   "; 
               
        $result = $this->_db->runQuery( $strSql );
        
         if( true == empty( $result ) || true == !isset( $result ) ){
             return false;
        }
        return  $result;
    }
    
    public function change_pwd( $id ,$fields = array() ) {
        if( !$this->_db->update( 'users1', $id, $fields ) ) {
                throw new Exception('problem in updating password');
                return false;
        }
        return true;
    }
   
}

