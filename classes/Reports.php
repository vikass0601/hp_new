<?php
require_once 'core/init.php';

class Reports {

    public $_db;
    
    public function __construct() {
      $this->_db = DB::getInstnace();    
    }
    
    public function generateReportDateWise( $fromdate, $todate ) {
        
        $where = " WHERE 
                      app.date 
                   Between '" . $fromdate . "' and '" . $todate ."'";
        
        if( 'n/a' == $fromdate ) {
             $where = " WHERE app.date = '" . $todate . "'";  
        }
        
        $strSql = "SELECT 
                        app.id,
                        us.name,
                        us.username,
                        usr.contact,
                        usr.address,
                        usr.gender,
                        lb.lab_name,
                        aps.name as status,
                        aps.id as status_id,
                        ts.test_name as test_name,
                        ts.test_charge
                   FROM
                        `appointments` app 
                   INNER JOIN
                       users usr ON app.user_id = usr.id
                   INNER JOIN
                        locations lc ON app.loc_id = lc.id 
                   INNER JOIN 
                        labs lb ON app.lab_id = lb.id 
                   LEFT JOIN 
                        users1 us ON us.id = usr.login_id
                   INNER JOIN 
                        appointment_status aps ON aps.id = app.status_id
                   INNER JOIN 
                        appointment_tests apt ON apt.appointment_id = app.id 
                   INNER JOIN
                        tests ts ON ts.id = apt.test_id
                   " . $where; 
               // echo $strSql; die();
        $result = $this->_db->runQuery( $strSql );
        
        if( true == empty( $result ) || true == !isset( $result ) ){
             return false;
        }
        return  $result;
    }
}

