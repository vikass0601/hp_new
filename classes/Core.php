<?php
/********************************************************************
 * register_user - Registers a user in the database.
 * Class is used for temporary purpose we have to remove 
 * it when we filter our code
 * ADDED BY VIKAS 04-july-2014
 * For Validate Emaila and basic functionality 
 ********************************************************************/
class Core{
	public $db;
	protected $result;
	private $rows;

	public function __construct() {
		$this->db = new mysqli('localhost', 'root', '', 'login');
	}

	public function query($sql){
		$this->result = $this->db->query($sql);
	}

	public function rows(){
		for($i = 1; $i <= $this->db->affected_rows; $i++){
			$this->rows[] = $this->result->fetch_assoc();
		}
		return $this->rows;
	}
}
