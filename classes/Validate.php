<?php

class Validate {

    private $_passed = false,
            $_errors = array(),
            $_db = null;

    public function __construct() {
        $this->_db = DB::getInstnace();
    }

    public function check($source, $items = array()) {
        $sValidationMsg = array(
                            'username' => "Username / Email" ,
                            'password' => "Password" ,
                            'password_again' => "Confirm Password" ,
                            'fname' => "First Name" ,
                            'lname' => "Last Name" ,
                            'contact' => "Mobile No." ,
                            'add_street' => "Address" ,
                            'add_area' => "Area" ,
                            'add_landmark' => "Landmark" ,
                            'add_zipcode' => "Pincode" ,
                            'gender' => "Gender",
                            'dob' => "Date of Birth",
                            'age' => "Age",
                            'docName' => "Doctor name"
                         );
        $errorMsgName = '';
        foreach ($items as $item => $rules) {
            foreach ($rules as $rule => $rule_value) {
                // echo "{$item} {$rule} must be ";
                $value = trim( $source[$item] );
                $item = escape($item);
                /*
                 * will remove above array and below if condition in future
                 */
                
                if( true == array_key_exists( $item, $sValidationMsg) ) {
                    $errorMsgName  = $sValidationMsg[$item];
                } else {
                    if( true == array_key_exists('name_to_display', $rules) ){
                        $errorMsgName = $rules['name_to_display'];
                    }
                }
                
                
                if ($rule === 'require' && empty( $value ) ) {                 
                    $this->addError( ucfirst($errorMsgName) . ' is required.');    
                } else if( !empty( $value ) ) {
                    switch ( $rule ) {
                    case 'min':
                        if( strlen( $value ) < $rule_value ){
                            $this->addError(ucfirst($errorMsgName).' must be a minimum of character '.$rule_value);
                        }
                    break;
                
                    case 'max':
                        if( strlen( $value ) > $rule_value ){
                                $this->addError(ucfirst($errorMsgName).' must be a maximum of character '.$rule_value);
                        }
                    break;
                    
                    case 'matches':
                        if( $value != $source[$rule_value] ) {
                            $this->addError(ucfirst($errorMsgName).' must match with confirm password. ');
                        }
                    break;
                    
                    case 'unique':
                        $check = $this->_db->get( $rule_value,array(
                            $item, '=', $value));
                        if( $check->count() ){
                            $this->addError($value.' is alreday exist');
                        }
                    break;
                    
                    case 'no_space':
                        if( true == preg_match("/\\s/", $value ) ) {
                            $this->addError( 'Your '. ucfirst( $errorMsgName ).' must not contain any space.');
                        }
                    break;
                    
                    case 'valid_email':
                        if( false === filter_var( $value, FILTER_VALIDATE_EMAIL ) ){
                            $this->addError( ' A valid email address is required. ' );
                        } 
                    break;    
                        
                        
                    
                    default:
                    break;
                }
                    
                }
            }
        }
        
        if ( empty( $this->_errors ) ) {
            $this->_passed = true;
        }
        return $this;
    }

    public function addError( $error ) {
        $this->_errors[] = $error;
    }
    
    public function errors() {
        return $this->_errors;
    }
    public function passed() {
        return $this->_passed;
    }

}
