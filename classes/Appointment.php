<?php
error_reporting(E_ALL);
require_once 'core/init.php';
class Appointment {

    public $_db;
    
    public function __construct() {
      $this->_db = DB::getInstnace();    
    }
    
    public function createAppointment( $fields = array() ) {
        if( !$this->_db->insert( 'appointments', $fields ) ){
            throw new Exception( 'problem in inserting' );
            return false;
        }
        
        return true;
    }

    public function createAppointmentTest( $fields = array() ) {

        if( !$this->_db->insert( 'appointment_tests', $fields ) ){
            throw new Exception( 'problem in inserting' );
            return false;
        }
        
        return true;
    }
    
    public function createAppointmentOther($fields = array()){
        if( !$this->_db->insert( 'relation', $fields ) ){
            throw new Exception( 'problem in inserting' );
            return false;
        }
        
        return true;
    }


    public function updateAppointment( $id ,$fields = array() ) {
        if( !$this->_db->update( 'appointments', $id, $fields ) ) {
                throw new Exception('problem in update');
                return false;
        }
        return true;
        }
    
    public function updateAppointmentOther( $id ,$fields = array() ) {
        if( !$this->_db->update( 'relations', $id, $fields ) ) {
                throw new Exception( mysql_error());
                return false;
        }
        return true;
    }
    
    public function updateAppointmentAddress( $id ,$fields = array() ) {
        if( !$this->_db->update( 'appoint_addreses', $id, $fields ) ) {
                throw new Exception('problem in update');
                return false;
        }
        return true;
    }
    
    
    
    
    public function deleteAppointment( $id ){
        if( !$this->_db->delete( 'appointments', array( 'id','=',$id ) ) ) {
            throw new Exception(' in deletion of record.');
                return false;
        }
        return true;
    }
    
    public function insertMultipleApppoinments( $appointmentId,  $testIds ) {
        $strSql = "INSERT INTO `appointment_tests`(`appointment_id`, `test_id`)"
                . " VALUES ";
        $values = '';
        foreach( $testIds as $testid ) {
            $values .= "(". $appointmentId . "," . $testid . "),";
        }
       $sql = $strSql.trim( $values, ",") ;
       return $this->_db->executeQuery( $sql );
    }
    
    public function insertAppointmentAddress( $fields = array() ) {
        
        if( !$this->_db->insert( 'appoin_addresses', $fields ) ){
            throw new Exception( 'problem in inserting addresses' );
            return false;
        }
        
        return true;
    }

}

