<?php
include('base-header.php');
include('page-header.php');
$aAllCat=getAllCategories();
$aAllTests=getAllTests();
$aAllLoc=getAllLocations();
?>
<script type="text/javascript">
            // When the document is ready
      $(document).ready(function () {
        // var datepicker = $.fn.datepicker.noConflict();
          $('#idAppointmentDate').datepicker({
              dateFormat: 'dd-mm-yy' ,
              startDate: '+1d',
              minDate:'1'
          }); 
          
     /**
      * Code Added by vikas to select combobox runtime
      */  
          $( "#idTest" ).select2( {
                placeholder: "Select Your Test..",
          });
          
          
          $( "#idLocation" ).select2( {
                placeholder: "Select Your Location..",
          });
          
 
          $("#dob").datepicker({
             dateFormat: "yy-mm-dd",
            defaultDate:'1989-01-01',
            maxDate: 0,
            changeMonth: true, 
            changeYear: true,
            /*yearRange: '1900:' + new Date().getFullYear()*/
            yearRange: "c-100:c+25"
          });

            $('#idSpanDob').click(function(){
                $(this).closest('.input-group').find('.hasDatepicker').focus();
            }).css('cursor','pointer');

                function calculateAge(birthday) {
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    return Math.abs(ageDate.getUTCFullYear() - 1970);
                }


               $('#dob').change(function(){
                var dob = $(this).val();
                var aDOB = dob.split('-');
                var year = parseInt(aDOB[0]);
                var month = parseInt(aDOB[1]) - 1;
                var day = parseInt(aDOB[2]);
                var bDay = new Date(year, month, day, 0, 0, 0, 0);
                console.log(bDay);
                var age = calculateAge(bDay);

                $('#age').val(age);

              });   
      });
  </script>
   <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
  <style>
   
  </style>
       
<script>
    $(document).ready(function(){

    $("#idBookAppointmentOnLogin").on( "click", function(){

          var tempDate = $("#idAppointmentDate").val().split( '-' );
          var appointmentDate = tempDate[2] + '-' + tempDate[1] + '-' + tempDate[0];
          var appointmentForm=$("#on_form_appointment");
          
            $.ajax({
                  type:"POST",
                  url:appointmentForm.attr("action"),
                  data:appointmentForm.serialize()+"&action=addapp&appointmentDate="+appointmentDate,//only input
                  success: function(response){
                      if( response == 1 ){
                          //$("#form_add_app").trigger("reset");
                          window.location = 'thanks-appointment-done.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                      } else {
                          alert('try again!');
                      }
                  }
              });  
          });

         /* On key press remove error alert */
    $("#username").keypress(function(){
      $("#idUsernameErr").html("");
    });
    $("#password").keypress(function(){
      $("#idPasswordErr").html("");
    });

});    
       
</script>
        <div class="classSliderHolder">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                      <div class="classHAbtTextWrapper">                         
                        <div class="classOSCFont classFrontText">
                          Your convenience is our priority...<br/>
                          And how do we do it? <br/>We provide Laboratory testing right at your doorstep.<br/><br/>
                          <div class="classBColor">Book your Appointment Now&nbsp;&nbsp;&nbsp;<i class="fa fa-hand-o-right"></i></div>
                        </div>
                       
                      </div>                     
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <!-- Form Div Start-->
                        <div class="auth">
                            <div id="big-form" class="well auth-box">
                            
                                <form method='post' id="on_form_appointment" action="userController.php">
                              
                                <fieldset>

                                  <!-- Form Name -->
                                  <legend><i class="fa fa-calendar"></i> Book Your Appointment</legend>
                                  <!-- Select Basic -->
                                  <div class="form-group">
                                    <label class=" control-label" for="selectbasic">Test</label>
                                     <!--  <div class="ui-widget">

                                      <select id="idTest" name="test" class="form-control selectpicker">
                                        <option value='select'>Select Your Test</option>
                                        <?php
                                          /*foreach ($aAllTests as $key => $value) {
                                            echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                          }*/
                                        ?>
                                      </select>
                                      </div> -->
                                      <div class="ui-widget">
                                        <select id="idTest" name="test" class="form-control selectpicker">
                                          <option value="select" selected="">Select Your Test</option>
                                                  <?php
                                                foreach ($aAllTests as $key => $value) {
                                                  echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                                }
                                              ?>
                                        </select>
                                      </div>
                                    <div class="" id="idTestErr"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class=" control-label" for="selectbasic">Your Location</label>
                                    <div class="ui-widget">
                                      <select id="idLocation" name="location" class="form-control">
                                        <option value='select'>Select Your Location</option>
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                    <div class="" id="idLocationErr"></div>
                                  </div>
                                  <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class=" control-label" for="appointmentDate">Appointment Date</label>  
                                            <div class="">
                                              <input id="idAppointmentDate" name="appointmentDate" placeholder="dd/mm/yyyy" class="form-control input-md" type="text">
                                            </div>
                                            <div class="" id="idAppDateErr"></div>
                                        </div> 
                                      <div class="form-group col-md-6">
                                        <label class=" control-label" for="appointmentTime">Appointment Time</label>
                                        <div class="">
                                         <input id="textinput" name="appointmentTime" placeholder="" class="form-control input-md" type="text" value="Between 7:00 am to 12:00pm" readonly/>
                                        </div>
                                      </div>
                                        
                                    </div>

                                                                  
                                 
                                  <!-- Button (Double) -->
                                  <!-- <div class="form-group">
                                    <label class=" control-label" for="button1id">Appointment Time</label>
                                    <div class="">
                                      <button id="idBookedSlot" name="bookedSlotss" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                       <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>                                      
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                    </div>
                                  </div> -->

                                  <div class="form-group">
                                    <div class="">
                                      <input type="hidden" name="bookAppFlag" id="idBookAppFlag" value="">
                                      <?php
                                      if($bLoginFlag){
                                        ?>
                                        <input type="button" id="idBookAppointmentOnLogin" name="bookAppointment" value="Book Appointment" class=" btn btn-success"></inputs>
                                        <?php
                                      }else{
                                      ?>
                                      <a href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"><input type="submit" id="idBookAppointment" name="bookAppointment" value="Book Appointment" class=" btn btn-success"></input></a>
                                       <?php 
                                      }
                                      ?>
                                      
                                      
                                      <span class="help-block">help</span>  
                                    </div>
                                  </div>

                                </fieldset>
                              </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Form Div End-->
                    </div>
                </div>    
            </div>
        </div>
        <div id="page-wrappera" class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Click To Request A Check Up</h3>
                    <a href="#"><img src="images/1.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Importance Of Check Ups</h3>
                    <a href="#"><img src="images/2.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Learn More</h3>
                    <a href="#"><img src="images/3.jpg" width="100%"/></a>

                </div>
                <!-- /.col-lg-12 -->
            </div>
<br/><br/><br/>
            <!-- /.row -->             
        </div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>




<strong>YEAR-2015</strong>
<ol class="classPList">
<li>Ranganathan P, Gogtay NJ. Improving peri-operative patient care: the surgical safety checklist. J Postgrad Med 2015; 61: 73-4.</li>
<li>Ray S, Kumar V, Bhave A, Singh V, Gogtay N, Thatte U, Talukdar A, Kochar SK,Patankar S, Srivastava S. Proteomic analysis of Plasmodium falciparum induced alterations in humans from different endemic regions of India to decipher malaria pathogenesis and identify surrogate markers of severity. J Proteomics. 2015 May 14. pii: S1874-3919(15)00232-8. doi:10.1016/j.jprot.2015.04.032. [Epub ahead of print]</li>
<li>Thaker SJ, Sinha RS, Gogtay NJ & Thatte UM. Evaluation of inter rater agreement between three causality scales used in Pharmacovigilance" Jr Pharmacol PharmacoTher – accepted for publication.</li>
<li>Jasani B,  Kannan S,  Nanavati R,  Gogtay NJ & Thatte UM. An Audit of Colistin Use in Neonatal Sepsis from a Tertiary Care Centre of a Resource Limited Country.  Ind Jr Med Res 2015; accepted for publication.</li>
</ul>