<?php
include('base-header.php');
include('page-header.php');
?>
        <div class="classTopHeading">
        <div class="container">
            <div class="row">
            <div class="col-lg-12">
                <h1 class="">About Us</h1>
            </div>
        </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
        <div id="page-wrappera" class="container">
            
            <!-- /.row -->
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="classAboutHighLightBox classOSCFont">
                        <i class="fa fa-quote-left classAQuote"></i> At Healthplease.in Your convenience is our priority. <br/>
                        Our support team or as we fondly call it as convenience team puts in every effort to fulfil your medical needs. 
                        And how do we do it? <br/>
                        We provide Laboratory testing right at your doorstep. We do not want you to go to laboratory instead we bring the laboratory for you at your house. 
                        Thus we eliminate the trouble that a patient has to go through. 
                        Patient’s happiness and satisfaction is what gives us energy to keep moving. 
                    </div>
                </div>
            </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">We Are In Support</h3>
                    <div class="classSFormHolder">
                        <img src="images/1.jpg" width="100%"/>
                        <div class="classSupportForm">
                            <div class="classLabDetailWrapper">
                                <div class="classLabName"> Dhanwantary Path Lab </div>
                                <div class="classLabDetails">
                                    Dr. S.S Jagalpure  M.D. (Path) <br/>
                                    Geeta Complex Kasturaba Hgs Soc, Shop no-3, Behind Jakat Naka, Vishrantwadi Pune - 411015                               
                                </div>
                            </div>             

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Importance Of Check Ups</h3>
                    <a href="#"><img src="images/2.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Ask your Query</h3>
                    
                    <div class="classSFormHolder">
                        <img src="images/3.jpg" width="100%"/>
                        <div class="classSupportForm">
                            <form id="idSupportPanal" class="classSupportPanal" action="" method="GET">
                              
                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                       <input type="text" class="form-control" id="idName" name="user[name]" placeholder="Full name*" required>           
                               </div> 
                              </div>

                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                       <input type="email" class="form-control" id="idEmail" name="user[email]" placeholder="User name - Email*" required>           
                               </div> 
                              </div>
                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-comment "></i></span>
                                       <textarea class="form-control" id="idQuery" name="user[query]" placeholder="Your Query*" style="height: 150px;" required> </textarea>
                               </div> 
                              </div>
                              <div class="row">
                                            
                                 <div class="col-lg-4">
                                    <button type="submit" class="classBtnSubmit btn btn-info" id="idSupportSubmit">Send</button>
                                 </div>
                              </div>  

                            </form>
                        </div>
                    </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div><br/><br/>
            <!-- /.row -->
        <!-- </div>
        
        </div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>
