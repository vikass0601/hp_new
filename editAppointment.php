<?php
include('base-header.php');
include('page-header.php');
$aAllCat = getAllCategories();
$aAllTests = getAllTests();
$aAllLoc = getAllLocations();
error_reporting(E_ALL);
require_once 'core/init.php';
        $id = $_REQUEST['id'];
        $user = new User();      
        // To check wether appointment detail come from relations table or not  
        $strSql = 'SELECT count(*) as cnt FROM relations where appointment_id ='.$id;
        $Count = $user->_db->runQuery( $strSql );
        // if its 0 then it come from appointment
        if( 0 == $Count[0]['cnt'] ) {
        $strSql = 'SELECT 
                        app.id,
                        app.loc_id,
                        apt.test_id,
                        lc.location_name,
                        app.date,
                        ts.test_name,
                        ts.test_charge,
                        lb.lab_name,
                        us.contact,
                        CONCAT( "Between 7:00 am"," to 12:00pm") as time,
                        app.doctor_name
                    FROM
                        `appointments` app
                    INNER JOIN 
                   	appointment_tests apt ON app.id = apt.appointment_id
                    INNER JOIN 
                   	tests ts ON ts.id = apt.test_id
                    INNER JOIN
                        users1 usr ON app.user_id = usr.id 
                    INNER JOIN
                        locations lc ON app.loc_id = lc.id 
                    INNER JOIN 
                        labs lb ON app.lab_id = lb.id 
                    LEFT JOIN 
                        users us ON us.login_id = usr.id
                   Where app.id = '. ( int )$id;
        $UserData = $user->_db->runQuery( $strSql );
        } else {
           $strSql = 'SELECT 
                        app.id,
                        app.date,
                        apt.test_id,
                        ts.test_name,
                        ts.test_charge,
                        lb.lab_name,
                        rs.r_name,
                        rs.r_email,
                        rs.relation,
                        rs.r_phone,
                        rs.r_address,
                        rs.`r_gender`,
                        rs.`add_street`,
                        rs.add_area,
                        rs.add_landmark,
                        rs.add_city,
                        rs.add_zipcode,
                        rs.r_dob,
                        rs.r_age,
                        CONCAT( "Between 7:00 am"," to 12:00pm") as time,
                        app.doctor_name,
                        app.loc_id,
                        lc.location_name
                    FROM
                        `appointments` app
                    INNER JOIN 
                        appointment_tests apt ON app.id = apt.appointment_id
                    INNER JOIN 
                        tests ts ON ts.id = apt.test_id
                    INNER JOIN
                        users1 usr ON app.user_id = usr.id 
                    INNER JOIN
                        locations lc ON app.loc_id = lc.id 
                    INNER JOIN 
                        labs lb ON app.lab_id = lb.id 
                    LEFT JOIN 
                        relations rs ON rs.appointment_id = app.id
                   Where app.id = '. ( int )$id;
        
        $UserData = $user->_db->runQuery( $strSql );
           
       }

  
  $data = $user->_db->runQuery("SELECT
                                       *,
                                       CONCAT_WS( '', fname,' ', mname, ' ', lname) as name
                                  FROM
                                    `users` 
                                  WHERE 
                                    users.login_id =".Session::get( $user->_sessionName ));
  
  $data1 = $user->_db->runQuery("SELECT
                                       *
                                  FROM
                                    `users1` 
                                  WHERE 
                                    users1.id =".Session::get( $user->_sessionName )); 
?>       

        <div class="classSliderHolder">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <div class="classOSRBut" id="idOSRHolder">
                            <label>Edit Appointment</label>
                        </div>    
                    </div> 
<!--                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                          <div class="radio classOSRBut" id="idOSRHolder">
                            <label><input type="radio" id="idBookYourself" name="bookApp">Book Yourself</label>
                          </div>
                    </div>      
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                          <div class="radio classORBut" id="idORHolder">
                            <label><input type="radio" id="idBookOther" name="bookApp">Book for other</label>
                          </div>                
                    </div>                     -->
                </div>    
                <div class="row" id="idDivBookYourself">
                  <div class="col-sm-12 col-lg-8 col-md-8 col-xs-12">
                      <?php if( 0 == $Count[0]['cnt'] ) include_once"editYourself.php"; ?>
                  </div>    
                </div>
                <div class="row" id="idDivBookOther">
                    <div class="col-sm-12 col-lg-8 col-md-8 col-xs-12">
                      <?php if( 0 < $Count[0]['cnt'] ) include_once"editOther.php"; ?>
                    </div>  
                </div>
               
            </div>
        </div>
        <div id="page-wrappera" class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Click To Request A Check Up</h3>
                    <a href="#"><img src="images/1.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Importance Of Check Ups</h3>
                    <a href="#"><img src="images/2.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Learn More</h3>
                    <a href="#"><img src="images/3.jpg" width="100%"/></a>

                </div>
                <!-- /.col-lg-12 -->
            </div>
<br/><br/><br/>
            <!-- /.row -->             
        </div>
        <!-- /#page-wrapper -->

<script>
/**
 * 
 * @returns to load edit data
 */
            $(document).ready(function(){
                
                        var editInformationDetail = <?php echo $Count[0]['cnt'];?>; // 0 - self 1 - other
                        if( 0 == editInformationDetail ) {
                            $("#idBookYourself").attr('checked', 'checked');
                            
                        } else {  }
//                        $.ajax({
//                            type:"POST",
//                            url:'appoincontroller.php?action=loadEditInfo&edit='+ editInformationDetail +'&appid='+<?php //echo $_REQUEST['id']; ?>,
//                            success: function(response){
//                                var obj = $.parseJSON( response );
//                                $("#idTest").val( obj[0]['test_id'] );
//                                
//                            }
//                        });

            });  


   $("#idAppDate").datepicker({
       format: 'dd-mm-yyyy',
        startDate: '+1d',
        minDate:'1',
        maxDate:'+2m',
        viewMode: "months", 
        changeMonth: true,
         autoclose: true
    });


 function generateDate( date ) {
            var tempDate = date.split( '/' );
            return tempDate[2] + '-' + tempDate[0] + '-' + tempDate[1];
          }
               $('#pDob').change(function(){
                   var tempAge = $(this).val();
                   var dob = new Date( tempAge );
                   var today = new Date();
                   var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                   $('#pAge').val(age);

              });   

               $('#dob').change(function(){
                   var tempAge = $(this).val();
                   var dob = new Date( tempAge );
                   var today = new Date();
                   var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                   alert(age);
                   $('#age').attr('value','');
                   $('#age').attr('value',age);

              });  

            $('#idSpanDob').click(function(){
                $(this).closest('.input-group').find('.hasDatepicker').focus();
            }).css('cursor','pointer');

            
            $("#editAppYour").click( function() {
                 var appointmentForm = $("#formBookAppYour");
                 var appointment_date = generateDate( $( "#idAppDate" ).val() );
                 /*
                  * Validation of form goes here
                  */
                  var form = $("#formBookAppYour");
                  if ( false == form.valid() ) exit;
	
                 
                  $.ajax({
                        type:"POST",
                        url:appointmentForm.attr( "action" ),
                        data:appointmentForm.serialize()+"&appointmentDate="+ appointment_date +"&action=editappYourself",//only input
                        success: function(response){
        
                if( response ){
                     window.location = 'thanks-appointment-done.php?appid='+$( "#appid" ).val()+'&edit=1';
                } else {
                     alert('Can\'t update your appointment at this moment!');
                }
                        }
                    });  
            });
            //to generate date for database table
             function generateDate( date ) {
                 var tempDate = date.split( '/' );
                 return tempDate[2] + '-' + tempDate[0] + '-' + tempDate[1];
             } 

             $("#EditAppOther").click(function(){
                 var appointmentForm=$("#formBookAppOther");
                 //to generate date for database table
                 var appointment_date = generateDate( $( "#app_date" ).val() );
                 
                 /*
                  * Validation of form goes here
                  */
                  var form = $("#formBookAppOther");
                  if ( false == form.valid() ) return;
	
                 
                  $.ajax({
                        type:"POST",
                        url:appointmentForm.attr("action"),
                        data:appointmentForm.serialize()+"&appointmentDate="+ appointment_date +"&action=editappOther",//only input
                        success: function(response){
                            if( response ){
                                //$("#form_add_app").trigger("reset");
                                 window.location = 'thanks-appointment-done.php?appid='+$( "#appidOther" ).val()+'&edit=1';
                            } else {
                                alert('try again!');
                            }
                        }
                    });  
            });
</script>
<?php
include('page-footer.php');
include('base-footer.php');
?>
