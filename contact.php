<?php
include('base-header.php');
include('page-header.php');
?>
        <div class="classTopHeading">
        <div class="container">
            <div class="row">
            <div class="col-lg-12">
                <h1 class="">Contact Us</h1>
            </div>
        </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
        <div id="page-wrappera" class="container">
            
            <!-- /.row -->
                <div class="row">
                    <div class=" col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                        <div class="classHighLightBox classOSCFont ">
                            Have Any Questions?? <br/>Feel free to contact us on our customer support number <i class="fa fa-mobile"></i> <a href="tel:+91 8600080252">+91 8600080252</a> or<br/> mail us at <i class="fa fa-envelope-o"></i><a href="Milto:help@healthplease.in"> help@healthplease.in</a> <br/>we will be glad to help you. 
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4" style="margin-top: -90px;">
                    <h3 class="page-header classHeaderText">Ask your Query</h3>
                    
                    <div class="classSFormHolder">
                        <img src="images/3.jpg" width="100%"/>
                        <div class="classSupportForm">
                            <form id="idSupportPanal" class="classSupportPanal" action="" method="GET">
                              
                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                       <input type="text" class="form-control" id="idName" name="user[name]" placeholder="Full name*" required>           
                               </div> 
                              </div>

                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                       <input type="email" class="form-control" id="idEmail" name="user[email]" placeholder="User name - Email*" required>           
                               </div> 
                              </div>
                              <div class="form-group"> 
                               <div class="input-group">           
                                    <span class="input-group-addon"><i class="fa fa-comment "></i></span>
                                       <textarea class="form-control" id="idQuery" name="user[query]" placeholder="Your Query*" style="height: 150px;" required> </textarea>
                               </div> 
                              </div>
                              <div class="row">
                                            
                                 <div class="col-lg-4">
                                    <button type="submit" class="classBtnSubmit btn btn-info" id="idSupportSubmit">Send</button>
                                 </div>
                              </div>  

                            </form>
                        </div>
                    </div>

                </div>
                </div>
              <div class="row">
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <h3 class="page-header classHeaderText">We bring Convenience to you!</h3>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d242118.06894793978!2d73.86296739999999!3d18.524616450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bf2e67461101%3A0x828d43bf9d9ee343!2sPune%2C+Maharashtra!5e0!3m2!1sen!2sin!4v1430582809404" width="100%" height="360" frameborder="0" style="border:0"></iframe>
                </div>
                
                <!-- /.col-lg-12 -->
            </div>
        </div><br/><br/>
            <!-- /.row -->
        <!-- </div>
        
        </div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>
