<?php
include('base-header.php');
include('page-header.php');
?>

    <!-- Signup form modal code start -->
    <div class="container" id="addUser" name="addUser">

      <div class="modala-header">
            <h4 class="modal-title text-center" id="exampleModalLabel">Signup</h4>
		        <div id="msg"></div>
      </div>
  	  <div class="error_container alert alert-danger">
  			<ol></ol>
  	  </div>
      <div class="">
        <form method='post' id="form_register" action="userController.php">
   
            <table class='table table-bordered'>
                <tr>
                    <td>Name</td>
                    <td>
                        <input type='text' name='fname' id='fname' class='form-control' placeholder="First Name" title="Please enter your First name !" data="">
                        <input type='text' name='mname' id='mname'  placeholder="Middle Name"class='form-control'>
                        <input type='text' name='lname' id='lname' class='form-control' placeholder="Last Name" title="Please enter your Last name !" data=""></td>
                    </td>
                        <input type='hidden' name='token' id='token' class='form-control' value="<?php echo Token::generate();?>" >
                </tr>
                <tr id="idErrName">
                    
                </tr>
                <tr>
                    <td>Email</td>
                    <input type='hidden' name='id' id="id" class='form-control' value="">
                    <td><input type='email' name='username' id='idUsername' placeholder="E-mail" class='form-control' title="Please enter your email address!" data=""></td>
                </tr>
                <tr id="idErrEmail">
                </tr>  
                <tr  id="idErrEmailAddress">
                </tr>  
                <tr>
                    <td>Password</td>
                    <td><input type='password' name='password' id='idPassword' class='form-control' placeholder="Password" title="Please enter your password !" data=""></td>
                </tr>
                <tr id="idErrPassword"></tr>  
                <tr>
                    <td>Confirm Password</td>
                    <td><input type='password' name='password_again' id='password_again' class='form-control' placeholder="Password_again" title="Please enter password again field !" data=""></td>
                </tr>
                <tr id="idErrPasswordAgain"></td></tr>        
                <tr id="idErrPasswordCheck"></td></tr>        
                <tr>
                    <td>Mobile No</td>
                    <td><input type='number' name='contact' id='contact' class='form-control' placeholder="Contact Number" maxlength="10" size="10" title="Please provide Contact no !" data=""></td>
                </tr>
                <tr id="idErrContact"></tr>        
                <tr>
                    <td>Address</td>
                    <td> 
                        <!--<textarea class="form-control classAddress" rows="3" id="address" name="address"></textarea>-->
                        <fieldset>    
                                        <!-- Textarea -->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_street">Street</label>-->
                                      <div class="controls">                     
                                          <textarea id="add_street" name="add_street" class="input-medium form-control" placeholder="Street" title="Please provide Street name !" name="add_street" data=""=""></textarea>
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_area">Area</label>-->
                                      <div class="controls">
                                        <input id="add_area" name="add_area" placeholder="Area" class="input-medium form-control" data=""="" title="Please provide area !" type="text">

                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_landmark">Landmark</label>-->
                                      <div class="controls">
                                        <input id="add_landmark" name="add_landmark" placeholder="Landmark" class="input-medium form-control" type="text">

                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="area_city">City</label>-->
                                      <div class="controls">
                                          <input id="add_city" name="add_city" placeholder="City" class="input-medium form-control" data=""=""  value="Pune" type="text" readonly="">

                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="area_zipcode">Pincode</label>-->
                                      <div class="controls">
                                          <input id="add_zipcode" name="add_zipcode" placeholder="Zipcode" class="input-medium form-control" minlength="6" maxlength="6"  type="number" title="Please provide Pincode !" data="">

                                      </div>
                                    </div>

                                    </fieldset>
                    </td>
                </tr>
                <tr id="idErrAddress"></tr>        
                <tr>
                    <td>Gender</td>
                    <td>
                     <div class="radio">
                         <label><input type="radio" name="gender" value="M" checked="checked">Male</label>
                         <label> <input type="radio" name="gender" value="F">Female</label>
                     </div>
                </tr>
                
                <tr>
                    <td>DOB</td>
                    <td>

                    <div class='date' id='datetimepicker1'>
                        
                            <input type='text' name="dob" id="dob" placeholder="Date Of Birth" class="form-control classdob" />
                       
                    </div>    
                    </td>
                </tr>
                <tr id="idErrDob"></tr>
                <tr>
                    <td>Age</td>
                    <td><input type='text' name='age' id='age' class='form-control' placeholder="Age" value="" data="" readonly></td>
                </tr>
                
                <tr>
               
                <td colspan="2">
                   
                    <div id="edituser">
                        <button type="submit" class="btn btn-primary" name="btn-update" id="idRegisterSubmit" name="register">
                  <span class="glyphicon glyphicon-edit"></span>  Register
                        </button>
                        <a href="#"  data-dismiss="modal" class="btn btn-large btn-danger"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
                    </div> 
                
                </td>
                    
                 
                 
                </tr>
         
            </table>
        </form>
        <div class=""id="idLoaderDiv" style="display:none;" >
            <img src="iamges/loader.gif" id="idLoaderGif" class="classLoaderImg">
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-12 control"> 
                  Already had a account
              <a class="btn btn-info btn-xs loginid" id="" href="#"  data-keyboard="false" data-backdrop="false">
              <!-- <a class="btn btn-info btn-xs" id="loginid" href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"> -->
                  Login
              </a>
             
          </div>
        </div>    
      </div>
    </div>
<!-- SignUp modal form code end-->

<script type="text/javascript">
 function generateDate( date ) {
            var tempDate = date.split( '/' );
            return tempDate[2] + '-' + tempDate[0] + '-' + tempDate[1];
          }
               $('#dob').change(function(){
                   var tempAge = $(this).val();
                   var dob = new Date( tempAge );
                   var today = new Date();
                   var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                   $('#age').val(age);

              }); 
</script>
<?php
include('page-footer.php');
include('base-footer.php');
?>
