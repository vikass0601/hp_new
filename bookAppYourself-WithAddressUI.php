         <?php 
         $aAllCat = getAllCategories();
         $aAllTests = getAllTests();
         $aAllLoc = getAllLocations();
         ?>   


<?php
  //include('steps-form.php');
?>
<script type="text/javascript">
   
    $(document).ready(function(){
      // Smart Wizard 
      $('#wizard').smartWizard({transitionEffect:'slide'});
     
    });
</script>
<!-- <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;">
<tr><td>  -->
 <form method='post' id="formBookAppYour" class="classOSCAForm" action="userController.php">
 
      <div id="wizard" class="swMain">
        <ul>
          <li class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3"><a href="#step-1">                
                <span class="stepDesc">
                   Appointment<br />
                   <small>Details</small>
                </span>
            </a></li>
          <li class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3"><a href="#step-2">                
                <span class="stepDesc">
                   Address<br />
                   <small>Detials</small>
                </span>
            </a></li>
          <li class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xl-3"><a href="#step-3">                
                <span class="stepDesc">
                   Check Appoitment<br />
                   <small>Details</small>
                </span>                   
             </a></li>
         
        </ul>
        <div id="step-1"> 
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
             <table class='table table-bordered'>
                  <tr>
                  <?php $testName = getAllTestsByIds($_GET['testName']);?>
                  <td>Test*</td>
                  <td>
                       <select id="idTest" name="test[]" class="form-control selectpicker classTestSelect" multiple="">
                          <?php  foreach( $testName as $key => $value ){?>
                                     <option value="<?php echo $testName[$key]['id']; ?>" selected=""><?php echo $testName[$key]['test_name']; ?></option>    
                                 <?php } ?>
                       
                                <?php
                              foreach ($aAllTests as $key => $value) {
                                echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                              }
                            ?>
                      </select>
                      <!--<input type="hidden" name="test" value="<?php //echo $_GET['testName'] ?>">-->                                            
                          <!--<input type='text' name='testName' id='testName' class='form-control' value="<?php// echo $testName[0]['test_name']; ?>" required>-->
                   </td>                      
              </tr>
              <tr id="idErrTest"></tr>

              <tr>
                  <?php $locName = getAllLocationsById($_GET['locName']);
                  ?>
                  <td>Location*</td>
                  <td>
                      <select id="idLocation" name="location" class="form-control classLocationSelect">
                      <option value='<?php echo $_GET['locName']; ?>'><?php echo $locName['location_name'];?></option>
                      <?php
                        foreach ($aAllLoc as $key => $value) {
                          echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                        }
                      ?>
                    </select>
                      <!--<input type="hidden" name="location" value="<?php //echo $_GET['locName']; ?>">-->                                                 
                      <!--<input type='text' name='testLocation' id='testLoction' class='form-control' value="<?php //echo $locName['location_name'];?>" required></td>-->                      
                  </td>     
              </tr>
              <tr id="idErrLocation"></tr>
              <tr>
                  <td>Appointment Date*</td>
                  <td><input type='text' name='appointmentDate' id='idAppDate' class='form-control' value="<?php echo $_GET['appDate'];?>" required></td>
              </tr>
              <tr id="idErrAppDate"></tr>  
              <tr>
                  <td>Appointment Time*</td>
                  <td><input type='text' name='appointmentTime' id='isAppTime' class='form-control' value="<?php echo $_GET['appTime'];?>" required></td>
              </tr>
              <tr id="idErrApptime"></td></tr>  
              <tr>
                  <td>Doctor Name*</td>
                  <td><input type='text' name='docName' id='isDocName' class='form-control' value="" required></td>
              </tr>
              <tr id="idErrDocName"></td></tr>         
              
              </table>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                  <table class='table table-bordered'>
                    <tr id="idUserName">
                        <td>Your Name *</td>
                        <td><input type='text' name='name' id='name' class='form-control' value="<?php echo $data[0]['name']; ?>" required></td>
                            
                    </tr>
                    <tr id="idErrName"></tr>
                    <tr id="idRelationDiv" ></tr>
                    
                    
                  
                    <tr>
                        <td>Email*</td>
                        <input type='hidden' name='id' id="id" class='form-control' value="">
                        <td><input type='email' name='username' id='idUsername' class='form-control' value="<?php echo $data1[0]['username']; ?>"required></td>
                    </tr>
                    <tr id="idErrEmail">
                    </tr>  
                    <tr  id="idErrEmailAddress">
                    </tr>  
                    <tr>
                        <td>Mobile No*</td>
                        <td class="input-group"> <div class="input-group-addon">+91</div><input type='number' name='contact' id='contact' class='form-control' value="<?php echo $data[0]['contact']; ?>" style="position: relative;  z-index: 1;" maxlength="10" size="10" required></td>
                    </tr>
                    <tr id="idErrContact"></tr>
                     <tr>
                        <td>Gender</td>
                        <td>
                         <div class="radio">
                            <?php if($data[0]['gender'] == 'M'){ ?>
                                 <label><input type="radio" name="gender" value="M" checked="checked">Male</label>
                                 <label><input type="radio" name="gender" value="F">Female</label>
                            <?php } else { ?>
                                 <label><input type="radio" name="gender" value="M" >Male</label>
                                 <label><input type="radio" name="gender" value="F" checked="checked">Female</label> 
                            <?php } ?>
                         </div>
                    </tr>
                    
                    <tr>
                        <td>DOB</td>
                        <td>
                        <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="dob" id="dob"  value="<?php echo $data [0]['dob']; ?>" class="form-control classdob" disabled />
                                <span class="input-group-addon" id="idSpanDob">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>    
                        </td>
                    </tr>
                    <tr id="idErrDob"></tr>
                    <tr>
                        <td>Age</td>
                        <td><input type='text' value="<?php echo $data[0]['age'] ?> " name='age' id='age' class='form-control' required readonly></td>
                    </tr>
                    
                            
                  </table>
                </div>
              </div>
          </div>
          <div id="step-2"> 
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                  <table class='table table-bordered'> 
                     <tr rowspan="2">
                      <td>Address*</td>
                      <!--<td> <textarea class="form-control classAddress" rows="5" id="address" name="address"><?php //echo $data[0]['address']; ?></textarea></td>-->
                      <td>
                      <fieldset>    
                          <!-- Textarea -->
                      <div class="control-group">
                        <!--<label class="control-label" for="add_street">Street</label>-->
                        <div class="controls">                     
                            <textarea id="add_street" name="add_street" class="input-medium form-control" placeholder="Street" name="add_street" required=""><?php echo $data[0]['add_street']; ?>  </textarea>
                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="control-group">
                        <!--<label class="control-label" for="add_area">Area</label>-->
                        <div class="controls">
                          <input id="add_area" name="add_area" placeholder="Area" class="input-medium form-control" value="<?php echo $data[0]['add_area']; ?>" required="" type="text">
                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="control-group">
                        <!--<label class="control-label" for="add_landmark">Landmark</label>-->
                        <div class="controls">
                          <input id="add_landmark" name="add_landmark" placeholder="Landmark" class="input-medium form-control" value="<?php echo $data[0]['add_landmark']; ?>" required="" type="text">
                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="control-group">
                        <!--<label class="control-label" for="area_city">City</label>-->
                        <div class="controls">
                            <input id="add_city" name="add_city" placeholder="City" class="input-medium form-control" required=""  value="Pune" type="text" readonly="">
                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="control-group">
                        <!--<label class="control-label" for="area_zipcode">Pincode</label>-->
                        <div class="controls">
                          <input id="add_zipcode" name="add_zipcode" placeholder="Zincode" class="input-medium form-control" value="<?php echo $data[0]['add_zipcode']; ?>" type="number" required>

                        </div>
                      </div>

                      </fieldset>
                    </td>
                    </tr>
                    <tr id="idErrAddress"></tr>        
                  </table> 
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                  <table class='table table-bordered'>
                   <tr rowspan="2">
                    <td>Pick Up Address*
                      <div class="control-group">
                        
                        <div class="controls text-left">                     
                            <input type="checkbox" id="idCheckAddress" name="check_address" class="input-medium form-control"  name="check_address" style="width: 10%;" required=""/>                            
                        </div>
                        <label class="control-label" for="check_address">Check To Have Same Address</label>
                      </div>
                    </td>
                    <!--<td> <textarea class="form-control classAddress" rows="5" id="address" name="address"><?php //echo $data[0]['address']; ?></textarea></td>-->
                    <td>
                    <fieldset>    
                        <!-- Textarea -->
                    <div class="control-group">
                      <!--<label class="control-label" for="add_street">Street</label>-->
                      <div class="controls">                     
                          <textarea id="add_street" name="add_street" class="input-medium form-control" placeholder="Street" name="add_street" required=""><?php echo $data[0]['add_street']; ?>  </textarea>
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                      <!--<label class="control-label" for="add_area">Area</label>-->
                      <div class="controls">
                        <input id="add_area" name="add_area" placeholder="Area" class="input-medium form-control" value="<?php echo $data[0]['add_area']; ?>" required="" type="text">
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                      <!--<label class="control-label" for="add_landmark">Landmark</label>-->
                      <div class="controls">
                        <input id="add_landmark" name="add_landmark" placeholder="Landmark" class="input-medium form-control" value="<?php echo $data[0]['add_landmark']; ?>" required="" type="text">
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                      <!--<label class="control-label" for="area_city">City</label>-->
                      <div class="controls">
                          <input id="add_city" name="add_city" placeholder="City" class="input-medium form-control" required=""  value="Pune" type="text" readonly="">
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="control-group">
                      <!--<label class="control-label" for="area_zipcode">Pincode</label>-->
                      <div class="controls">
                        <input id="add_zipcode" name="add_zipcode" placeholder="Zincode" class="input-medium form-control" value="<?php echo $data[0]['add_zipcode']; ?>" type="number" required>

                      </div>
                    </div>

                    </fieldset>
                  </td>
                  </tr>
                  <tr id="idErrAddress"></tr>        
                
                </div>
              </div>
            </table> 
          </div>
          <div id="step-3"> 
              <table class='table table-bordered'>
                <tr>
                   
                    <td colspan="2">
                       
                        <div id="edituser">
                            <button type="button" class="btn btn-primary" name="btn-update" id="idBookAppYour" name="register">
                             <span class="glyphicon glyphicon-edit"></span> Confirm Appointment
                            </button>
                            
                        </div> 
                    
                    </td>
                        
                     
                     
                    </tr>
                  </table>
          </div>
         
      </div>

                       
                                
                         
                            </table>
                      </form>

