<?php
require_once 'core/init.php';
 if ( Input::exists() ) {
                    if( Token::check( Input::get( 'token' ) ) ) {
                 
                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                        'username' => array('require' =>true),
                                        'password'=> array( 'require' => true )));
                                    
                                    if( $validation->passed() ) {
                                        //log user in
                                        $user = new User();
                                        $login = $user->login( Input::get( 'username' ), Input::get( 'password' ) );
                                        if( $login ) {  Redirect::to("lab.php"); } else { echo "Sorry Login failed"; }
                                    } else {
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                    }
                    }
            
            }     