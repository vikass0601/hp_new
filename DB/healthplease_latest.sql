-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2015 at 06:44 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `healthplease`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE IF NOT EXISTS `appointments` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `loc_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `slot_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `user_id`, `loc_id`, `lab_id`, `date`, `slot_id`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_deleted`, `status_id`) VALUES
(1, 2, 9, 1, '2015-05-12 00:00:00', 1, 0, '2015-06-11 04:36:32', 0, '2015-06-11 04:36:32', 0, 2),
(2, 2, 8, 1, '2015-05-12 00:00:00', 1, 0, '2015-06-11 04:56:38', 0, '2015-06-11 04:56:38', 0, 2),
(3, 2, 8, 1, '2015-05-12 00:00:00', 1, 0, '2015-06-11 04:57:32', 2, '2015-06-14 05:29:19', 0, 3),
(4, 2, 8, 1, '0000-00-00 00:00:00', 1, 0, '2015-06-11 04:58:55', 2, '2015-06-13 10:01:39', 0, 2),
(5, 2, 8, 1, '2015-06-16 00:00:00', 1, 0, '2015-06-11 05:00:14', 2, '2015-06-14 05:28:31', 0, 3),
(6, 2, 8, 1, '0000-00-00 00:00:00', 1, 2, '2015-06-11 05:03:03', 0, '2015-06-11 05:03:03', 0, 2),
(7, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 05:06:23', 0, '2015-06-11 05:06:23', 0, 2),
(8, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 05:08:31', 2, '2015-06-14 05:27:19', 0, 3),
(9, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 05:13:41', 2, '2015-06-11 05:13:41', 0, 2),
(10, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 05:13:59', 2, '2015-06-14 05:20:04', 0, 3),
(11, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 05:14:29', 2, '2015-06-14 05:30:13', 0, 3),
(12, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 05:16:46', 2, '2015-06-13 10:00:44', 0, 2),
(13, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 05:17:52', 2, '2015-06-14 05:30:22', 0, 3),
(14, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 05:19:44', 2, '2015-06-13 09:13:12', 0, 2),
(15, 2, 8, 1, '2015-06-16 00:00:00', 1, 0, '2015-06-11 05:33:40', 2, '2015-06-13 09:12:46', 2, 2),
(16, 2, 8, 1, '2015-06-16 00:00:00', 1, 0, '2015-06-11 05:46:28', 2, '2015-06-11 05:46:28', 2, 2),
(17, 2, 8, 1, '2015-06-16 00:00:00', 1, 0, '2015-06-11 05:46:37', 2, '2015-06-11 05:46:37', 2, 2),
(18, 2, 8, 1, '2015-06-16 00:00:00', 1, 0, '2015-06-11 05:48:29', 2, '2015-06-11 05:48:29', 2, 2),
(19, 2, 8, 1, '2015-06-16 00:00:00', 1, 0, '2015-06-11 05:48:32', 2, '2015-06-11 05:48:32', 2, 2),
(20, 2, 8, 1, '2015-06-16 00:00:00', 1, 0, '2015-06-11 05:51:46', 2, '2015-06-11 05:51:46', 2, 2),
(21, 2, 8, 1, '2015-06-16 00:00:00', 1, 2, '2015-06-11 07:04:57', 2, '2015-06-11 07:04:57', 2, 2),
(22, 2, 9, 1, '2015-06-18 00:00:00', 1, 2, '2015-06-11 07:44:27', 2, '2015-06-11 07:44:27', 0, 2),
(23, 2, 9, 1, '2015-06-18 00:00:00', 1, 2, '2015-06-11 07:44:49', 2, '2015-06-11 07:44:49', 0, 2),
(24, 2, 9, 1, '2015-06-18 00:00:00', 1, 2, '2015-06-11 07:47:18', 2, '2015-06-11 07:47:18', 0, 2),
(25, 2, 9, 1, '2015-06-18 00:00:00', 1, 2, '2015-06-11 07:52:02', 2, '2015-06-11 07:52:02', 0, 2),
(26, 2, 10, 1, '2015-06-19 00:00:00', 1, 2, '2015-06-11 08:01:14', 2, '2015-06-13 08:58:06', 0, 2),
(27, 2, 10, 1, '2015-06-19 00:00:00', 1, 2, '2015-06-11 08:03:15', 2, '2015-06-11 08:03:15', 0, 2),
(28, 2, 9, 1, '2015-06-24 00:00:00', 1, 2, '2015-06-12 04:06:06', 2, '2015-06-12 04:06:06', 2, 2),
(29, 2, 9, 1, '2015-06-27 00:00:00', 1, 2, '2015-06-12 04:07:27', 2, '2015-06-13 09:00:41', 0, 2),
(30, 2, 9, 1, '2015-06-26 00:00:00', 1, 2, '2015-06-12 04:10:26', 2, '2015-06-12 04:10:26', 2, 2),
(31, 2, 10, 1, '2015-06-27 00:00:00', 1, 2, '2015-06-12 04:12:15', 2, '2015-06-12 04:12:15', 2, 2),
(32, 2, 9, 1, '2015-06-25 00:00:00', 1, 2, '2015-06-12 04:14:10', 2, '2015-06-12 04:14:10', 2, 2),
(33, 2, 9, 1, '2015-06-25 00:00:00', 1, 2, '2015-06-12 04:16:46', 2, '2015-06-12 04:16:46', 2, 2),
(34, 2, 9, 1, '2015-06-20 00:00:00', 1, 2, '2015-06-12 04:17:46', 2, '2015-06-12 04:17:46', 2, 3),
(35, 2, 9, 1, '2015-06-26 00:00:00', 1, 2, '2015-06-12 04:19:53', 2, '2015-06-12 04:19:53', 2, 3),
(36, 2, 10, 1, '2015-06-26 00:00:00', 1, 2, '2015-06-12 04:21:44', 2, '2015-06-12 04:21:44', 2, 3),
(37, 2, 9, 1, '2015-06-20 00:00:00', 1, 2, '2015-06-12 04:55:53', 2, '2015-06-12 04:55:53', 2, 3),
(38, 2, 8, 1, '2015-06-19 00:00:00', 1, 2, '2015-06-12 07:48:30', 2, '2015-06-12 07:48:30', 2, 3),
(39, 2, 8, 1, '2015-06-26 00:00:00', 1, 2, '2015-06-13 09:38:02', 2, '2015-06-13 09:38:02', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `appointment_status`
--

CREATE TABLE IF NOT EXISTS `appointment_status` (
`id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment_status`
--

INSERT INTO `appointment_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Cancelled'),
(3, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `appointment_tests`
--

CREATE TABLE IF NOT EXISTS `appointment_tests` (
`id` int(11) NOT NULL,
  `appointment_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment_tests`
--

INSERT INTO `appointment_tests` (`id`, `appointment_id`, `test_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(21, 21, 1),
(22, 22, 2),
(23, 23, 2),
(24, 24, 2),
(25, 25, 3),
(26, 26, 4),
(27, 27, 4),
(28, 28, 2),
(29, 29, 3),
(30, 30, 1),
(31, 31, 4),
(32, 32, 3),
(33, 33, 12),
(34, 34, 2),
(35, 35, 23),
(36, 36, 3),
(37, 37, 5),
(38, 38, 2),
(39, 39, 2);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `permissions` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`) VALUES
(1, 'Standard user', '{"user":"1"}'),
(2, 'Administrator', '{"admin":"1"}');

-- --------------------------------------------------------

--
-- Table structure for table `labs`
--

CREATE TABLE IF NOT EXISTS `labs` (
`id` int(11) NOT NULL,
  `lab_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lab_location_id` int(11) NOT NULL,
  `lab_details` varchar(250) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labs`
--

INSERT INTO `labs` (`id`, `lab_name`, `lab_location_id`, `lab_details`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_deleted`, `deleted_by`, `deleted_on`) VALUES
(1, 'HealthPlease', 10, 'Pathology lab', NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
`id` int(11) NOT NULL,
  `location_name` varchar(500) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location_name`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_valid`) VALUES
(7, 'Vishranwadi', 1, '2015-05-25 00:00:00', 1, '2015-05-25 00:00:00', 0),
(8, 'Viman Nagar', 1, '2015-05-25 00:00:00', 1, '2015-05-25 00:00:00', 0),
(9, 'Shastrinagar', 1, '2015-05-25 00:00:00', 1, '2015-05-25 00:00:00', NULL),
(10, 'Kalyaninagar', 1, '2015-05-25 00:00:00', 1, '2015-05-25 00:00:00', 0),
(11, 'Koregaon Park', 1, '2015-05-25 00:00:00', 1, '2015-05-25 00:00:00', 0),
(12, 'Koregaon Park ext', 1, '2015-05-25 00:00:00', 1, '2015-05-25 00:00:00', 0),
(13, 'Tingrenagar', 1, '2015-05-25 00:00:00', 1, '2015-05-25 00:00:00', 0),
(14, 'Dhanori', 1, '2015-05-25 00:00:00', 1, '2015-05-25 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE IF NOT EXISTS `relations` (
`id` int(11) NOT NULL,
  `appointment_id` int(11) NOT NULL,
  `relation` varchar(50) DEFAULT NULL,
  `r_name` varchar(100) DEFAULT NULL,
  `r_email` varchar(50) DEFAULT NULL,
  `r_phone` varchar(20) DEFAULT NULL,
  `r_gender` int(11) DEFAULT NULL,
  `r_doctor` varchar(250) DEFAULT NULL,
  `r_address` varchar(500) DEFAULT NULL,
  `r_dob` varchar(10) DEFAULT NULL,
  `r_age` smallint(6) DEFAULT NULL,
  `created_by` int(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updted_by` int(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_valid` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `appointment_id`, `relation`, `r_name`, `r_email`, `r_phone`, `r_gender`, `r_doctor`, `r_address`, `r_dob`, `r_age`, `created_by`, `created_date`, `updted_by`, `updated_date`, `is_valid`) VALUES
(1, 14, 'dsfdsf', 'sdfdsf', 'vikasjhsdgf', '2364563', 1, 'jhsdfhjds', '', '1990-01-10', 25, 2, '2015-06-11 05:19:44', 2, '2015-06-11 05:19:44', 0),
(2, 22, 'brother', 'Vikas Sharma', 'vikas@mail.com', '9985985698', 0, 'abcd', '', '1989-01-05', 26, 2, '2015-06-11 07:44:27', 2, '2015-06-11 07:44:27', 0),
(3, 23, 'brother', 'Vikas Sharma', 'vikas@mail.com', '9985985698', 1, 'abcd', '', '1989-01-05', 26, 2, '2015-06-11 07:44:50', 2, '2015-06-11 07:44:50', 0),
(4, 24, 'brother', 'Vikas Sharma', 'vikas@mail.com', '9985985698', 1, 'abcd', '', '1989-01-05', 26, 2, '2015-06-11 07:47:18', 2, '2015-06-11 07:47:18', 0),
(5, 25, 'Brother', 'Vikas Sharma', 'jhdshf@mail.com', '234324', 1, 'jsdfj', '', '1989-01-13', 26, 2, '2015-06-11 07:52:02', 2, '2015-06-11 07:52:02', 0),
(6, 26, 'kjdfhgkjd', 'djfhgjfkdh', 'jsehfjksd', 'kjhsdjhsdk', 1, 'kjsdhfjk', '', '1989-01-20', 26, 2, '2015-06-11 08:01:14', 2, '2015-06-11 08:01:14', 0),
(7, 27, 'kjdfhgkjdfhgjk', 'kjdhgkdshgdh', 'dkjfhgdkjfhgdjfk', 'kjsdhgjkdh', 1, 'jkhfjhdsjkhfdgjk', '', '1989-01-19', 26, 2, '2015-06-11 08:03:15', 2, '2015-06-11 08:03:15', 0),
(8, 29, 'skjdhskjd', 'vikas sharmjas', 'kjsdhfjkds', 'kjdshhfjsk', 1, 'kjdfhgjds', '', '1989-01-19', 26, 2, '2015-06-12 04:07:27', 2, '2015-06-12 04:07:27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `slabs`
--

CREATE TABLE IF NOT EXISTS `slabs` (
`id` int(11) NOT NULL,
  `slab_name` varchar(255) NOT NULL,
  `slab_duration` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slots`
--

CREATE TABLE IF NOT EXISTS `slots` (
  `id` int(11) NOT NULL DEFAULT '0',
  `slab_id` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `lab_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
`id` int(11) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `email_id` varchar(50) NOT NULL,
  `contact_no` bigint(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `first_name`, `last_name`, `email_id`, `contact_no`) VALUES
(23, 'pradeep', 'bhodhkhe', 'pradeep@gmail.com', 9876543210),
(24, 'sohan', 'mahamune', 'sohan@gmail.com', 9874563210),
(25, 'john', 'doe', 'john@someone.com', 9778456123),
(62, 'vikas', 'sharma', 'vikas@gmail.com', 9975774913),
(66, 'vikas', 'sharma', 'vikas2000@gmail.com', 9975774913),
(70, 'vikas', 'sharma', 'vikas99@gmail.com', 9975774913);

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
`id` int(11) NOT NULL,
  `test_name` varchar(255) NOT NULL,
  `test_cat_id` int(10) NOT NULL COMMENT 'Foreign key for the category',
  `test_charge` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` smallint(6) NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `test_name`, `test_cat_id`, `test_charge`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_deleted`) VALUES
(1, 'HAEMOGRAM/CBC', 1, 180, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(2, 'HAEMOGRAM & ESR', 1, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(3, 'R.B.C/W.B.C', 1, 80, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(4, 'E.S.R', 1, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(5, 'P.C.V', 1, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(6, 'BT & CT', 1, 180, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(7, 'PROTHROMBIN TIME', 1, 250, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(8, 'ABSOLUTE EOSINOPHIL COUNT', 1, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(9, 'PLATELET COUNT', 1, 120, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(10, 'PORTAL HAEMPGLOBIN PERCENT', 1, 0, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(11, 'OSMOTIC FRAGILITY', 1, 0, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(12, 'GLYCOSELATED HB PERCENT', 1, 550, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(13, 'RH ANTIBODY TITRE', 1, 300, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(14, 'BLOOD GROUP', 1, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(15, 'PBS M.P.', 1, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(16, 'M.P.', 1, 250, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(17, 'WIDAL', 2, 300, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(18, 'V.D.R.L', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(19, 'TITRE PER DILUTION', 2, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(20, 'IIIV (I & II)', 2, 350, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(21, 'WESTERN BLOT', 2, 2200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(22, 'R.A. TEST (SCREEN)', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(23, 'TITRE PER DILUTION', 2, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(24, 'C-REACTIVPROTEIN', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(25, 'A.S.O (SCREEN)', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(26, 'AUSTRALIA Anyigen', 2, 250, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(27, 'TUBFRCULIN TEST', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(28, 'DIRECT COOMS TEST', 2, 450, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(29, 'DENGUE', 2, 1200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(30, 'T3*\r\n', 3, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(31, 'T4*', 3, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(32, 'TSH*', 3, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(33, 'LH*', 3, 300, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(34, 'FSH*', 3, 300, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(35, 'LDH*', 3, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(36, 'TFT*', 3, 750, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(37, 'PROLACTINE*', 3, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(38, 'ANC PROLIFaE*', 3, 1, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(39, 'FreeT3, FreeT4, TSH*', 3, 850, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(40, 'CD4*', 3, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(41, 'CD4/CD8', 3, 1500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(42, 'BLOOD & URINE SUGAR (F & PP)', 4, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(43, 'BLOOD SUGAR (''R)', 4, 80, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(44, 'SERUM CHOLESTROL', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(45, 'BLOOD UREA', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(46, 'SERUM TRIGLYCERIDES', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(47, 'LIPIDS (LDL & VLDL)', 4, 650, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(48, 'SERUM ELECTROLYTES', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(49, 'SERUM BILLIRUBIN', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(50, 'SGPT', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(51, 'SGOT', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(52, 'SERUM LDII*', 4, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(53, 'ALK PROSPHATASE', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(54, 'SERUM URIC ACID', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(55, 'SERUM CREATININE', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(56, 'SERUM PROTEINS', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(57, 'SERUM CALCIUM', 4, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(58, 'SERUM ACID PHOSPHATASE WITH P.F.', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(59, 'SERUM AMYLASE', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(60, 'SERUM HDL CHOLESTEROL', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(61, 'PSA*', 4, 800, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(62, 'CA 125*', 4, 1000, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(63, 'ACCP*', 4, 1200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(64, 'Vit B3*', 4, 1500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(65, 'Vit B12*', 4, 700, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(66, 'BLAB27*', 4, 1800, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(67, 'FIRRINOGEN', 4, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(68, 'CPK', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(69, 'LFT', 4, 450, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(70, 'MAGNESIUM', 4, 350, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(71, 'PHOSPHORUS', 4, 350, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(72, 'CULTURE & SENSITIVITY', 5, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(73, 'GRAM STANING', 5, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(74, 'AFB STAININ', 5, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(75, 'DIPHTHERIA STAINING*', 5, 350, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(76, 'AFB CULTURE*', 5, 6000, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(77, 'URNE ROUTINE', 6, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(78, 'STOOL ROUTINE COB', 6, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(79, 'SEMEN ANLYSIS', 6, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(80, 'PREGNANCY TEST', 6, 120, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(81, 'PREGNANCY TEST ELISA', 6, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(82, 'FLUID ASPIRATED', 6, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(83, 'TRIPLE TEST*', 6, 2500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(84, 'CPK MB', 6, 450, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(85, 'D.N.A*', 7, 600, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(86, 'A.N.A*', 7, 600, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(87, 'ALPHA FETO-PROTEIN', 7, 0, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(88, 'RENAL CALCULUS ANALYSIS', 7, 0, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(89, 'ANCA*', 7, 1200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(90, 'TORCHI - 5 PARA*', 7, 1800, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(91, 'TORCHI - 8 PARA*', 7, 2000, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `test_categories`
--

CREATE TABLE IF NOT EXISTS `test_categories` (
  `id` int(11) DEFAULT NULL,
  `category_name` varchar(500) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `test_categories`
--

INSERT INTO `test_categories` (`id`, `category_name`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_valid`) VALUES
(1, 'HEAMATOLOGY', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(2, 'SEROLOGY', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(3, 'HORMONAL TEST	', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(4, 'BIOCHEMISTRY	', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(5, 'BACTERIOLOGY	', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(6, 'CLINICAL PATHOLOGY', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(7, 'OTHER TEST', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

CREATE TABLE IF NOT EXISTS `times` (
`id` int(11) NOT NULL,
  `time` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `times`
--

INSERT INTO `times` (`id`, `time`) VALUES
(1, '7:00'),
(2, '7:30'),
(3, '8:00'),
(4, '8:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `gender` smallint(6) NOT NULL DEFAULT '1',
  `dob` varchar(200) NOT NULL,
  `age` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` varchar(250) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` varchar(250) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login_id`, `name`, `contact`, `address`, `gender`, `dob`, `age`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_deleted`) VALUES
(1, 1, 'Ishtiyaque Mohammad', '9876543210', 'fatima Nagar, Pune', 1, '05/20/2015', 21, 0, '2015-05-28 07:37:23pm', 0, '2015-05-28 07:37:23pm', 0),
(2, 2, 'demo', '12345612', 'hsdgfjhsfhj', 1, '05/06/2015', 21, 0, '2015-05-31 08:15:51am', 0, '2015-05-31 08:15:51am', 0),
(3, 3, 'VIKAS SHARMA', '12312121', 'sdjhfghjsd', 1, 'skjdhf', 25, 0, '2015-06-13 03:26:54pm', 0, '2015-06-13 03:26:54pm', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users1`
--

CREATE TABLE IF NOT EXISTS `users1` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `joined` datetime NOT NULL,
  `group` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users1`
--

INSERT INTO `users1` (`id`, `username`, `password`, `salt`, `name`, `joined`, `group`) VALUES
(1, 'ishti512@gmail.com', 'b152c443cd940a4e75a92516a2bae256675df78be46c06ea34392f2fb831e6fe', '…X›ÓŒ÷ìÁí&^ìa:oI‚îõ\r @_éTgB‡S', 'Ishtiyaque Mohammad', '2015-05-28 19:37:23', 1),
(2, 'sweetrook@gmail.com', 'd3ca6036c39c292561b8adf4dccd554d8a16a2e3eb6c73c058430d59554fca13', 'Dž=°á{ÄW`u¬Â¯dXV€1š6%D©yg2+V', 'demo', '2015-05-31 08:15:50', 1),
(3, 'vikas', '39b7018b7591f29b019cfdc7b0937b5c237ca2d4944db4ab78cb0b31326993b3', 'ßE125üzøh×Î,“ÜŽ†t^Ï¡í‚‚ªöRÍé	', 'VIKAS SHARMA', '2015-06-13 15:26:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment_status`
--
ALTER TABLE `appointment_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment_tests`
--
ALTER TABLE `appointment_tests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labs`
--
ALTER TABLE `labs`
 ADD PRIMARY KEY (`id`), ADD KEY `lab_location_id` (`lab_location_id`), ADD KEY `lab_location_id_2` (`lab_location_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slabs`
--
ALTER TABLE `slabs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slots`
--
ALTER TABLE `slots`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `times`
--
ALTER TABLE `times`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users1`
--
ALTER TABLE `users1`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access`
--
ALTER TABLE `user_access`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `appointment_status`
--
ALTER TABLE `appointment_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `appointment_tests`
--
ALTER TABLE `appointment_tests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `labs`
--
ALTER TABLE `labs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `slabs`
--
ALTER TABLE `slabs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users1`
--
ALTER TABLE `users1`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `labs`
--
ALTER TABLE `labs`
ADD CONSTRAINT `labs_id_fk` FOREIGN KEY (`lab_location_id`) REFERENCES `locations` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
