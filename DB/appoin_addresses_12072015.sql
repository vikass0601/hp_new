/*
SQLyog Community v11.26 (64 bit)
MySQL - 5.5.43-0ubuntu0.14.04.1 : Database - healthplease
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`healthplease` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `healthplease`;

/*Table structure for table `appoin_addresses` */

DROP TABLE IF EXISTS `appoin_addresses`;

CREATE TABLE `appoin_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) NOT NULL,
  `add_street` varchar(250) DEFAULT NULL,
  `add_area` varchar(250) DEFAULT NULL,
  `add_landmark` varchar(250) DEFAULT NULL,
  `add_city` varchar(50) DEFAULT 'Pune',
  `add_zipcode` varchar(10) DEFAULT NULL,
  `created_by` int(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
