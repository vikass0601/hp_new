-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2015 at 03:33 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hp`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `loc_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `slot_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `appointment_tests`
--

CREATE TABLE IF NOT EXISTS `appointment_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`) VALUES
(1, 'Standard user', '{"user":"1"}'),
(2, 'Administrator', '{"admin":"1"}');

-- --------------------------------------------------------

--
-- Table structure for table `labs`
--

CREATE TABLE IF NOT EXISTS `labs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lab_location_id` int(11) NOT NULL,
  `lab_details` varchar(250) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lab_location_id` (`lab_location_id`),
  KEY `lab_location_id_2` (`lab_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(500) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `slabs`
--

CREATE TABLE IF NOT EXISTS `slabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slab_name` varchar(255) NOT NULL,
  `slab_duration` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `slots`
--

CREATE TABLE IF NOT EXISTS `slots` (
  `id` int(11) NOT NULL DEFAULT '0',
  `slab_id` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `lab_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `email_id` varchar(50) NOT NULL,
  `contact_no` bigint(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_name` varchar(255) NOT NULL,
  `test_cat_id` int(10) NOT NULL COMMENT 'Foreign key for the category',
  `test_charge` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` smallint(6) NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `test_cat_id` (`test_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `test_name`, `test_cat_id`, `test_charge`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_deleted`) VALUES
(1, 'HAEMOGRAM/CBC', 1, 180, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(2, 'HAEMOGRAM & ESR', 1, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(3, 'R.B.C/W.B.C', 1, 80, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(4, 'E.S.R', 1, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(5, 'P.C.V', 1, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(6, 'BT & CT', 1, 180, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(7, 'PROTHROMBIN TIME', 1, 250, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(8, 'ABSOLUTE EOSINOPHIL COUNT', 1, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(9, 'PLATELET COUNT', 1, 120, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(10, 'PORTAL HAEMPGLOBIN PERCENT', 1, 0, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(11, 'OSMOTIC FRAGILITY', 1, 0, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(12, 'GLYCOSELATED HB PERCENT', 1, 550, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(13, 'RH ANTIBODY TITRE', 1, 300, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(14, 'BLOOD GROUP', 1, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(15, 'PBS M.P.', 1, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(16, 'M.P.', 1, 250, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(17, 'WIDAL', 2, 300, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(18, 'V.D.R.L', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(19, 'TITRE PER DILUTION', 2, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(20, 'IIIV (I & II)', 2, 350, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(21, 'WESTERN BLOT', 2, 2200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(22, 'R.A. TEST (SCREEN)', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(23, 'TITRE PER DILUTION', 2, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(24, 'C-REACTIVPROTEIN', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(25, 'A.S.O (SCREEN)', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(26, 'AUSTRALIA Anyigen', 2, 250, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(27, 'TUBFRCULIN TEST', 2, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(28, 'DIRECT COOMS TEST', 2, 450, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(29, 'DENGUE', 2, 1200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(30, 'T3*\r\n', 3, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(31, 'T4*', 3, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(32, 'TSH*', 3, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(33, 'LH*', 3, 300, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(34, 'FSH*', 3, 300, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(35, 'LDH*', 3, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(36, 'TFT*', 3, 750, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(37, 'PROLACTINE*', 3, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(38, 'ANC PROLIFaE*', 3, 1, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(39, 'FreeT3, FreeT4, TSH*', 3, 850, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(40, 'CD4*', 3, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(41, 'CD4/CD8', 3, 1500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(42, 'BLOOD & URINE SUGAR (F & PP)', 4, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(43, 'BLOOD SUGAR (''R)', 4, 80, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(44, 'SERUM CHOLESTROL', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(45, 'BLOOD UREA', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(46, 'SERUM TRIGLYCERIDES', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(47, 'LIPIDS (LDL & VLDL)', 4, 650, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(48, 'SERUM ELECTROLYTES', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(49, 'SERUM BILLIRUBIN', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(50, 'SGPT', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(51, 'SGOT', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(52, 'SERUM LDII*', 4, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(53, 'ALK PROSPHATASE', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(54, 'SERUM URIC ACID', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(55, 'SERUM CREATININE', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(56, 'SERUM PROTEINS', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(57, 'SERUM CALCIUM', 4, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(58, 'SERUM ACID PHOSPHATASE WITH P.F.', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(59, 'SERUM AMYLASE', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(60, 'SERUM HDL CHOLESTEROL', 4, 130, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(61, 'PSA*', 4, 800, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(62, 'CA 125*', 4, 1000, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(63, 'ACCP*', 4, 1200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(64, 'Vit B3*', 4, 1500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(65, 'Vit B12*', 4, 700, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(66, 'BLAB27*', 4, 1800, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(67, 'FIRRINOGEN', 4, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(68, 'CPK', 4, 400, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(69, 'LFT', 4, 450, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(70, 'MAGNESIUM', 4, 350, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(71, 'PHOSPHORUS', 4, 350, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(72, 'CULTURE & SENSITIVITY', 5, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(73, 'GRAM STANING', 5, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(74, 'AFB STAININ', 5, 150, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(75, 'DIPHTHERIA STAINING*', 5, 350, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(76, 'AFB CULTURE*', 5, 6000, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(77, 'URNE ROUTINE', 6, 100, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(78, 'STOOL ROUTINE COB', 6, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(79, 'SEMEN ANLYSIS', 6, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(80, 'PREGNANCY TEST', 6, 120, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(81, 'PREGNANCY TEST ELISA', 6, 200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(82, 'FLUID ASPIRATED', 6, 500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(83, 'TRIPLE TEST*', 6, 2500, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(84, 'CPK MB', 6, 450, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(85, 'D.N.A*', 7, 600, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(86, 'A.N.A*', 7, 600, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(87, 'ALPHA FETO-PROTEIN', 7, 0, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(88, 'RENAL CALCULUS ANALYSIS', 7, 0, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(89, 'ANCA*', 7, 1200, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(90, 'TORCHI - 5 PARA*', 7, 1800, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0),
(91, 'TORCHI - 8 PARA*', 7, 2000, 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `test_categories`
--

CREATE TABLE IF NOT EXISTS `test_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(500) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_valid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `test_categories`
--

INSERT INTO `test_categories` (`id`, `category_name`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_valid`) VALUES
(1, 'HEAMATOLOGY', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(2, 'SEROLOGY', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(3, 'HORMONAL TEST	', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(4, 'BIOCHEMISTRY	', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(5, 'BACTERIOLOGY	', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(6, 'CLINICAL PATHOLOGY', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1),
(7, 'OTHER TEST', 1, '2015-05-24 00:00:00', 1, '2015-05-24 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

CREATE TABLE IF NOT EXISTS `times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `times`
--

INSERT INTO `times` (`id`, `time`) VALUES
(1, '7:00'),
(2, '7:30'),
(3, '8:00'),
(4, '8:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` int(11) NOT NULL,
  `address` varchar(250) NOT NULL,
  `gender` smallint(6) NOT NULL DEFAULT '1',
  `dob` datetime NOT NULL,
  `age` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login_id`, `name`, `contact`, `address`, `gender`, `dob`, `age`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_deleted`) VALUES
(1, 42, 'john Cena', 99, 'sdfsd', 1, '2015-06-06 00:00:00', 25, NULL, NULL, NULL, NULL, NULL),
(2, 43, 'Gourav Saraf', 545655564, 'ddsfsdds', 1, '2015-06-06 00:00:00', 25, 0, '2015-05-01 08:53:23', 43, '2015-05-01 11:52:25', NULL),
(3, 44, 'vikas', 0, '', 0, '0000-00-00 00:00:00', 0, 0, '2015-05-01 09:17:42', 0, '2015-05-01 09:17:42', NULL),
(4, 45, 'Laxmikant', 2147483647, 'Viman Nagar pune', 1, '0000-00-00 00:00:00', 26, 0, '2015-05-01 03:56:45', 0, '2015-05-01 03:56:45', NULL),
(5, 46, 'Laxmikant', 2147483647, 'Hi', 1, '0000-00-00 00:00:00', 25, 0, '2015-05-01 09:11:11', 0, '2015-05-01 09:11:11', NULL),
(6, 47, 'Lokesh', 2147483647, 'Viman Nagar', 1, '0000-00-00 00:00:00', 25, 0, '2015-05-02 09:20:26', 0, '2015-05-02 09:20:26', NULL),
(7, 48, 'Laxmikant Killekar', 2147483647, 'Viman Nagar', 1, '0000-00-00 00:00:00', 25, 0, '2015-05-13 09:04:26', 0, '2015-05-13 09:04:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users1`
--

CREATE TABLE IF NOT EXISTS `users1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(5000) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `name` varchar(5) NOT NULL,
  `joined` datetime NOT NULL,
  `group` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `users1`
--

INSERT INTO `users1` (`id`, `username`, `password`, `salt`, `name`, `joined`, `group`) VALUES
(49, 'john', '8426809c94a0a0dd9c6e4dee33de61a1fb9449a1eaf704a90e0565d334b7157e', 'ÌëMM''²n×HÇr`éqäÿpßËä\0„<9¤Òz­Co', 'john ', '2015-05-01 08:00:05', 1),
(50, 'qwerty', '9949512487144c81881315ac426a7e72d6166964843225e456ac6a3c3305ab1f', '2j¶P²\rû¾ƒY×ä¬xÅ¾-¹iàþf9®(y—Sÿ', 'Vikas', '2015-05-01 08:53:23', 2),
(51, 'vikas', '1855b3005d7fa317d9414dc92493073616867f213a6c4a7358c05611d3fe13c1', 'Ç „?\\OÛã‹¼Ä8¶`(ìý‚¹˜ñ]Í¿ÿà,C²', 'vikas', '2015-05-01 09:17:42', 1),
(52, 'lk', 'b588f7d0578d5fc00735fe15fd63791c408911a75f552e5e0e2edf828fc3f004', 'B/÷º×Ú#Ó+¶aFzØgæaPˆ{m1÷Ä', 'Laxmi', '2015-05-01 15:56:45', 1),
(53, 'lt@gmail.com', 'b240bc6feefcaf2fed77728cc180e6823c54a7331b9045bbd41d42119bd4e832', '_Ñ&8K\ZD¥¹…£Çcš4¢wcSYð®²ýð®,ãx', 'Laxmi', '2015-05-01 21:11:11', 1),
(54, 'laxmikantkillekar@gmail.com', '31c165d7a89264d4c53f882edf2653e6de415005cda913afcea8aed5e12e3294', 'ÀŸD°\r9`~¤È0ˆ1¨¶„+™¼À ƒ*öu…f>', 'Lokes', '2015-05-02 21:20:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `labs`
--
ALTER TABLE `labs`
  ADD CONSTRAINT `labs_id_fk` FOREIGN KEY (`lab_location_id`) REFERENCES `locations` (`id`);

--
-- Constraints for table `tests`
--
ALTER TABLE `tests`
  ADD CONSTRAINT `tests_ibfk_1` FOREIGN KEY (`test_cat_id`) REFERENCES `test_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
