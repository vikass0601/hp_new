<form method='post' id="formBookAppOther" action="userController.php" class="classOCAForm">
 
                            <table class='table table-bordered'>
                                <tr id="idUserName">
                                    <td>Patient Name*</td>
                                    <td><input type='text' name='pName' id='pName' class='form-control' value="" required></td>
                                        
                                </tr>
                                <tr id="idErrName"></tr>
                                <tr id="idUserRelation">
                                    <td>Patient Relation*</td>
                                    <td><input type='text' name='pRelation' id='pRelation' class='form-control' value="" required></td>
                                        
                                </tr>                            
                                <tr id="idRelationDiv" ></tr>
                                
                                


                                <tr>
                                    <?php $testName = getAllTestsByIds( $_GET['testName']); ?>
                                    <td>Patient Test*</td>
                                    <td>
                                        <select id="idTest" name="r_test[]" class="form-control selectpicker classTestSelect" multiple="" style="width:500px">
                                         <?php  foreach( $testName as $key => $value ){?>
                                                       <option value="<?php echo $testName[$key]['id']; ?>" selected=""><?php echo $testName[$key]['test_name']; ?></option>    
                                                   <?php } ?>
                                                  <?php
                                                foreach ($aAllTests as $key => $value) {
                                                  echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                                }
                                              ?>
                                        </select>
                                    </td>
                                    <!--<input type="hidden" name="r_test" value="<?php// echo $_GET['testName']; ?>" ?>-->
                                    <!--<td><input type='text' name='pTest' id='pTest' class='form-control' value="<?php //echo $testName[0]['test_name']; ?>" required></td>-->                      
                                </tr>
                                <tr id="idErrTest"></tr>

                                <tr>
                                    <?php $locName = getAllLocationsById($_GET['locName']);?>
                                    <td>Patient Location</td>
                                    <td>
                                        <select id="idLocation" name="r_location" class="form-control classLocationSelect" style="width:500px">
                                        <option value='<?php echo $_GET['locName']; ?>'><?php echo $locName['location_name'];?></option>
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                    </td>  
                                    <!--<input type="hidden" name="r_location" value="<?php// echo $_GET['locName']; ?>" ?>-->
                                    <!--<td><input type='text' name='pLocation' id='pLoction' class='form-control' value="<?php //echo $locName['location_name'];?>" required></td>-->                      
                                </tr>
                                <tr id="idErrLocation"></tr>

                                <tr>
                                    <td>Patient Email*</td>
                                    <td><input type='email' name='pEmail' id='pEmail' class='form-control' value="" required ></td>
                                </tr>
                                <tr id="idErrEmail">
                                </tr>  
                                <tr  id="idErrEmailAddress">
                                </tr>  
                                <tr>
                                    <td>Appointment Date*</td>
                                    <input type="hidden" id="app_date" name="r_appointmentDate" value="<?php echo $_GET['appDate']; ?>" ?>
                                    
                                    <td><input type='text' name='pAppDate' id='pAppDate' class='form-control' value="<?php echo $_GET['appDate'];?>" required></td>
                                </tr>
                                <tr id="idErrAppDate"></tr>  
                                <tr>
                                    <td>Appointment Time*</td>
                                    <td><input type='text' name='pAppTime' id='pAppTime' class='form-control' value="<?php echo $_GET['appTime'];?>" required></td>
                                </tr>
                                <tr id="idErrApptime"></td></tr>  
                                <tr>
                                    <td>Doctor Name*</td>
                                    <td><input type='text' name='pDocName' id='pDocName' class='form-control' value="" required></td>
                                </tr>
                                <tr id="idErrDocName"></td></tr>         
                                <tr>
                                    <td>Patient Mobile No*</td>
                                    <td class="input-group"> <div class="input-group-addon">+91</div><input type='number' name='pContact' id='pContact' class='form-control' value="" style="position: relative;  z-index: 1;" maxlength="10" size="10" required></td>
                                </tr>
                                <tr id="idErrContact"></tr>        
                                <tr>
                                    <td>Patient Address*</td>
                                    <!--<td> <textarea class="form-control classAddress" rows="5" id="pAddress" name="pAddress"></textarea></td>-->
                                    <td>
                                    <fieldset>    
                                        <!-- Textarea -->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_street">Street</label>-->
                                      <div class="controls">                     
                                          <textarea id="add_street" name="add_street" class="input-medium form-control" placeholder="Street" name="add_street" required=""></textarea>
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_area">Area</label>-->
                                      <div class="controls">
                                        <input id="add_area" name="add_area" placeholder="Area" class="input-medium form-control" required="" type="text">
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_landmark">Landmark</label>-->
                                      <div class="controls">
                                        <input id="add_landmark" name="add_landmark" placeholder="Landmark" class="input-medium form-control" required="" type="text">
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="area_city">City</label>-->
                                      <div class="controls">
                                        <input id="add_city" name="add_city" placeholder="City" class="input-medium form-control" required=""  value="Pune" type="text" disabled>
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="area_zipcode">Pincode</label>-->
                                      <div class="controls">
                                        <input id="add_zipcode" name="add_zipcode" placeholder="Zincode" class="input-medium form-control" type="number">
                                      </div>
                                    </div>

                                    </fieldset>
                                </td>
                                </tr>
                                <tr id="idErrAddress"></tr>        
                                <tr>
                                    <td>Patient Gender*</td>
                                    <td>
                                     <div class="radio">
                                         <label><input type="radio" name="pGender" value="M" checked="checked">Male</label>
                                        <label><input type="radio" name="pGender" value="F">Female</label>
                                     </div>
                                </tr>
                                
                                <tr>
                                    <td>Patient DOB</td>
                                    <td>
                                    <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' name="pDob" id="pDob"  value="" class="form-control classdob" />
                                            <span class="input-group-addon" id="idSpanDob">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div>    
                                    </td>
                                </tr>
                                <tr id="idErrDob"></tr>
                                <tr>
                                    <td>Patient Age</td>
                                    <td><input type='text' value="" name='pAge' id='pAge' class='form-control' value="" required readonly></td>
                                </tr>
                                
                                <tr>
                                <td colspan="2">
                                    <div id="edituser">
                                        <button type="button" class="btn btn-primary" name="btn-update" id="idBookAppOther" name="register">
                                         <span class="glyphicon glyphicon-edit"></span>  Confirm Appointment
                                        </button>
                                    </div> 
                                </td>
                                </tr>
                            </table>
                      </form>