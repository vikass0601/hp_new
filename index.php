<?php
include('base-header.php');
include('page-header.php');
$aAllCat=getAllCategories();
$aAllTests=getAllTests();
$aAllLoc=getAllLocations();
?>
<script type="text/javascript">
    $(document).ready(function(){

    $("#idBookAppointmentOnLogin").on( "click", function(){

          var sTestName = $("#idTest").val();
          var sLocationName = $("#idLocation").val();
          if(sTestName == "select"){
              $("#idTestErr").fadeIn(1500);
              $("#idTestErr").html("<label style='color:red;'>Please select your test</label>")
              $("#idTestErr").fadeOut(1500);
              return false;
          }
           if(sLocationName == "select"){
              $("#idLocationErr").fadeIn(1500);
              $("#idLocationErr").html("<label style='color:red;'>Please select location of test</label>")
              $("#idLocationErr").fadeOut(1500);
              return false;
          }

          appointmentDate = $("#idAppointmentDate").val();
          if(appointmentDate == ""){
            $("#idAppDateErr").fadeIn(1500);
              $("#idAppDateErr").html("<label style='color:red;'>Please select date</label>")
              $("#idAppDateErr").fadeOut(1500);
             return false; 
          }

          var appTime  = $("#idAppTime").val();
          var appDetails = "";
          window.location.href="confirmAppointment.php?testName="+sTestName+"&locName="+sLocationName+"&appDate="+appointmentDate+"&appTime="+appTime;

    
    });


          

         /* On key press remove error alert */
    $("#username").keypress(function(){
      $("#idUsernameErr").html("");
    });
    $("#password").keypress(function(){
      $("#idPasswordErr").html("");
    });

});    
       
</script>
        <div class="classSliderHolder">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                      <div class="classHAbtTextWrapper">                         
                        <div class="classOSCFont classFrontText">
                          Your Convenience, Our Priority...<br/>
                          <div class="text-left"><img src="images/steps/3-steps.png" width="95%" class="classStepImg" /></div>
                          <div class="classBColor">Book your Appointment Now&nbsp;&nbsp;&nbsp;<span class="hvr-buzz-out"><i class="fa fa-hand-o-right "></i></span></div>
                        </div>
                       
                      </div>                     
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <!-- Form Div Start-->
                        <div class="auth">
                            <div id="big-form" class="well auth-box">
                            
                                <form method='post' id="on_form_appointment" action="userController.php">
                              
                                <fieldset>

                                  <!-- Form Name -->
                                  <legend><i class="fa fa-calendar"></i> Book Your Appointment</legend>
                                  <!-- Select Basic -->
                                  <div class="form-group">
                                    <label class=" control-label" for="selectbasic">Test</label>
                                   <div class="ui-widget">
                                        <select id="idTest" name="test" class="form-control selectpicker" multiple="multiple">
                                          <!--<option value="select" selected="">Select Your Test</option>-->
                                          <?php echo $value['test_name'];?>
                                                  <?php
                                                foreach ($aAllTests as $key => $value) {
                                                  echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                                }
                                              ?>
                                        </select>
                                      </div>
                                    <div class="" id="idTestErr"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class=" control-label" for="selectbasic">Your Location</label>
                                    <div class="ui-widget">
                                      <select id="idLocation" name="location" class="form-control">
                                        <option value='select'>Select Your Location</option>
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                    <div class="" id="idLocationErr"></div>
                                  </div>
                                  <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class=" control-label" for="appointmentDate">Appointment Date</label>  
                                            <div class="">
                                              <input id="idAppointmentDate" name="appointmentDate" placeholder="dd/mm/yyyy" class="form-control input-md" type="text">
                                            </div>
                                            <div class="" id="idAppDateErr"></div>
                                        </div> 
                                      <div class="form-group col-md-6">
                                        <label class=" control-label" for="appointmentTime">Appointment Time</label>
                                        <div class="">
                                         <input id="idAppTime" name="appointmentTime" placeholder="" class="form-control input-md" type="text" value="Between 7:00 am to 12:00pm" readonly/>
                                        </div>
                                      </div>
                                        
                                    </div>

                                                                  
                                 
                                  <div class="form-group">
                                    <div class="">
                                      <input type="hidden" name="bookAppFlag" id="idBookAppFlag" value="">
                                      <?php
                                      if($bLoginFlag){
                                        ?>
                                        <input type="button" id="idBookAppointmentOnLogin" name="bookAppointment" value="Book Appointment" class=" btn btn-success"></inputs>
                                        <?php
                                      }else{
                                      ?>
                                      <a href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"><input type="submit" id="idBookAppointment" name="bookAppointment" value="Book Appointment" class=" btn btn-success"></input></a>
                                       <?php 
                                      }
                                      ?>
                                      
                                      
                                      <span class="help-block">help</span>  
                                    </div>
                                  </div>

                                </fieldset>
                              </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Form Div End-->
                    </div>
                </div>    
            </div>
        </div>
        <div id="page-wrappera" class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Click To Request A Check Up</h3>
                    <div style="position:relative;">
                      <a href="#"><img src="images/1.jpg" width="100%"/>
                       <div class="classSupportForm">
                            <div class="classLabDetailWrapper">
                                <div class="classLabName"> Dhanwantary Path Lab </div>
                                <div class="classLabDetails">
                                    Dr. S.S Jagalpure  M.D. (Path) <br/>
                                    Geeta Complex Kasturaba Hgs Soc, Shop no-3, Behind Jakat Naka, Vishrantwadi Pune - 411015                               
                                </div>
                            </div>             

                        </div>
                    </a>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Importance Of Check Ups</h3>
                    <a href="#"><img src="images/2.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Learn More</h3>
                    <a href="#"><img src="images/3.jpg" width="100%"/></a>

                </div>
                <!-- /.col-lg-12 -->
            </div>
<br/><br/><br/>
            <!-- /.row -->             
        </div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>
