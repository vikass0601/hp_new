<h1>Profile</h1>
<form method='post' id="form_upd_user" action="userController.php">
  <input type='hidden' name='token' id='token' class='form-control' value="<?php echo Token::generate();?>" >
    
    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12" id="idErrName">
      <label class=" control-label" for="fname">First Name *</label>
      <input type='text' name='fname' id='fname' class='form-control classProfileCustomControl' placeholder="First Name" title="Please enter your First name !" data="">
    </div>

    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12" id="">
      <label class=" control-label" for="lname">Last Name *</label>
      <input type='text' name='lname' id='lname' class='form-control classProfileCustomControl' placeholder="Last Name" title="Please enter your Last name !" data=""></td>
      <input type='hidden' name='mname' id='mname'  placeholder="Middle Name"class='form-control classProfileCustomControl'>              
    </div>

    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12" id="">
      <label class=" control-label" for="selectbasic">Email *</label>
      <input type='hidden' name='id' id="id" class='form-control classProfileCustomControl' value="">
      <div class="input-group"> <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div><input type='email' name='username' id='username' placeholder="E-mail" class='form-control classProfileCustomControl' title="Please enter your email address!" data=""></div>
    </div>
    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <label class=" control-label" for="selectbasic">Mobile no. *</label>
      <div  class="input-group"> <div class="input-group-addon">+91</div><input type='text' name='contact' id='contact' class='form-control classProfileCustomControl' placeholder="Mobile Number" maxlength="10" size="10" title="Please provide Contact no !" data=""></div>
    </div>    
    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <label class="control-label" for="add_street">Address *</label>
      <div class="controls">                     
          <input type="text" id="add_street" name="add_street" class="input-medium form-control classProfileCustomControl" placeholder="Address" title="Please provide Street name !" name="add_street" data=""="" />
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <label class="control-label" for="add_landmark">Landmark</label>
      <div class="controls">
        <input id="add_landmark" name="add_landmark" placeholder="Landmark" class="input-medium form-control classProfileCustomControl" type="text">

      </div>
    </div>

    <!-- Text input-->
    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <label class="control-label" for="add_area">Area *</label>
      <div class="controls">
        <input id="add_area" name="add_area" placeholder="Area" class="input-medium form-control classProfileCustomControl" data=""="" title="Please provide area !" type="text">

      </div>
    </div>

    <!-- Text input-->
    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <label class="control-label" for="area_city">City *</label>
      <div class="controls">
          <input id="add_city" name="add_city" placeholder="City" class="input-medium form-control classProfileCustomControl" data=""=""  value="Pune" type="text" readonly="">

      </div>
    </div>

    <!-- Text input-->
    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
      <label class="control-label" for="area_zipcode">Pincode *</label>
      <div class="controls">
          <input id="add_zipcode" name="add_zipcode" placeholder="Pincode" class="input-medium form-control classProfileCustomControl" minlength="6" maxlength="6"  type="text" title="Please provide Pincode !" data="">

      </div>
    </div>

    <div class="form-group col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12">
       <label class="control-label" for="gender">Gender *</label>
       <div class="radio">
           <label><input type="radio" name="gender" value="M" checked="checked">Male</label>
           <label> <input type="radio" name="gender" value="F">Female</label>
       </div>
     </div>

     <div class="form-group col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label class="control-label" for="area_zipcode">Date Of Birth *</label>
        <div class='date controls' id='datetimepicker1'>
          <div class="input-group "> <div class="input-group-addon "><i class="fa fa-calendar"></i></div><input type='text' name="dob" id="dob" placeholder="Date Of Birth" class="form-control classProfileCustomControl classdob" /></div>
        </div>
     </div>
     
      <div class="form-group col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label class="control-label" for="age">Age</label>
        <input type='text' name='age' id='age' class='form-control classProfileCustomControl' placeholder="Age" value="" data="" readonly>
      </div>
<!--      <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label class=" control-label" for="selectbasic">Password *</label>
        <input type='password' name='password' id='idPassword' class='form-control classProfileCustomControl' placeholder="Password" title="Please enter your password !" data="">
      </div>
      <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label class=" control-label" for="selectbasic">Confirm Password *</label>
        <input type='password' name='password_again' id='password_again' class='form-control classProfileCustomControl' placeholder="Confirm Password" title="Please enter password again field !" data="">
      </div>-->
      <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <strong>*</strong> Fields are mandatory.
      </div>

      <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="edituser" class="pull-right">
          <button type="button" class="btn btn-primary" name="btn-upd" id="btn-upd">
          <span class="glyphicon glyphicon-edit"></span> Update
          </button> 
          <a href="#"  data-dismiss="modal" class="btn btn-large btn-danger"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
        </div> 
      </div>
</form>