<?php
error_reporting(E_ALL);

include_once 'core/init.php';
// include "functions/function.php";
if( !empty( Input::get('action') )){

    switch ( Input::get('action') ) {
        case 'login':
            if ( Input::exists() ) {
                  //  if( Token::check( Input::get( 'token' ) ) ) {
                 
                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                        'username' => array( 'require' =>true ),
                                        'password'=> array( 'require' => true )));
                                    
                                    if( $validation->passed() ) {
                                        $usr = sanitize( Input::get( 'username' ) );
                                       
                                        $user = new User();
                                        $login = $user->login( $usr,  sanitize( Input::get( 'password' ) ) );
                                        
                                        if( $login ) {
                                                //log user in
                                           if ( false == user_active( $usr )){
                                                echo 'You haven\'t activated your account yet.';
                                                exit;
                                            }
                                            $data = $user->_db->get( 'users1',array( 'id','=',getUserId( $usr ) ) );
                                            $user = $data->results();

                                            if( 1 == $user[0]->password_recover ){
                                               $_SESSION['RECOVER_PWD_EXECUTED'] = 1;
                                               echo 0;
                                               exit();
                                            } else {
                                                echo 1;
                                            }
                                        } else { echo '<strong>Please try again!</strong> Invalid Credentials provided.'; }
                                    } else {
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                    }
                   // }
            
            }                                
            break;
            
//        case 'logout' :
//            if ( Input::exists() ) {
//
//                                       $user = new User();
//                                       if( $user->logout()){
//                                           echo 1;
//                                          
//                                       } else { echo 0;}
//                                      
//                                }
//            break;
        case 'register'  :
                        if ( Input::exists() ) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    'username' => array(
                                                        'require' => true,
                                                        'unique' => 'users1',
                                                        'no_space' => true,
                                                        'valid_email' => true
                                                        ),
                                                    'password' => array(
                                                        'require' => true,
                                                        'matches' =>'password_again',
                                                        'min' => 6,
                                                        'max' => 12
                                                     ), 
                                                    'password_again' => array(
                                                        'require' => true
                                                    ),
                                                    'fname' => array(
                                                       'require' => true 
                                                     ),
                                                    'lname' => array(
                                                       'require' => true 
                                                     ),
                                                    'contact' => array(
                                                        'require' =>true  
                                                    ),
//                                                    'address' => array(
//                                                         'require' =>true  
//                                                    ),
                                                    'add_street' => array(
                                                       'require' => true 
                                                     ),
                                                     'add_area' => array(
                                                       'require' => true 
                                                     ),
                                                     'add_landmark' => array(
                                                       'require' => true 
                                                     ),
//                                                     'add_city' => array(
//                                                       'require' => true 
//                                                     ),
                                                    'add_zipcode' => array(
                                                       'require' => true 
                                                     ),
                                                    'gender' => array(
                                                         'require' =>true
                                                    ),
                                                    'dob'=> array(
                                                         'require' =>true
                                                  )
                                                    
                                    )); 

                                   if( true == $validation->passed()){
                                       
                                       $user = new User();
                                       $salt = Hash::salt(32);
                                       $fullName = Input::get('fname'). " ".Input::get('mname')." ".Input::get('lname');
                                       try {
                                       $register_data = array( 
                                                   'username' => Input::get('username'),
                                                   'password' => Hash::make(Input::get('password'), $salt),
                                                   'name' => $fullName,
                                                   'joined' => date( 'Y-m-d H:i:s' ),
                                                   'group' => 1,
                                                   'verification_code' => Hash::make( Input::get('username'), $salt)    
                                                   );
                                       /*
                                        * array_walk function is goes through each element 
                                        * array_sanitize location sanitize.php get element and sanitize it
                                        * and agian assign it back to array
                                        */            
                                       array_walk( $register_data, 'array_sanitize' );
                                       
                                       /*
                                        * As array salt contain lots of special character so adding it 
                                        * after sanitize user data 
                                        */
                                       $register_data['salt'] = $salt;
                                       
                                       $user_status = $user->createUser( $register_data );
                                        $emailStatus = emailForVerification(Input::get('username'),Hash::make( Input::get('username'), $salt), Input::get('fname'));
                                       $lastInserId = json_decode( $user->_db->getLastInsertId(), true );
                                       
                                       if( $user_status ){
                                                try{
                                                 $registeration_info =  $user->registerUser( array( 
                                                    'login_id' => $lastInserId['id'],
                                                    'fname' => Input::get('fname'),
                                                    'mname' => Input::get('mname'),
                                                    'lname' => Input::get('lname'),
                                                    'contact' => Input::get('contact'),
                                                    'add_street' => Input::get( 'add_street' ),
                                                    'add_area' => Input::get( 'add_area' ),
                                                    'add_landmark' => Input::get( 'add_landmark' ),
                                                    'add_city' => Input::get( 'add_city' ),
                                                    'add_zipcode' => Input::get( 'add_zipcode' ),
                                                    //'address' => Input::get('address'),
                                                    'gender' => Input::get( 'gender' ),
                                                    'dob' => Input::get( 'dob' ),
                                                    'age' => Input::get('age'),
						    'created_by' => 0,
                                                    'created_date' => date("Y-m-d h:i:s"),
                                                    'updated_by' => 0,//Session::get( Config::get( 'session/session_name' ) ),
                                                    'updated_date' => date("Y-m-d h:i:s")
                                                ));
                                              
                                                if( $user_status ){
                                                    echo 1;
                                                } 
                                                 
                                                } catch ( Exception $e ){
                                                    echo $e
;                                                }
                                          
                                        }
                                   } catch ( Exception $e ){
                                        echo $e;
                                    
                                    } 
                                   } else {
                                     
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                   
                }//input exists ends here  
            break;
            
        case 'upd': $user = new User();
                    if (Input::exists()) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    'username' => array(
                                                        'require' => true
                                                       
                                                     ),
                                                    'password' => array(
                                                        'require' => true
                                                        

                                                    ),
                                                    'password_again' => array(
                                                        'require' => true
                                                    ),
                                                    'name' => array(
                                                       'require' => true 
                                                        
                                                    )
                                    )); 

                                   if( $validation->passed() ){
                                       $user = new User();
                                       try{
                                          $user_status = $user->updateUser( Input::get('id'), array( 
                                                   'username' => Input::get('username'),
                                                   'password' => Input::get('password'),
                                                   'name' => Input::get('name')
                                                   ));

                                            if( $user_status ){
                                               echo 1;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
                    
            
        break;
        
        case 'del': $user =new User();
                  if (Input::exists()) {
                                            $user = new User();
                                                try{
                                                   $user_status = $user->deleteUser( Input::get('id'));

                                                     if( $user_status ){
                                                        echo 1;
                                                     }

                                                } catch ( Exception $e ) {
                                                    echo $e;
                                                }
                    }
            break;
            
        case 'getuser': $user =new User();
                            if (Input::exists()) {
                                                      $user->_db->get('users1',array( 'id', '=', Input::get('id') ));
                                                      $data = ( array )$user->_db->results();// print_r($data);
                                                      echo json_encode($data[0]); 
                              }
        break;
           
        case 'deleteuser': $user =new User();
                           $html ='';
                  if (Input::exists()) {
                              $html =  "<div class='container'><table class='table table-bordered'>
                                        <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Password</th>
                                        <th>Name</th>
                                        </tr>";
                              $user->_db->fetchData( "SELECT * FROM users1 WHERE id=".Input::get('delete_id') );
                              $row = $user->_db->results();
                                for($i=0; $i< $user->_db->count();$i++ ) {

                                      $html.='<tr><td>'.$row[$i]['id'].'</td>
                                       <td>'.$row[$i]['username'].'</td>
                                       <td>'.$row[$i]['password'].'</td>
                                       <td>'.$row[$i]['name'].'</td>
                                       </table></div><div class="container"><p>';
                                      
                                }
               
        if( Input::get( 'delete_id' ) ) {                       
            $html .='<form method="post" id="delUser" action="userController.php">
                    <input type="hidden" name="id" value="'.Input::get('delete_id').'" />
                    <button class="btn btn-large btn-primary" type="button" name="btn-del" id="btn-del">
                    <i class="glyphicon glyphicon-trash"></i> &nbsp; YES</button>
                    <a href="user.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>
                    </form></p></div> ';
         } else {
             $html.='<a href="user.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp;</p></div> Back to index</a>';
         }
                                
          
         $html.='<script>
            $("#btn-del").click(function(){
                    var form=$("#delUser");
                    $.ajax({
                            type:"POST",
                            url:form.attr("action"),
                            data:form.serialize()+"&action=del",//only input
                            success: function(response){
                                if( response == 1 ){
                                    $(\'#msg\').html(\' <div class="alert alert-success"><strong>Success!</strong> record was deleted... </div>\');
                                    window.location = "user.php?page_no='. Input::get('page_no') .'";
                                   // $(\'#msg\').show();
                                } else {
                                    $(\'#msg\').html(\'<div class="alert alert-danger"><strong>Problem!</strong>\'+ response +\'... </div>\');
                                    //$(\'#msg\').show();

                                }
                            }
                            });
            });
            </script>';
         }
         echo $html;
                    
           break;   
           
        case 'pg':   $user = new User();
            $page_no = (!empty( Input::get('page_no') ))? Input::get('page_no'): 1;
            echo '<table class="table table-bordered table-responsive">
                    <tr>
                    <tr>
                    <th>#</th>
                    <th>User Name</th>
                    <th>Password</th>
                    <th>Name</th>
                    <th colspan="2" align="center">Actions</th>
                    </tr>';
                     $user->viewUsers($page_no);       
    
             break; 
         
        case 'viewprofile':
                             $user =new User();
                            if (Input::exists()) {
                                                      //$user->_db->get('users1',array( 'id', '=', Input::get('id') ));
                                                      $data = $user->_db->runQuery("
                                                              SELECT
                                                                   us.`fname`,
                                                                   us.mname,
                                                                   us.lname,
                                                                   us.`contact`,
                                                                   us.add_street,
                                                                   us.`add_area`,
                                                                   us.`add_landmark`,
                                                                   us.`add_city`,
                                                                   us.`add_zipcode`,
                                                                   us.`gender`,
                                                                   us.`dob`,
                                                                   us.`age`,
                                                                   us1.username
                                                             FROM
                                                                `users` us
                                                             LEFT JOIN
                                                                users1 us1 ON us1.id = us.login_id 
                                                             WHERE 
                                                                us.login_id =".Input::get('id')); 
                                                      //echo ($data);die;
                                                      echo json_encode($data[0]); 
                              }
        break;
        
         case 'updprofile':
                             $user =new User();
                            if (Input::exists()) {
                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
//                                                    'username' => array(
//                                                    'require' => true,
//                                                    'unique' => 'users1'
//                                                     ),
//                                                    'password' => array(
//                                                        'require' => true,
//                                                        'matches' =>'password_again'
//                                                     ), 
//                                                    'password_again' => array(
//                                                        'require' => true
//                                                    ),
                                                    'fname' => array(
                                                       'require' => true 
                                                     ),
                                                    'lname' => array(
                                                       'require' => true 
                                                     ),
                                                    'contact' => array(
                                                        'require' =>true  
                                                    ),
//                                                    'address' => array(
//                                                         'require' =>true  
//                                                    ),
                                                    'add_street' => array(
                                                       'require' => true 
                                                     ),
                                                     'add_area' => array(
                                                       'require' => true 
                                                     ),
                                                     'add_landmark' => array(
                                                       'require' => true 
                                                     ),
//                                                     'add_city' => array(
//                                                       'require' => true 
//                                                     ),
                                                    'add_zipcode' => array(
                                                       'require' => true 
                                                     ),
                                                    'gender' => array(
                                                         'require' =>true
                                                    ),
                                                    'dob'=> array(
                                                         'require' =>true
                                                  )
                                                    
                                    ));  
                              }
                              
                               if( $validation->passed() ){
                                       $user = new User();
                                       
                                           try{ 
                                                $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                                    
                                        $update_user_data = array( 
                                            'fname' => Input::get('fname'),
                                            'mname' => Input::get('mname'),
                                            'lname' => Input::get('lname'),
                                            'contact' => Input::get('contact'),
                                            'add_street' => Input::get( 'add_street' ),
                                            'add_area' => Input::get( 'add_area' ),
                                            'add_landmark' => Input::get( 'add_landmark' ),
                                            'add_city' => Input::get( 'add_city' ),
                                            'add_zipcode' => Input::get( 'add_zipcode' ),
                                            //'address' => Input::get('address'),
                                            'gender' => Input::get( 'gender' ),
                                            'dob' => Input::get( 'dob' ),
                                            'age' => Input::get('age'),
                                            'updated_by' => Session::get( $user->_sessionName ),
                                            'updated_date' => date("Y-m-d h:i:s") ); 
                                                
                                       /*
                                        * array_walk function is goes through each element 
                                        * array_sanitize location sanitize.php get element and sanitize it
                                        * and agian assign it back to array
                                        */            
                                        array_walk( $update_user_data, 'array_sanitize' ); 
                                                
                                                
                                        $update =  $user->updateUser( $data[0]['id'], $update_user_data );
                                        
                                        if( $update ){
                                            echo  1;
                                        } 

                                        } catch ( Exception $e ){
                                            echo $e
;                                       }
                                          
                                            }else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
        break;
        
        case 'addapp' : $app = new Appointment();

                    if (Input::exists()) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    //'test' => array(
                                                    //    'require' => true
                                                    // ),
                                                    'location' => array(
                                                        'require' => true
                                                    ),
                                                    'appointmentDate' => array(
                                                        'require' => true
                                                    ),
                                                    'appointmentTime' => array(
                                                       'require' => true 
                                                    )
                                    )); 

                                   if( $validation->passed() ){
                                    
                                       $user = new User();
                                       try{
                                          $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                         
                                $test_status = $app->createAppointment(array( 
                                                   'user_id' => $data[0]['id'],
                                                   'loc_id' => Input::get('location'),
                                                   'lab_id' => "1",
                                                   'date' => Input::get( 'appointmentDate' ),
                                                   'slot_id' => "1",
                                                   'doctor_name' => Input::get( 'docName' ),
                                                   'created_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'created_date' => date("Y-m-d h:i:s"),
                                                   'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'updated_date' => date("Y-m-d h:i:s")
                                                   ));
                                             if( $test_status ){
                                               $lastInserId = json_decode($app->_db->getLastInsertId(), true);
                                                
                                               $test_status = $app->insertMultipleApppoinments( $lastInserId['id'], Input::get('test'));
                                               $insert_appoin_address = $app->insertAppointmentAddress(array( 
                                                         'appointment_id' => $lastInserId['id'],
                                                         'add_street' => Input::get('add_street'),
                                                         'add_area' => Input::get('add_area'),
                                                         'add_landmark' => Input::get('add_landmark'),
                                                         'add_city' => Input::get('add_city'),
                                                         'add_zipcode' => Input::get('add_zipcode'),
                                                         'created_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                         'created_date' => date("Y-m-d h:i:s"),                                              
                                                         'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                         'updated_date' => date("Y-m-d h:i:s")
                                                ));
                                               
                                                if( $insert_appoin_address ){
                                                    echo $lastInserId['id'];
                                                 }
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
        break;                        
        
        case 'appointHistory' :
                    if (Input::exists()) {
                        
                         $user = new User();
                          try{
                                   $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                          

                                $report_status = $user->loadAppointmentHistory( $data[0]['id'] );
                                $getAppointIds = $user->_db->runQuery( '
                                                            SELECT
                                                                id
                                                             FROM
                                                                `appointments` 
                                                            WHERE 
                                                                user_id ='. $data[0]['id']);
   

                                /**
                                * Generation of history
                                */     
                                
                                 $report_html = '<table class="table table-bordered table-responsive">
                                                                <tr>
                                                                    <th>Appointment ID</th>
                                                                    <th>Name</th>
                                                                    <th>Lab Name</th>
                                                                    <th>Test Name || Test Charge </th>
                                                                 <!--   <th>Test Charge</th> -->
                                                                    <th>Date</th>
                                                                    <th>Status</th>
                                                                     <th colspan="2" align="center">Actions</th>
                                                               </tr>';
               
                                    if( true == !empty( $report_status )  ){
                                         $report_html_start = $report_html_test = $testIds = $report_html_end = '';
                                         foreach( $getAppointIds as $apkey => $value ) {  
                                               $flag = 1;
                                               foreach( $report_status as $key => $value ) {
                                                   $count = 1;
                                                   
                                                    if( $getAppointIds[$apkey]['id'] == $report_status[$key]['id'] ){
                                                        
                                                      if( 1 == $flag ) {
                                                         $report_html_start .= "<tr>";
                                                         $report_html_start .= '<td>'. $report_status[$key]['id'] .'</td>';
                                                         $report_html_start .= '<td>'. $report_status[$key]['name'] .'</td>';
                                                         $report_html_start .= '<td>'. $report_status[$key]['lab_name'] .'</td>';  
                                                          
                                                         $report_html_end .= '<td>'. substr($report_status[$key]['date'], 0, -8) .'</td>';
                                                         $report_html_end .= '<td>'. $report_status[$key]['status'] .'</td>';
                                                         if( $report_status[$key]['status']=='Completed'){
                                                            $report_html_end .= '<td align="center">-</td>';
                                                         }else{                                                         
                                                            $report_html_end .= '<td align="center">'
                                                                 . '<a href="javascript:void(0);" class="js-editappointment" data-value="action=loadEditPage&id='.$report_status[$key]['id'] . '" id="' .$report_status[$key]['id'] . '">edit</a>
                                                                    <a href="javascript:void(0);" class="js-cancelappointment" data-value="action=cancelapp&id='.$report_status[$key]['id'].'&test_id=' . $report_status[$key]['test_id'] . '">cancel</a>'
                                                                 . '</td>';
                                                          }
                                                         $report_html_end .= "</tr>";
                                                         
                                                         $report_html_test .= $count. ') ' .$report_status[$key]['test_name'] .' || '. $report_status[$key]['test_charge'].'<br/>';
                                                         $testIds .= $report_status[$key]['test_id'] .',';
                                                         $flag = 0;
                                                       } else {
                                                          $report_html_test .= ($count + 1). ') ' .$report_status[$key]['test_name'] .' || '. $report_status[$key]['test_charge'].'<br/>';
                                                          $testIds .= $report_status[$key]['test_id'].',';
                                                      }
                                                      
                                                    };
                                                      
                                            }
                                            
                                            $dataToCarry = '<input type="hidden" data-tests="' . $testIds .'" data-loc="' . $report_status[$key]['loc_id'] .'" data-main="'. $getAppointIds[$apkey]['id']. '">' ;
                                            $report_html .= $report_html_start . '<td>' . $report_html_test . '</td>' .  $report_html_end .$dataToCarry;
                                            
                                            $report_html_start = $report_html_test = $report_html_end = $dataToCarry = $testIds = '';
                                        }    
                                            

                                               echo $report_html;
                                            } else { 

                                               $report_html.= '<tr>
                                                                <td colspan="7" class="danger"> No Record Found .....     </td>
                                                              </tr>';
                                                $report_html.= '</table>';
                                            
                                              echo $report_html;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }
                                            
                        
                    }
             break;
        case 'addappother' : $app = new Appointment();
            
                     if (Input::exists()) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
//                                                    'r_test' => array(
//                                                        'require' => true
//                                                     ),
                                                    'r_location' => array(
                                                        'require' => true
                                                    ),
                                                    'r_appointmentDate' => array(
                                                        'require' => true
                                                    ),
                                                    'pAppTime' => array(
                                                       'require' => true 
                                                    )
                                    )); 

                                   if( $validation->passed() ){
                                    
                                       $user = new User();
                                       try{
                                          $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                          
                                        $test_status = $app->createAppointment( array( 
                                                   'user_id' => $data[0]['id'],
                                                   'loc_id' => Input::get('r_location'),
                                                   'lab_id' => "1",
                                                   'date' => Input::get( 'appointmentDate' ),
                                                   'slot_id' => "1",
                                                   'created_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'created_date' => date("Y-m-d h:i:s"),
                                                   'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'updated_date' => date("Y-m-d h:i:s")
//                                                   'is_deleted' => NULL
                                        ));
                                            if( $test_status ){
                                                
                                                $lastInserId = json_decode($app->_db->getLastInsertId(), true);
                                               
                                                $test_status = $app->insertMultipleApppoinments( $lastInserId['id'], Input::get('r_test'));
                                                /*
                                                 * To insert address in different table
                                                 */
                                                $insert_appoin_address = $app->insertAppointmentAddress(array( 
                                                         'appointment_id' => $lastInserId['id'],
                                                         'add_street' => Input::get('add_street'),
                                                         'add_area' => Input::get('add_area'),
                                                         'add_landmark' => Input::get('add_landmark'),
                                                         'add_city' => Input::get('add_city'),
                                                         'add_zipcode' => Input::get('add_zipcode'),
                                                         'created_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                         'created_date' => date("Y-m-d h:i:s"),                                              
                                                         'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                         'updated_date' => date("Y-m-d h:i:s")
                                                ));
                                               
                                                if( $insert_appoin_address ){
                                                         $r_reltion = Input::get('pRelation');
                                                         $r_name = Input::get('pName');
                                                         $r_email = Input::get('pEmail');
                                                         $r_phone = Input::get('pContact');
                                                         $r_gender = Input::get('pGender');
                                                         $r_doctor = Input::get('pDocName');
//                                                         $r_address = Input::get('pAddress');
                                                         $add_street = Input::get('add_street');
                                                         $add_area = Input::get('add_area');
                                                         $add_landmark = Input::get('add_landmark');
                                                         $add_city = Input::get('add_city');
                                                         $add_zipcode = Input::get('add_zipcode');
                                                         $r_address = $add_street.' '.$add_area.' '.$add_landmark.' '.$add_city.' '.$add_zipcode;
                                                         $r_dob = Input::get('pDob');
                                                         $r_age = Input::get('pAge');                                               
                                                         $created_by = Session::get( Config::get( 'session/session_name' ) );
                                                         $created_date = date("Y-m-d h:i:s");
                                                         $updated_by = Session::get( Config::get( 'session/session_name' ) );
                                                         $updated_date = date("Y-m-d h:i:s");
                                                         $is_deleted = 0;

                                                  $sql = "INSERT INTO 
                                                                    `relations`(`id`, `appointment_id`, `relation`, `r_name`, `r_email`,
                                                                                `r_phone`, `r_gender`, `r_doctor`, `r_address`, `add_street`,
                                                                                `add_area`, `add_landmark`, `add_city`, `add_zipcode`, `r_dob`,
                                                                                `r_age`, `created_by`, `created_date`, `updted_by`,
                                                                                `updated_date`, `is_valid`) 
                                                          VALUES (NULL,'{$lastInserId['id']}','{$r_reltion}','{$r_name}','{$r_email}','{$r_phone}',
                                                                  '{$r_gender}','{$r_doctor}','{$r_address}','{$add_street}','{$add_area}',
                                                                  '{$add_landmark}','{$add_city}','{$add_zipcode}','{$r_dob}','{$r_age}','{$created_by}',
                                                                  '{$created_date}','{$updated_by}','{$updated_date}','{$is_deleted}')";
                                                
                                                 $result = $app->_db->runQuery( $sql );
//                                                  print_r($result);die;
                                                    echo $lastInserId['id'];
                                               }
                                             
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
                    
        break; 
        
        case 'getRecentCheckup' :
                    if (Input::exists()) {
                        
                         $user = new User();
                          try{
                                   $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                          

                                $checkup_status = $user->loadRecentCheckups( $data[0]['id'] );

                                /**
                                * Generation of Recent CHeck up 
                                 * added by vikas 03 -july -2015
                                */     
                                
                                 $recent_checkup_info = '';
               
                                            if( true == !empty( $checkup_status )  ){
                              
                                              
                                               foreach( $checkup_status as $key => $value ) {
                                                   
                                                  $recent_checkup_info = '<div class="classROItem">'. $checkup_status[$key]['test_name'] .'<div class="pull-right">'. substr( $checkup_status[$key]['date'], 0, -8 ) .'</div></div>';
                                               }
                                            
                                               echo $recent_checkup_info;
                                            } else { 

                                               $recent_checkup_info.= '<div class="classROItem">No Record </div>';
                                                
                                              echo $recent_checkup_info;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }
                                            
                        
                    }
             break;
	
         case 'getPendingTestCount' :
                    if (Input::exists()) {
                        
                         $user = new User();
                          try{
                                   $data = $user->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `users` 
                                                              WHERE 
                                                                users.login_id =".Session::get( $user->_sessionName )); 
                                          

//                                $pen_test_status = $user->loadRecentCheckups( $data[0]['id'] );

                                /**
                                * Generation of Recent CHeck up for peding count 
                                 * added by vikas 03 -july -2015
                                */     
                                
                                
                                         $strSql = 'SELECT 
														Count(*) as cnt 
													 FROM 
														`appointment_tests` 
													 WHERE 
														appointment_id IN ( 
																			SELECT 
																				  id 
																			  FROM 
																			    `appointments` 
																			  WHERE 
																			     user_id = ' . $data[0]['id'] . ' and status_id = 1 )' ;
                                         $penTestCount = $user->_db->runQuery( $strSql );
                                            
                                            if( $penTestCount ){
                                               echo json_encode( $penTestCount );
                                               $user->_db->closeConnection();
                                            } else {
                                               echo 0;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }
                                            
                        
                    }
             break;     
        case 'null': 
            
            if ( Input::exists() ) {
                $validate = new Validate();
                    $validation = $validate->check( $_POST, array(
                                                    'c_pwd' => array(
                                                        'require' => true
                                                        ),
                                                    'password' => array(
                                                        'require' => true,
                                                        'matches' =>'password_again',
                                                        'min' => 6,
                                                        'max' => 12,
                                                        'name_to_display' => 'New password'
                                                     ), 
                                                    'password_again' => array(
                                                        'require' => true
                                                    )
                                                 ));   
                            if( $validation->passed() ){

                                $user = new User();
                                try{
                                   $data = $user->_db->runQuery("
                                                              SELECT
                                                                   password as pwd,
                                                                   salt
                                                              FROM
                                                                `users1` 
                                                              WHERE 
                                                                id =".Session::get( $user->_sessionName )
                                           );
                                if( Input::get( 'password' ) != Input::get( 'c_pwd' ) ) {   
                                   if( $data[0]['pwd'] == Hash::make( trim( escape( Input::get('c_pwd') ) ), $data[0]['salt']) ) {
                                       
                                       $salt = Hash::salt(32);
                                       $update_status = $user->change_pwd( Session::get( $user->_sessionName ), array( 
                                                   'salt' => $salt,
                                                   'password' => Hash::make(Input::get('password'), $salt),
                                                   'password_recover' => 0,
                                                   'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'updated_on' => date("Y-m-d h:i:s")
                                                   ));
                                       if( true == $update_status) {
                                           echo 1;
                                       }       
                                     } else {
                                        $validate->addError("Your current password is incorrect");
                                        show_errors( $validate->errors() ); 
                                   }
                                } else {
                                        $validate->addError("Current password should not be similar to new password");
                                        show_errors( $validate->errors() ); 
                                }   
                                          
                                } catch ( Exception $e ) {
                                           echo $e;
                                    }
                                       
//                                $pen_test_status = $user->loadRecentCheckups( $data[0]['id'] );
                                
                            } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                            }                        
                     
                
            }
        break;
        case 'recover':
                if ( Input::exists() ) {
                $validate = new Validate();
                    $validation = $validate->check( $_POST, array(
                                                    'email' => array(
                                                        'require' => true,
                                                        'name_to_display' => 'Email'
                                                    )));
                       if( $validation->passed() ){
                                if( true == user_exists( Input::get( 'email' )) ){
                                    $username = Input::get( 'email' );
                                    $generated_password = substr( md5( rand( 999,99999 ) ), 0, 7 );
                                    
                                    $user = new User();
                                    $salt = Hash::salt(32);
                                    $update_status = $user->change_pwd( getUserId( $username ), array( 
                                                   'salt' => $salt,
                                                   'password' => Hash::make( $generated_password, $salt),
                                                   'password_recover' => 1
                                                   ));
                                    emailForgetPassword($username, $generated_password, getFirstName( $username ));
                                    echo 1;
                                } else {
                                    echo 0;
                                    die;
                                }
                                
                            } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                            }
                }
                
            
            break;
   
}
}




