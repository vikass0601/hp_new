<?php
require_once 'core/init.php';

//var_dump( Token::check( Input::get('token') ) );

?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div id="msg" class="container"></div>


<!--Add user start from here-->
<div class="container" id="addUser" name="addUser">
    <div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
        <li><a href="#book" data-toggle="tab">Book</a></li>
        <li><a href="#history" data-toggle="tab">History</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="profile">
            <h1>Profile</h1>
            <form method='post' id="form_upd_user" action="userController.php">

                            <table class='table table-bordered'>

                                <tr>
                                    <td>Username</td>
                                    <!--<input type='hidden' name='id' id="id" class='form-control' value="">-->
                                    <td><input type='text' name='username' id='username' class='form-control' value="" disabled="disabled" ></td>
                                </tr>

                        <!--        <tr>
                                    <td>Password</td>
                                    <td><input type='password' name='password' id='password' class='form-control' value="" required></td>
                                </tr>

                                <tr>
                                    <td>Password Again</td>
                                    <td><input type='password' name='password_again' id='password_again' class='form-control' value="" required></td>
                                </tr>-->

                                <tr>
                                    <td>Your Name</td>
                                    <td><input type='text' name='name' id='name' class='form-control' value="" required></td>
                                <input type='hidden' name='token' id='token' class='form-control' value="<?php echo Token::generate();?>" >
                                </tr>

                                <tr>
                                    <td>Contact No</td>
                                    <td><input type='number' name='contact' id='contact' class='form-control' value="" required></td>
                                </tr>

                                <tr>
                                    <td>Address</td>
                                    <td> <textarea class="form-control" rows="5" id="address" name="address"></textarea></td>
                                </tr>

                                <tr>
                                    <td>Gender</td>
                                    <td>
                                     <div class="radio">
                                        <label><input type="radio" name="gender" value="1">Male</label>
                                        <label><input type="radio" name="gender" value="0">Female</label>
                                     </div>
                                </tr>

                                <tr>
                                    <td>DOB</td>
                                    <td>
                                    <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' name="dob" id="dob"  class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div>    
                                    </td>
                                </tr>

                                <tr>
                                    <td>Age</td>
                                    <td><input type='number' name='age' id='age' class='form-control' value="" required></td>
                                </tr>

                                <tr>


                                <tr>

                                <td colspan="2">
                                    <div id="submituser">
                                       <button type="button" class="btn btn-primary" name="btn-upd" id="btn-upd">
                                           <span class="glyphicon glyphicon-plus"></span> Update
                                       </button>  
                                       <a href="login.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to login</a>
                                    </div>

                                </td>



                                </tr>

                            </table>
                        </form>
        </div>
        <div class="tab-pane" id="book">
            <h1>Book</h1>
            <form method='post' id="form_add_app" action="appoinController.php">

                            <table class='table table-bordered'>

                                <?php $location = $user->_db->runQuery("SELECT 
                                                    `id`,
                                                    `location_name`
                                                 FROM 
                                                    `locations`
                                            ");
                                 ?>
                                <tr>
                                    <td>Location </td>
                                    <td> <select class='form-control' id="loc_id" name="loc_id">
                                        <?php foreach($location as  $option) { ?>
                                        <option  value="<?php echo $option['id'] ?>"> 
                                        <?php echo $option['location_name'] ?>
                                        </option>
                                        <?php }?>
                                        </select></td>
                                </tr>

                                <?php $labs = $user->_db->runQuery("SELECT 
                                                    `id`,
                                                    `lab_name`
                                                 FROM 
                                                    `labs`
                                            ");
                                 ?>
                                <tr>
                                    <td>Lab Name </td>
                                    <td> <select class='form-control' id="lab_id" name="lab_id">
                                        <?php foreach($labs as  $option) { ?>
                                        <option  value="<?php echo $option['id'] ?>"> 
                                        <?php echo $option['lab_name'] ?>
                                        </option>
                                        <?php }?>
                                        </select></td>
                                </tr>

                                 <?php $test = $user->_db->runQuery("SELECT 
                                                    `id`,
                                                    `category_name`
                                                 FROM 
                                                    `test_categories`
                                            ");
                                ?>
                                   <tr>
                                       <td>Test Category</td>
                                       <td> <select class="form-control" id="cat_id" name="cat_id">
                                           <?php foreach($test as  $option) { ?>
                                           <option value="<?php echo $option['id'] ?>"> 
                                           <?php echo $option['category_name'] ?>
                                           </option>
                                           <?php }?>
                                           </select></td>
                                   </tr>

                                <tr>
                                    <td>Tests</td>
                                    <td> <span id="loadTest"></span></td>
                                 </tr>

                                <tr>
                                    <td>Time</td>
                                    <td><span id="laodTime"></span></td>
                                     
                                </tr>

                                <tr>
                                    <td>Date</td>
                                    <td>
                                    <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' name="dob" id="dob"  class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div>    
                                    </td>
                                </tr>

                                <tr>


                                <tr>

                                <td colspan="2">
                                    <div id="submituser">
                                       <button type="button" class="btn btn-primary" name="add-app" id="add-app">
                                           <span class="glyphicon glyphicon-plus"></span> Update
                                       </button>  
                                       <a href="login.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to login</a>
                                    </div>

                                </td>



                                </tr>

                            </table>
                        </form>
        </div>
        <div class="tab-pane" id="history">
            <h1>History</h1>
            <p> You will able to see your appointment history soon........</p>
        </div>
        
    </div>
</div>

</div>
<!--Add user ends here-->
<?php include_once 'footer.php'; ?>
<script>
    $(document).ready(function(){

       $.ajax({
                type:"POST",
                url:'userController.php',
                data:{ action:'viewprofile',
                       id:<?php echo Session::get( $user->_sessionName);?>
                    
                     },//only input
                success: function(response){
                    var obj = $.parseJSON( response );
                    $("#id").val(obj.id);
                    $("#username").val(obj.username);
//                    $("#password").val(obj.password);
//                    $("#password_again").val(obj.password);
                    $("#name").val(obj.name);
                    $("#contact").val(obj.contact);
                    $("#age").val(obj.age);
                    $("#dob").val(obj.dob);
                    if(obj.gender){  $('input:radio[name=gender]')[0].checked = true; } else { $('input:radio[name=gender]')[1].checked = true;  }
                   
                }
            });
            
    /******
    * Actual updation of content starts here
     */
    $("#btn-upd").on("click", function(){
            var form=$("#form_upd_user");
            $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize()+"&action=updprofile",//only input
                    success: function(response){
                        if( response == 1 ){
                            $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was updated successfully <a href="login.php">HOME</a>!</div>');
                            $('#msg').show();
                             $('#form_upd_user').trigger("reset");
                        } else {
                            $('#msg').html("<div class='alert alert-danger'><strong>SORRY!</strong> ERROR while updating record !<br>"+ response  +"</div>");
                            $('#msg').show();

                        }
                    }
            });
        });
        
    /***************************
     * Submit new Appointment data
     ******/      
    $("#add-app").on( "click", function(){
        var form=$("#form_add_app");
        $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+"&action=addapp",//only input
                success: function(response){
                    if( response == 1 ){
                        $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                        $("#form_add_app").trigger("reset");
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });
        });
        
        
        /****
        * get test on test category
         */
          $("#cat_id").on("change", function(){
           $.ajax({
                    type:"POST",
                    url:'appoinController.php',
                    data:"action=gettest&test_cat_id="+$("#cat_id").val(),//only input
                    success: function(response){
                       // alert(response);
                       var text = '';
                       var i =0;
                        var json = $.parseJSON( response );
                        for (i = 0; i < json.length; i++) {
                            text += '<div  class="checkbox"><label><input  type="checkbox" name="test[]" value="' + json[i].id + '">'+ json[i].test_name +'</input></label><button type="button" class="btn btn-info">'+ json[i].test_charge +'</button></div>'+ "<br>";
                        }
                        $('#loadTest').html(text);

                    }
            });
          });
          
           /****
            * Load fix time from times table
            */
          $.ajax({
                    type:"POST",
                    url:'appoinController.php',
                    data:"action=gettime",//only input
                    success: function(response){
                       // alert(response);
                       var text = '';
                       var i =0;
                        var json = $.parseJSON( response );
                        for (i = 0; i < json.length; i++) {
                            text += '<div class="checkbox"><label><input type="checkbox" name="time[]" value="' + json[i].id + '">'+ json[i].time +'</input></label></div>'+ "<br>";
                        }
                        $('#laodTime').html(text);

                    }
            });
        
        
        
    });
   
</script>
    