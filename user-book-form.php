<form method='post' id="on_form_appointment" action="userController.php">
                              
                                <fieldset>

                                  <!-- Form Name -->
                                  <legend><i class="fa fa-calendar"></i> Book Your Appointment</legend>
                                  <!-- Select Basic -->
                                  <div class="row">
                                  <div class="form-group  col-md-4">
                                    <label class=" control-label" for="selectbasic">Test</label>
                                   <div class="ui-widget classBookInProfile">
                                       <select id="idTest" name="test[]" class="form-control selectpicker" multiple="">
                                          <!--<option value="select" selected="">Select Your Test</option>-->
                                                  <?php
                                                foreach ($aAllTests as $key => $value) {
                                                  echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                                }
                                              ?>
                                        </select>
                                      </div>
                                    <div class="" id="idTestErr"></div>
                                  </div>

                                  <div class="form-group  col-md-3">
                                    <label class=" control-label" for="selectbasic">Your Location</label>
                                    <div class="ui-widget classBookInProfile">
                                      <select id="idLocation" name="location" class="form-control">
                                        <option value='select'>Select Your Location</option>
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                    <div class="" id="idLocationErr"></div>
                                  </div>
                                        <div class="form-group col-md-2">
                                            <label class=" control-label" for="appointmentDate">Appointment Date</label>  
                                            <div class="">
                                              <input id="idAppointmentDate" name="appointmentDate" placeholder="dd/mm/yyyy" class="form-control input-md" type="text">
                                            </div>
                                            <div class="" id="idAppDateErr"></div>
                                        </div> 
                                      <div class="form-group col-md-3">
                                        <label class=" control-label" for="appointmentTime">Appointment Time</label>
                                        <div class="">
                                         <input id="idAppTime" name="appointmentTime" placeholder="" class="form-control input-md" type="text" value="Between 7:00 am to 12:00pm" readonly/>
                                        </div>
                                      </div>   
                                    </div>                                     
                                    <div class="row">
                                      <div class="form-group col-md-2 pull-right">
                                         <label class=" control-label" ></label>
                                        <div class="">                                         
                                            <input type="button" id="bookFromUserProfile" name="bookFromUserProfile" value="Book Appointment" class=" btn btn-success"></inputs>                                          
                                        </div>
                                      </div>
                                  </div>
                                </fieldset>
                              </form>
