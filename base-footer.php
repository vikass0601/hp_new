  
    <!-- updated by vikas 31-may-2015-->
    <script src="js/external/jquery_validation/jquery.validate.min.js"></script>
    <script src="js/internal/js-form-validation.js"></script>
    <script src="js/internal/js_combine.js"></script>
    <script src="js/internal/jquery.inputlimiter.1.3.1.js"></script>
       

<!-- Login form modal code start --> 
<div class="modal fade " id="idLoginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false" backdrop='true' data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="exampleModalLabel">Login</h4>
        <div id="" class="msg"></div>
      </div>
      <div class="modal-body">
           <div id="msg_login" class="msg"></div>
        <form method='post' id="form_login" action="userController.php">
     
        <table class='table table-bordered'>
     
            <tr>
                <td>Email</td>
                <td><input type='text' name='username' id='username' class='form-control' value=""required ></td>
            </tr>
            <tr><td colspan="2" id="idUsernameErr"></td></tr>
     
            <tr>
                <td>Password</td>
                <td><input type='password' name='password' id='password' class='form-control' value="" required></td>
            </tr>
            <tr><td colspan="2" id="idPasswordErr"></td></tr>
     
         
            <tr>
            <input type='hidden' name='token'  class='form-control' value="<?php echo Token::generate();?>" ></td>
            <td colspan="2">
                <div id="submituser">
                   <button type="submit" class="btn btn-primary" name="btn-save" id="idLoginSubmit">
                       <span class="glyphicon glyphicon-plus"></span> Login
                   </button>  
                   <a href="sign-up.php" id="" class="signupid btn btn-large btn-success">Register Me &nbsp;<i class="glyphicon glyphicon-forword"></i> </a>
                </div>
                      
            </td>
                
             
             
            </tr>
     
        </table>
    </form>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-12 control"> 
                  Forget password
                  <a class="btn btn-info btn-xs forgetPasswordid" id="" href="#idForgetPasswordModal">
                  Click Here
              </a>
             
          </div>
      </div>    
      </div>
    </div>
  </div>
</div>
<!-- Login modal form code end-->

<!-- Forget form modal code start -->
<div class="modal fade " id="idForgetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="exampleModalLabel">Forget Password</h4>
        <div id="" class="msg"></div>
      </div>
      <div class="modal-body">
        <form method='post' id="form_fgt_user_pwd" action="userController.php">
      <div id="msg_pwd"></div> 
        <table class='table table-bordered'>
     
            <tr>
                <td>Email</td>
                <td><input type='text' name='email' id='recover_email' class='form-control' value=""required ></td>
            </tr>
     
            <tr>
            <input type='hidden' name='token'  class='form-control' value="<?php echo Token::generate();?>" ></td>
              <td colspan="2">
                  <div id="submituser">
                     <button type="button" class="btn btn-primary" name="btn-save" id="btn_fgt_pwd">
                         <span class="glyphicon glyphicon-plus"></span> Send
                     </button>  
                     <a href="#" id="#id" class="btn btn-large btn-danger" data-dismiss="modal">Cancel</i> </a>
                  </div>
                        
              </td>
            </tr>
     
        </table>
    </form>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-12 control"> 
                  New User Registration 
              <a class="btn btn-info btn-xs signupid" id="" href="sign-up.php">
              <!-- <a class="btn btn-info btn-xs" id="loginid" href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"> -->
                  Click Here
              </a>
             
          </div>
      </div>    
      </div>
    </div>
  </div>
</div>
<!-- Forget modal form code end-->
<!-- Share This Button Code Start -->
<!--<script>var sharebutton_is_horizontal = true; document.write('<script src="//cdn.script.to/share.js"></scr' + 'ipt>'); document.write("<div style='display: none'>");</script><a href="http://sharebutton.org/">social sharing buttons</a><script>document.write("</div>");</script>-->
<!-- Share This Button Code End -->
</body>

</html>