<?php
include('base-header.php');
include('page-header.php');
$aAllCat=getAllCategories();
$aAllTests=getAllTests();
$aAllLoc=getAllLocations();
?>
<script type="text/javascript">
            // When the document is ready
      $(document).ready(function () {
        // var datepicker = $.fn.datepicker.noConflict();
          $('#idAppointmentDate').datepicker({
              dateFormat: 'dd-mm-yy' ,
              startDate: '+1d'
          }); 
     /**
      * Code Added by vikas to select combobox runtime
      */  
          $( "#idTest" ).select2( {
                placeholder: "Select Your Test..",
          });
          
          
          $( "#idLocation" ).select2( {
                placeholder: "Select Your Location..",
          });
          
          
          
                  
          
          
          
  //           $('#idSpanDOB').click(function(){
  //   $(this).closest('.input-group').find('.hasDatepicker').focus();
  // }).css('cursor','pointer');



          $("#dob").datepicker({
            dateFormat: "yy-mm-dd",
            defaultDate:'1989-01-01',
            maxDate: 0,
            changeMonth: true, 
            changeYear: true,
            /*yearRange: '1900:' + new Date().getFullYear()*/
            yearRange: "c-100:c+25"
          });

            $('#idSpanDob').click(function(){
                $(this).closest('.input-group').find('.hasDatepicker').focus();
            }).css('cursor','pointer');

                function calculateAge(birthday) {
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    return Math.abs(ageDate.getUTCFullYear() - 1970);
                }


               $('#dob').change(function(){
                var dob = $(this).val();
                var aDOB = dob.split('-');
                var year = parseInt(aDOB[0]);
                var month = parseInt(aDOB[1]) - 1;
                var day = parseInt(aDOB[2]);
                var bDay = new Date(year, month, day, 0, 0, 0, 0);
                console.log(bDay);
                var age = calculateAge(bDay);

                $('#age').val(age);

              });   
      });
  </script>
   <script type="text/javascript">
$(document).ready(function(){
  $('.loginid').click(function(){   
        $("#idSignUpModal").modal('hide');
        $("#idLoginModal").modal('show');
         $("#idForgetPasswordModal").modal('hide');

  });
  $('.signupid').click(function(){   
        $("#idSignUpModal").modal('show');
        $("#idLoginModal").modal('hide');
         $("#idForgetPasswordModal").modal('hide');
         $('.modal').css("overflow","scroll");
  });
   $('.forgetPasswordid').click(function(){   
        $("#idForgetPasswordModal").modal('show');
        $("#idSignUpModal").modal('hide');
        $("#idLoginModal").modal('hide');
  });

});
</script>        
<script>
    $(document).ready(function(){


    //Validation for book Apponntment
    
    // $("#idBookAppointment").click(function(){
    //   var test_name = $("#idTest").val();
    //   if(test_name == "select"){
    //     $("#idTestErr").html("<label style='color:red;'>Select Test</label>");
    //     exit;
    //   }
    // });  
      // $("#idLoginSubmit").click()


      // $("#idBookAppointment").click(function(){
      //   $("#idBookAppFlag").val("1");
      // });
    $("#idBookAppointmentOnLogin").on( "click", function(){

          var tempDate = $("#idAppointmentDate").val().split( '-' );
          var appointmentDate = tempDate[2] + '-' + tempDate[1] + '-' + tempDate[0];
          var appointmentForm=$("#on_form_appointment");
          
            $.ajax({
                  type:"POST",
                  url:appointmentForm.attr("action"),
                  data:appointmentForm.serialize()+"&action=addapp&appointmentDate="+appointmentDate,//only input
                  success: function(response){
                      if( response == 1 ){
                          //$("#form_add_app").trigger("reset");
                          window.location = 'thanks-appointment-done.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                      } else {
                          alert('try again!');
                      }
                  }
              });  
          });



            /* On key press remove error alert */
            $("#username").keypress(function(){
              $("#idUsernameErr").html("");
            });
            $("#password").keypress(function(){
              $("#idPasswordErr").html("");
    });

    /***************************
     * Submit new user data
     */      
     $("#idLoginSubmit").on( "click", function(){

        var bookAppFlag = $("#idBookAppFlag").val();
        var  username = $("#username").val();
        var  password = $("#password").val();
        var form=$("#form_login");
        var appointmentForm=$("#on_form_appointment");
       
        $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+'&action=login',//only input
                success: function(response){
                    if( response == 1 ){
                       if(bookAppFlag == "1"){
                            $.ajax({
                                type:"POST",
                                url:appointmentForm.attr("action"),
                                data:appointmentForm.serialize()+"&action=addapp",//only input
                                success: function(response){
                                    if( response == 1 ){
                                        //$("#form_add_app").trigger("reset");
                                        window.location = 'thanks-appointment-done.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                                    } else {
                                        alert('try again!');
                                    }
                                }
                            }); 
                      }     
                      window.location = 'index.php';                       
                    } else {
                        alert('try again! Invalid Credential...');
                    }
                }
            });
        });
    
    
 });
</script>
<script>
    $(document).ready(function(){

         $('#name').keydown(function (e) {
          if (e.shiftKey || e.ctrlKey || e.altKey) {
          e.preventDefault();
          } else {
          var key = e.keyCode;
          if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
          }
          }
      });
        
      
      /* Remove error alert*/

    $("#name").keypress(function(){
      $("#idErrName").html("");
    });

     $("#idUsername").keypress(function(){
      $("#idErrEmail").html("");
      $("#idErrEmailAddress").html("");
    });

      $("#idPassword").keypress(function(){
      $("#idErrPassword").html("");
    });

       $("#password_again").keypress(function(){
      $("#idErrPasswordAgain").html("");
    });

        $("#contact").keypress(function(){
      $("#idErrContact").html("");
    });

         $("#address").keypress(function(){
      $("#idErrAddress").html("");
    });

          function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
      };



      /***************************
     * Submit new user data
     */      
    $("#idRegisterSubmit").on( "click", function(){

      var bookAppFlag = $("#idBookAppFlag").val();
        if(bookAppFlag == "1"){
         var sTestName = $("#idTest").val();
          var sLocationName = $("#idLocation").val();
          if(sTestName == "select"){
              $("#idTestErr").fadeIn(1500);
              $("#idTestErr").html("<label style='color:red;'>Select Your Test</label>");
              $("#idTestErr").fadeOut(1500);
              // exit;
              // $('#idLoginModal').modal('lock');
              // return e.preventDefault();
              return false;
          }
           if(sLocationName == "select"){
              $("#idLocationErr").fadeIn(1500);
              $("#idLocationErr").html("<label style='color:red;'>Select Location</label>");
              $("#idLocationErr").fadeOut(1500);
              // exit;
              // return e.preventDefault();
              return false;
          }
         } 
         
      var name = $("#name").val();
      if($.trim(name) == ""){
          $("#idErrName").fadeIn(1500);
          $("#idErrName").html("<td colspan='2'><label style='color:red;'>Enter Your Name</label></td>");
          $("#idErrName").fadeOut(1500);
          return false;
      }
      
      var id = $("#idUsername").val();
      if($.trim(id) == ""){
           $("#idErrEmail").fadeIn(1500);
          $("#idErrEmail").html("<td colspan='2'><label style='color:red;'>Enter Email Id</label></td>");
          $("#idErrEmail").fadeOut(1500);
          return false;
      }

       if (!ValidateEmail($("#idUsername").val())) {
          $("#idErrEmailAddress").fadeIn(1500);
          $("#idErrEmailAddress").html("<td colspan='2'><label style='color:red;'>Enter Valid Email Id</label></td>");
          $("#idErrEmailAddress").fadeOut(1500);
          return false;
      }

      var password = $("#idPassword").val();
      if($.trim(password) == ""){
          $("#idErrPassword").fadeIn(1500);
          $("#idErrPassword").html("<td colspan='2'><label style='color:red;'>Enter Password</label></td>");
          $("#idErrPassword").fadeOut(1500);
          return false;
      }

      var password_again = $("#password_again").val();
      if($.trim(password_again) == ""){
          $("#idErrPasswordAgain").fadeIn(1500);
          $("#idErrPasswordAgain").html("<td colspan='2'><label style='color:red;'>Enter Password Again</label></td>");
          $("#idErrPasswordAgain").fadeOut(1500);
          return false;    
      }

       // var Check = password.localeCompare(password_again);
       // alert(Check);
      if(password != password_again){
          $("#idErrPasswordCheck").fadeIn(1500);
          $("#idErrPasswordCheck").html("<td colspan='2'><label style='color:red;'>Password Not Match</label></td>");
          $("#idErrPasswordCheck").fadeOut(1500);
          return false;
      }

      var contact = $("#contact").val();
      if($.trim(contact) == ""){
          $("#idErrContact").fadeIn(1500);
          $("#idErrContact").html("<td colspan='2'><label style='color:red;'>Enter Conatct Number</label></td>");
          $("#idErrContact").fadeOut(1500);
          return false;
      }

      var mob = /^[1-9]{1}[0-9]{9}$/;
      if (mob.test($.trim($("#contact").val())) == false) {
          // alert("Please enter valid mobile number.");
          $("#idErrContact").fadeIn(1500);
          $("#idErrContact").html("<td colspan='2'><label style='color:red;'>Please enter valid mobile number.</label></td>");
          $("#idErrContact").fadeOut(1500);
          $("#contact").focus();
          return false;
      }
      
      var address = $("#address").val();
      if($.trim(address) == ""){
          $("#idErrAddress").fadeIn(1500);
          $("#idErrAddress").html("<td colspan='2'><label style='color:red;'>Enter Address</label></td>");
          $("#idErrAddress").fadeOut(1500);
          return false;
      }

      var dob = $("#dob").val();
       if($.trim(dob) == ""){
          $("#idErrDob").fadeIn(1500);
          $("#idErrDob").html("<td colspan='2'><label style='color:red;'>Enter Date of Birth</label></td>");
          $("#idErrDob").fadeOut(1500);
          return false;
      }


       var form=$("#form_register");
       var appointmentForm=$("#on_form_appointment");
         $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+'&action=register',//only input
                success: function(response){
                    if( response == 1 ){
                        $.ajax({
                                type:"POST",
                                url:"mail.php?name="+$("#name").val()+"&mailid="+$("#idUsername").val(),
                                //  beforeSend: function() {
                                //         $("#idLoaderDiv").show();
                                // },
                                success: function(response){
                                    if( response ){
                                      var bookAppFlag = $("#idBookAppFlag").val();
                                      if(bookAppFlag == "1"){
                                                $.ajax({
                                                    type:"POST",
                                                    url:appointmentForm.attr("action"),
                                                    data:appointmentForm.serialize()+"&action=addapp",//only input
                                                    success: function(response){
                                                        if( response == 1 ){
                                                            //$("#form_add_app").trigger("reset");
                                                            window.location = 'thanks-appointment-done.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                                                        } else {
                                                            alert('try again!');
                                                        }
                                                    }
                                                });
                                      }
                                      alert("You are successfully Register");
                                       // $.dialog.alert("Hii You are successfully Register");

                                       window.location = 'index.php';
                                    } else {
                                        alert('try again!');
                                    }
                                }
                            });
                      $('#msg').html('<div class="alert alert-info"><strong>Confirmation!</strong> Mail will send you shortly !</div>');
                      $('#form_register').trigger("reset");
                      // $("#idSignUpModal").hide();
                      // window.location = 'lab.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });



        });
   
    // $('#idLoginModal').on('show.bs.modal', function (e) {
    //       // if (!data) return e.preventDefault() // stops modal from being shown
    //       var sTestName = $("#idTest").val();
    //       var sLocationName = $("#idLocation").val();
    //       if(sTestName == "select"){
    //           $("#idTestErr").fadeIn(1500);
    //           $("#idTestErr").html("<label style='color:red;'>Select Your Test</label>")
    //           $("#idTestErr").fadeOut(1500);
    //           // exit;
    //           // $('#idLoginModal').modal('lock');
    //           // return e.preventDefault();
    //           return false;
    //       }
    //        if(sLocationName == "select"){
    //           $("#idLocationErr").fadeIn(1500);
    //           $("#idLocationErr").html("<label style='color:red;'>Select Location</label>")
    //           $("#idLocationErr").fadeOut(1500);
    //           // exit;
    //           // return e.preventDefault();
    //           return false;
    //       }
    // })
    
 });

</script>
        <div class="classSliderHolder">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                      <div class="classHAbtTextWrapper">                         
                        <div class="classOSCFont classFrontText">
                          Your convenience is our priority...<br/>
                          And how do we do it? <br/>We provide Laboratory testing right at your doorstep.<br/><br/>
                          <div class="classBColor">Book your Appointment Now&nbsp;&nbsp;&nbsp;<i class="fa fa-hand-o-right"></i></div>
                        </div>
                       
                      </div>                     
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <!-- Form Div Start-->
                        <div class="auth">
                            <div id="big-form" class="well auth-box">
                            
                                <form method='post' id="on_form_appointment" action="userController.php">
                              
                                <fieldset>

                                  <!-- Form Name -->
                                  <legend><i class="fa fa-calendar"></i> Book Your Appointment</legend>
                                  <!-- Select Basic -->
                                  <div class="form-group">
                                     <div class="ui-widget">
                                        <select id="idTest" name="test" class="form-control js-example-basic-multiple" multiple="multiple">
                                             <?php
                                                foreach ($aAllTests as $key => $value) {
                                                  echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                                }
                                              ?>
                                        </select>
                                      </div>
                                    <div class="" id="idTestErr"></div>
                                  </div>
                                  <div class="form-group">
                                    <div class="ui-widget">
                                      <select id="idLocation" name="location" class="form-control js-example-basic-multiple" multiple="multiple">
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                    <div class="" id="idLocationErr"></div>
                                  </div>
                                  <div class="row">
                                        <div class="form-group col-md-6">
                                            <div class="">
                                                <input id="idAppointmentDate" name="appointmentDate" placeholder="Please Select Date " class="form-control" type="text">
                                            </div>
                                            <div class="" id="idAppDateErr"></div>
                                        </div> 
                                      <div class="form-group col-md-6">
                                        <label class=" control-label" for="appointmentTime">Appointment Time</label>
                                        <div class="">
                                         <input id="textinput" name="appointmentTime" placeholder="" class="form-control input-md" type="text" value="Between 7:00 am to 12:00pm" readonly/>
                                        </div>
                                      </div>
                                        
                                    </div>

                                                                  
                                 
                                  <!-- Button (Double) -->
                                  <!-- <div class="form-group">
                                    <label class=" control-label" for="button1id">Appointment Time</label>
                                    <div class="">
                                      <button id="idBookedSlot" name="bookedSlotss" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                       <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>                                      
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                    </div>
                                  </div> -->

                                  <div class="form-group">
                                    <div class="">
                                      <input type="hidden" name="bookAppFlag" id="idBookAppFlag" value="">
                                      <?php
                                      if($bLoginFlag){
                                        ?>
                                        <input type="button" id="idBookAppointmentOnLogin" name="bookAppointment" value="Book Appointment" class=" btn btn-success"></inputs>
                                        <?php
                                      }else{
                                      ?>
                                      <a href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"><input type="submit" id="idBookAppointment" name="bookAppointment" value="Book Appointment" class=" btn btn-success"></input></a>
                                       <?php 
                                      }
                                      ?>
                                      
                                      
                                      <span class="help-block">help</span>  
                                    </div>
                                  </div>

                                </fieldset>
                              </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Form Div End-->
                    </div>
                </div>    
            </div>
        </div>
        <div id="page-wrappera" class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Click To Request A Check Up</h3>
                    <a href="#"><img src="images/1.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Importance Of Check Ups</h3>
                    <a href="#"><img src="images/2.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Learn More</h3>
                    <a href="#"><img src="images/3.jpg" width="100%"/></a>

                </div>
                <!-- /.col-lg-12 -->
            </div>
<br/><br/><br/>
            <!-- /.row -->             
        </div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>
