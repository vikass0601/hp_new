<!-- <div class="container">
	<div class="alert alert-info">
    <strong>tutorial link !</strong> <a href="http://cleartuts.blogspot.com/2015/04/php-pdo-crud-tutorial-using-oop-with.html">cleartuts</a>!
	</div>
</div>
 -->
  
    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="bower_components/raphael/raphael-min.js"></script>
    <script src="bower_components/morrisjs/morris.min.js"></script>
    <script src="js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
<!-- Signup form modal code start -->
<div class="modal fade " id="idSignUpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="exampleModalLabel">Signup</h4>
      </div>
      <div class="modal-body">
        <!-- <form id="idSignupPanal" class="classSignupPanal" action="" method="post">
          
          <div class="form-group"> 
           <div class="input-group">           
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                   <input type="text" class="form-control" id="idName" name="user[name]" placeholder="Full name*" required>           
           </div> 
          </div>

          <div class="form-group"> 
           <div class="input-group">           
                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                   <input type="email" class="form-control" id="idEmail" name="user[email]" placeholder="User name - Email*" required>           
           </div> 
          </div>
          <div class="form-group"> 
           <div class="input-group">           
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                   <input type="text" class="form-control" id="idContact" name="user[contact]" placeholder="Contact Number*" required>
           
           </div> 
          </div>
          <div class="form-group"> 
            <div class="input-group"> 
                <span class="input-group-addon"><i class="fa fa-lock"></i> </span>         
              <input type="password" class="form-control" id="idPassword" name="user[password]" placeholder="Password">
            </div>  
          </div>

          <div class="form-group"> 
            <div class="input-group"> 
                <span class="input-group-addon"><i class="fa fa-lock"></i> </span>         
              <input type="password" class="form-control" id="idCPassword" name="user[signupcpassword]" placeholder="Confirm Password">
            </div>  
          </div>

          <div class="row">
                        
             <div class="col-lg-4">
                <button type="submit" class="classBtnSubmit btn btn-info" id="idBtnSubmit">Sign up</button>
             </div>
          </div>  

        </form> -->

        <form method='post' id="form_register" action="userController.php">
 
          <table class='table table-bordered'>
              <tr>
                  <td>Your Name</td>
                  <td><input type='text' name='name' id='name' class='form-control' value="" required></td>
              <input type='hidden' name='token' id='token' class='form-control' value="<?php echo Token::generate();?>" >
              </tr>
              <tr>
                  <td>Email</td>
                  <input type='hidden' name='id' id="id" class='form-control' value="">
                  <td><input type='text' name='username' id='username' class='form-control' value=""required ></td>
              </tr>
       
              <tr>
                  <td>Password</td>
                  <td><input type='password' name='password' id='password' class='form-control' value="" required></td>
              </tr>
       
              <tr>
                  <td>Password Again</td>
                  <td><input type='password' name='password_again' id='password_again' class='form-control' value="" required></td>
              </tr>
       
              <tr>
                  <td>Contact No</td>
                  <td><input type='number' name='contact' id='contact' class='form-control' value="" required></td>
              </tr>
              
              <tr>
                  <td>Address</td>
                  <td> <textarea class="form-control" rows="5" id="address" name="address"></textarea></td>
              </tr>
              
              <tr>
                  <td>Gender</td>
                  <td>
                   <div class="radio">
                       <label><input type="radio" name="gender" value="1" checked="checked">Male</label>
                      <label><input type="radio" name="gender" value="0">Female</label>
                   </div>
              </tr>
              
              <tr>
                  <td>DOB</td>
                  <td>
                  <div class='input-group date' id='datetimepicker1'>
                          <input type='text' name="dob" id="dob"  class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                  </div>    
                  </td>
              </tr>
              
              <tr>
                  <td>Age</td>
                  <td><input type='text' name='age' id='age' class='form-control' value="" required></td>
              </tr>
              
              <tr>
             
              <td colspan="2">
                 
                  <div id="edituser">
                      <button type="button" class="btn btn-primary" name="btn-update" id="idRegisterSubmit" name="register">
                <span class="glyphicon glyphicon-edit"></span>  Register
                      </button>
                      <a href="#"  data-dismiss="modal" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
                  </div> 
              
              </td>
                  
               
               
              </tr>
       
          </table>
      </form>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-12 control"> 
                  Already had a account
              <a class="btn btn-info btn-xs loginid" id="" href="#">
              <!-- <a class="btn btn-info btn-xs" id="loginid" href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"> -->
                  Login
              </a>
             
          </div>
      </div>    
      </div>
    </div>
  </div>
</div>
<!-- SignUp modal form code end-->

<!-- Login form modal code start -->
<div class="modal fade " id="idLoginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="exampleModalLabel">Login</h4>
        <div id="" class="msg"></div>
      </div>
      <div class="modal-body">
        <form method='post' id="form_login" action="userController.php">
     
        <table class='table table-bordered'>
     
            <tr>
                <td>Username</td>
                <td><input type='text' name='username' id='username' class='form-control' value=""required ></td>
            </tr>
     
            <tr>
                <td>Password</td>
                <td><input type='password' name='password' id='password' class='form-control' value="" required></td>
            </tr>
     
         
            <tr>
            <input type='hidden' name='token'  class='form-control' value="<?php echo Token::generate();?>" ></td>
            <td colspan="2">
                <div id="submituser">
                   <button type="button" class="btn btn-primary" name="btn-save" id="idLoginSubmit">
                       <span class="glyphicon glyphicon-plus"></span> Login
                   </button>  
                   <a href="#" id="" class="signupid btn btn-large btn-success">Register Me &nbsp;<i class="glyphicon glyphicon-forword"></i> </a>
                </div>
                      
            </td>
                
             
             
            </tr>
     
        </table>
    </form>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-12 control"> 
                  Forget password
              <a class="btn btn-info btn-xs forgetPasswordid" id="" href="#">
              <!-- <a class="btn btn-info btn-xs" id="loginid" href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"> -->
                  Click Here
              </a>
             
          </div>
      </div>    
      </div>
    </div>
  </div>
</div>
<!-- Login modal form code end-->

<!-- Forget form modal code start -->
<div class="modal fade " id="idForgetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="exampleModalLabel">Forget Password</h4>
        <div id="" class="msg"></div>
      </div>
      <div class="modal-body">
        <form method='post' id="form_forget" action="userController.php">
     
        <table class='table table-bordered'>
     
            <tr>
                <td>Email</td>
                <td><input type='text' name='username' id='username' class='form-control' value=""required ></td>
            </tr>
     
            <tr>
            <input type='hidden' name='token'  class='form-control' value="<?php echo Token::generate();?>" ></td>
	            <td colspan="2">
	                <div id="submituser">
	                   <button type="button" class="btn btn-primary" name="btn-save" id="idLoginSubmit">
	                       <span class="glyphicon glyphicon-plus"></span> Send
	                   </button>  
	                   <a href="#" id="#id" class="btn btn-large btn-success" data-dismiss="modal">Cancel</i> </a>
	                </div>
	                      
	            </td>
            </tr>
     
        </table>
    </form>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="col-md-12 control"> 
                  New User Registration 
              <a class="btn btn-info btn-xs signupid" id="" href="#">
              <!-- <a class="btn btn-info btn-xs" id="loginid" href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"> -->
                  Click Here
              </a>
             
          </div>
      </div>    
      </div>
    </div>
  </div>
</div>
<!-- Forget modal form code end-->

<!--Share this button start-->
<script>var sharebutton_is_horizontal = true; document.write('<script src="//cdn.script.to/share.js"></scr' + 'ipt>'); document.write("<div style='display: none'>");</script><a href="http://sharebutton.org/">youtube share button</a><script>document.write("</div>");</script>
<!--Share this button end-->

</body>

</html>