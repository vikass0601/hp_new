<html>
<head>
<title>Demo</title>

<style type="text/css">
	/**
 * Move in a circle without wrapper elements
 * Idea by Aryeh Gregor, simplified by Lea Verou
 */

#spinner { -webkit-animation-name: spinner; -webkit-animation-timing-function: linear; -webkit-animation-iteration-count: infinite; -webkit-animation-duration: 6s; animation-name: spinner; animation-timing-function: linear; animation-iteration-count: infinite; animation-duration: 6s; -webkit-transform-style: preserve-3d; -moz-transform-style: preserve-3d; -ms-transform-style: preserve-3d; transform-style: preserve-3d; } #spinner:hover { -webkit-animation-play-state: paused; animation-play-state: paused; }

@keyframes spin {
    from {
        transform:rotate(0deg);
    }
    to {
        transform:rotate(360deg);
    }
}

.smile {
  width: 100px;
  height: 100px;
  position: relative;
  top: 200px;
  left: 50%;
  margin: -20px;
  font-size: 100px;
  animation: rot 3s infinite linear;
  
  border-radius: 100%;
}
</style>
</head>
<body>
  <div class="smile">Coming Soon!</div>
<!-- <div id="container">
  <div id="cube" class="classDiv animate">
    <div class="classDiv"></div>
    <div class="classDiv"></div>
    <div class="classDiv"></div>
    <div class="classDiv"></div>
    <div class="classDiv"></div>
    <div class="classDiv"></div>
  </div>
</div> -->


</body>
</html>
	