<h1>Profile</h1>
<form method='post' id="form_upd_user" action="userController.php">

  <table class='table table-bordered'>
    <tr>
    <td>Name</td>
    <td>
    <input type='text' name='fname' id='fname' class='form-control' placeholder="First Name" title="Please enter your First name !" required>
    <input type='text' name='mname' id='mname'  placeholder="Middle Name"class='form-control'>
    <input type='text' name='lname' id='lname' class='form-control' placeholder="Last Name" title="Please enter your Last name !" required></td>
    </td>
    <input type='hidden' name='token' id='token' class='form-control' value="<?php echo Token::generate();?>" >
    </tr>
    <tr id="idErrName">

    </tr>
    <tr>
    <td>Email</td>
    <input type='hidden' name='id' id="id" class='form-control' value="" >
    <td><input type='email' name='username' id='username' placeholder="E-mail" class='form-control' title="Please enter your email address!" required disabled></td>
    </tr>
    <tr id="idErrEmail">
    </tr>  
    <tr  id="idErrEmailAddress">
    </tr>  
    <tr id="idErrPasswordAgain"></td></tr>        
    <tr id="idErrPasswordCheck"></td></tr>        
    <tr>
    <td>Mobile No</td>
    <td><input type='number' name='contact' id='contact' class='form-control' placeholder="Contact Number" maxlength="10" size="10" title="Please provide Contact no !" required></td>
    </tr>
    <tr id="idErrContact"></tr>        
    <tr>
      <td>Address</td>
      <td> 
      <fieldset>    
      <!-- Textarea -->
      <div class="control-group">
      <!--<label class="control-label" for="add_street">Street</label>-->
      <div class="controls">                     
      <textarea id="add_street" name="add_street" class="input-medium form-control" placeholder="Street" title="Please provide Street name !" name="add_street" required=""></textarea>
      </div>
      </div>

      <!-- Text input-->
      <div class="control-group">
      <!--<label class="control-label" for="add_area">Area</label>-->
      <div class="controls">
      <input id="add_area" name="add_area" placeholder="Area" class="input-medium form-control" required="" title="Please provide area !" type="text">

      </div>
      </div>

      <!-- Text input-->
      <div class="control-group">
      <!--<label class="control-label" for="add_landmark">Landmark</label>-->
      <div class="controls">
      <input id="add_landmark" name="add_landmark" placeholder="Landmark" class="input-medium form-control" type="text">

      </div>
      </div>

      <!-- Text input-->
      <div class="control-group">
      <!--<label class="control-label" for="area_city">City</label>-->
      <div class="controls">
      <input id="add_city" name="add_city" placeholder="City" class="input-medium form-control" required  value="Pune" type="text" readonly="">

      </div>
      </div>

      <!-- Text input-->
      <div class="control-group">
      <!--<label class="control-label" for="area_zipcode">Pincode</label>-->
      <div class="controls">
      <input id="add_zipcode" name="add_zipcode" placeholder="Zipcode" class="input-medium form-control" minlength="6" maxlength="6"  type="number" title="Please provide Pincode !" required>

      </div>
      </div>

      </fieldset>
      </td>
    </tr>
    <tr id="idErrAddress"></tr>        
    <tr>
      <td>Gender</td>
      <td>
      <div class="radio">
      <label><input type="radio" name="gender" value="M" checked="checked">Male</label>
      <label> <input type="radio" name="gender" value="F">Female</label>
      </div>
      <td/>
    </tr>
    <tr>
      <td>DOB</td>
      <td>
      <div class='input-group date' id='datetimepicker1'>
      <input type='text' name="dob" id="dob"  class="form-control classdob" />
      <span class="input-group-addon">
      <span class="glyphicon glyphicon-calendar"></span>
      </span>
      </div>    
      </td>
    </tr>
    <tr>
      <td>Age</td>
      <td><input type='number' name='age' id='age' class='form-control' value="" required></td>
    </tr>
    <tr>
      <td colspan="2">
      <div id="submituser">
      <button type="button" class="btn btn-primary" name="btn-upd" id="btn-upd">
      <span class="glyphicon glyphicon-edit"></span> Update
      </button>  
      <!-- <a href="login.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to login</a> -->
      </div>

      </td>
    </tr>

  </table>
</form>