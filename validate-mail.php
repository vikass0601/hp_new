<?php 
require_once "phpmails/PHPMailerAutoload.php";
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "vikas.sharma6572@gmail.com";
$mail->Password = "480986572    ";
$mail->SetFrom("vikas.sharma6572@gmail.com");
$mail->Subject = "Congratulations! You are now a member of Healthplease.";
$mail->Body = '
Hello ' . $name .',<br/>
We welcome you to HealthPlease.in you are now just a step away from completing the registration process.<br/>
<br/>
Click on the link below and your account will be activated.<br/><br/>
<a href="http://localhost/healthplease/activate.php?email=' . $to .'&code=' . $verificationCode .'" target="_blank">click me to activate </a> <br/><br/>
or you can copy or paste below url to activate your account    <br/><br/>
http://localhost/healthplease/activate.php?email=' . $to .'&code=' . $verificationCode .'
<br/>
<br/>
At Healthplease your convenience is our priority. We will not only make sure that you get the services right but we will provide them with great comfort.
We wish you a happy time here at Healthplease! In case you need help feel free to write us at <a href="mailto:help@healthplease.com">help@healthplease.com</a> or call us at <a href="tel:918600080252"> +91 8600080252</a> We will be glad to help you.<br/><br/>
Wish you a Great health!<br/>
<br/><br/>
Regards<br/>
Convenience Team';
$mail->AddAddress( $to );
//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    return true;
}

?>