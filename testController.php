<?php
require_once 'core/init.php';
if( !empty( Input::get('action') )){

    switch ( Input::get('action') ) {
    
        case 'add'  :
                        if ( Input::exists() ) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    'test_name' => array(
                                                        'require' => true,
                                                        'unique' => 'tests'
                                                     ),
                                                   'test_cat_id' => array(
                                                        'require' => true
                                                        
                                                     ),
                                                   'test_charge' => array(
                                                        'require' => true
                                                        
                                                     )
                                    )); 

                                   if( $validation->passed() ){
                                       $user = new Test();
                                       try{
                                          $user_status = $user->createTest( array( 
                                                   'test_name' => Input::get('test_name'),
                                                   'test_cat_id' => Input::get('test_cat_id'),
                                                   'test_charge' => Input::get('test_charge'),
                                                   'created_by' => 1,
                                                   'created_date' => date("Y-m-d h:i:sa"),
                                                   'updated_by' => 1,
                                                   'updated_date' => date("Y-m-d h:i:sa")
                                                   ));

                                            if( $user_status ){
                                               echo 1;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
            break;
        case 'upd': $user = new Test();
                    if (Input::exists()) {

                                    $validate = new Validate();
                                   $validation = $validate->check($_POST, array(
                                                    'test_name' => array(
                                                        'require' => true,
                                                    ),
                                                   'test_cat_id' => array(
                                                        'require' => true
                                                        
                                                     ),
                                                   'test_charge' => array(
                                                        'require' => true
                                                        
                                                     )
                                    )); 

                                   if( $validation->passed() ){
                                       $user = new Test();
                                       try{
                                          $user_status = $user->updateTest( Input::get('id'), array( 
                                                    'test_name' => Input::get('test_name'),
                                                    'test_cat_id' => Input::get('test_cat_id'),
                                                    'test_charge' => Input::get('test_charge'),
                                                    'updated_by' => 3,
                                                    'updated_date' => date("Y-m-d h:i:sa")
                                                   ));

                                            if( $user_status ){
                                               echo 1;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
                    
            
        break;
        
        case 'del': $user =new Test();
                  if (Input::exists()) {
                                            $user = new Test();
                                                try{
                                                   $user_status = $user->deleteTest( Input::get('id'));

                                                     if( $user_status ){
                                                        echo 1;
                                                     }

                                                } catch ( Exception $e ) {
                                                    echo $e;
                                                }
                    }
            break;
            
        case 'gettest': $user =new Test();
                            if (Input::exists()) {
                                                      $user->_db->get('tests',array( 'id', '=', Input::get('id') ));
                                                      $data = ( array )$user->_db->results();// print_r($data);
                                                      echo json_encode($data[0]);	
                              }
        break;
           
        case 'deletetest': $user =new Test();
                           $html ='';
                  if (Input::exists()) {
                              $html =  "<div class='container'><table class='table table-bordered'>
                                        <tr>
                                        <th>#</th>
                                        <th>Test Name</th>
                                        
                                        </tr>";
                              $user->_db->fetchData( "SELECT * FROM tests WHERE id=".Input::get('delete_id') );
                              $row = $user->_db->results();
                              
                                for($i=0; $i< $user->_db->count();$i++ ) {

                                      $html.='<tr><td>'.$row[$i]['id'].'</td>
                                       <td>'.$row[$i]['test_name'].'</td>
                                       </table></div><div class="container"><p>';
                                      
                                }
               
        if( Input::get( 'delete_id' ) ) {                       
            $html .='<form method="post" id="delUser" action="testController.php">
                    <input type="hidden" name="id" value="'.Input::get('delete_id').'" />
                    <button class="btn btn-large btn-primary" type="button" name="btn-del" id="btn-del">
                    <i class="glyphicon glyphicon-trash"></i> &nbsp; YES</button>
                    <a href="test.php?page_no='. Input::get('page_no') .'" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>
                    </form></p></div> ';
         } else {
             $html.='<a href="test.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp;</p></div> Back to index</a>';
         }
                                
          
         $html.='<script>
            $("#btn-del").click(function(){
                    var form=$("#delUser");
                    $.ajax({
                            type:"POST",
                            url:form.attr("action"),
                            data:form.serialize()+"&action=del",//only input
                            success: function(response){
                                if( response == 1 ){
                                    $(\'#msg\').html(\' <div class="alert alert-success"><strong>Success!</strong> record was deleted... </div>\');
                                    window.location = "test.php?page_no='. Input::get('page_no') .'";
                                   // $(\'#msg\').show();
                                } else {
                                    $(\'#msg\').html(\'<div class="alert alert-danger"><strong>Problem!</strong>\'+ response +\'... </div>\');
                                    //$(\'#msg\').show();

                                }
                            }
                            });
            });
            </script>';
         }
         echo $html;
                    
           break;   
           
        case 'pg':   $user = new Test();
            $page_no = (!empty( Input::get('page_no') ))? Input::get('page_no'): 1;
            echo '<table class="table table-bordered table-responsive">
                    <tr>
                    <th>#</th>
                    <th>Test Name</th>
                    <th>Test Category</th>
                    <th>Test Charge</th>
                    <th colspan="2" align="center">Actions</th>
                    </tr>';
            $user->viewTests($page_no);       
    
             break;        
        default: 
        break;
    
}
}//end of if
