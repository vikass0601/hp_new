         <?php 
         $aAllCat = getAllCategories();
         $aAllTests = getAllTests();
         $aAllLoc = getAllLocations();
         ?>               
                        <form method='post' id="formBookAppYour" class="classOSCAForm" action="appoinController.php">
                                
                            <table class='table table-bordered'>
                                <tr id="idUserName">
                                    <td>Your Name*</td>
                                <input type='hidden' name='appid' id='appid' value="<?php echo $id;?>">
                                    <td><input type='text' name='name' id='name' class='form-control' value="<?php echo $data[0]['name']; ?>" required></td>
                                        
                                </tr>
                                <tr id="idErrName"></tr>
                                <tr id="idRelationDiv" ></tr>
                                
                                
                                <tr>
                                    <td>Test*</td>
                                    <td>
                                        <select id="idTest" name="test[]" class="form-control selectpicker classTestSelect" multiple="">
                                            <?php  foreach( $UserData as $key => $value ){?>
                                                       <option value="<?php echo $UserData[$key]['test_id']; ?>" selected=""><?php echo $UserData[$key]['test_name']; ?></option>    
                                                   <?php } ?>
                                         
                                                  <?php
                                                foreach ($aAllTests as $key => $value) {
                                                  echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                                }
                                              ?>
                                        </select>
                                        <!--<input type="hidden" name="test" value="<?php //echo $_GET['testName'] ?>">-->                                            
                                            <!--<input type='text' name='testName' id='testName' class='form-control' value="<?php// echo $testName[0]['test_name']; ?>" required>-->
                                     </td>                      
                                </tr>
                                <tr id="idErrTest"></tr>

                                <tr>
                                    <td>Location*</td>
                                    <td>
                                        <select id="idLocation" name="location" class="form-control classLocationSelect">
                                        <option value='<?php echo $UserData[0]['loc_id']; ?>'><?php echo $UserData[0]['location_name'];?></option>
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                        <!--<input type="hidden" name="location" value="<?php //echo $_GET['locName']; ?>">-->                                                 
                                        <!--<input type='text' name='testLocation' id='testLoction' class='form-control' value="<?php //echo $locName['location_name'];?>" required></td>-->                      
                                    </td>     
                                </tr>
                                <tr id="idErrLocation"></tr>

                                <tr>
                                    <td>Email*</td>
                                    <input type='hidden' name='id' id="id" class='form-control' value="">
                                    <td><input type='email' name='username' id='idUsername' class='form-control' value="<?php echo $data1[0]['username']; ?>"required></td>
                                </tr>
                                <tr id="idErrEmail">
                                </tr>  
                                <tr  id="idErrEmailAddress">
                                </tr>  
                                <tr>
                                    <td>Appointment Date*</td>
                                    <td><input type='text' name='appointmentDate' id='idAppDate' class='form-control' value="<?php echo substr( $UserData[0]['date'], 0, -9);?>" required></td>
                                </tr>
                                <tr id="idErrAppDate"></tr>  
                                <tr>
                                    <td>Appointment Time*</td>
                                    <td><input type='text' name='appointmentTime' id='isAppTime' class='form-control' value="<?php echo $UserData[0]['time'];?>" required></td>
                                </tr>
                                <tr id="idErrApptime"></td></tr>  
                                <tr>
                                    <td>Doctor Name*</td>
                                    <td><input type='text' name='docName' id='isDocName' class='form-control' value="<?php echo $UserData[0]['doctor_name'];?>" required></td>
                                </tr>
                                <tr id="idErrDocName"></td></tr>         
                                <tr>
                                    <td>Mobile No*</td>
                                    <td class="input-group"> <div class="input-group-addon">+91</div><input type='number' name='contact' id='contact' class='form-control' value="<?php echo $data[0]['contact']; ?>" style="position: relative;  z-index: 1;" maxlength="10" size="10" required></td>
                                </tr>
                                <tr id="idErrContact"></tr>        
                                <tr rowspan="2">
                                    <td>Address*</td>
                                    <!--<td> <textarea class="form-control classAddress" rows="5" id="address" name="address"><?php //echo $data[0]['address']; ?></textarea></td>-->
                                    <td>
                                    <fieldset>    
                                        <!-- Textarea -->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_street">Street</label>-->
                                      <div class="controls">                     
                                          <textarea id="add_street" name="add_street" class="input-medium form-control" placeholder="Street" name="add_street" required=""><?php echo $data[0]['add_street']; ?>  </textarea>
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_area">Area</label>-->
                                      <div class="controls">
                                        <input id="add_area" name="add_area" placeholder="Area" class="input-medium form-control" value="<?php echo $data[0]['add_area']; ?>" required="" type="text">
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="add_landmark">Landmark</label>-->
                                      <div class="controls">
                                        <input id="add_landmark" name="add_landmark" placeholder="Landmark" class="input-medium form-control" value="<?php echo $data[0]['add_landmark']; ?>" required="" type="text">
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="area_city">City</label>-->
                                      <div class="controls">
                                          <input id="add_city" name="add_city" placeholder="City" class="input-medium form-control" required=""  value="Pune" type="text" readonly="">
                                      </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="control-group">
                                      <!--<label class="control-label" for="area_zipcode">Pincode</label>-->
                                      <div class="controls">
                                        <input id="add_zipcode" name="add_zipcode" placeholder="Zincode" class="input-medium form-control" value="<?php echo $data[0]['add_zipcode']; ?>" type="number" required>

                                      </div>
                                    </div>

                                    </fieldset>
                                </td>
                                </tr>
                                <tr id="idErrAddress"></tr>        
                                <tr>
                                    <td>Gender</td>
                                    <td>
                                     <div class="radio">
                                        <?php if($data[0]['gender'] == 'M'){ ?>
                                             <label><input type="radio" name="gender" value="M" checked="checked">Male</label>
                                             <label><input type="radio" name="gender" value="F">Female</label>
                                        <?php } else { ?>
                                             <label><input type="radio" name="gender" value="M" >Male</label>
                                             <label><input type="radio" name="gender" value="F" checked="checked">Female</label> 
                                        <?php } ?>
                                     </div>
                                </tr>
                                
                                <tr>
                                    <td>DOB</td>
                                    <td>
                                    <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' name="dob" id="dob"  value="<?php echo $data [0]['dob']; ?>" class="form-control classdob" disabled />
                                            <span class="input-group-addon" id="idSpanDob">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                    </div>    
                                    </td>
                                </tr>
                                <tr id="idErrDob"></tr>
                                <tr>
                                    <td>Age</td>
                                    <td><input type='text' value="<?php echo $data[0]['age'] ?> " name='age' id='age' class='form-control' required readonly></td>
                                </tr>
                                
                                <tr>
                               
                                <td colspan="2">
                                   
                                    <div id="edituser">
                                        <button type="button" class="btn btn-primary" name="btn-updateApp" id="editAppYour" name="register">
                                         <span class="glyphicon glyphicon-edit"></span> Update Appointment
                                        </button>
                                        
                                    </div> 
                                
                                </td>
                                    
                                 
                                 
                                </tr>
                         
                            </table>
                      </form>