<?php
include('base-header.php');
include('page-header.php');
$test = new Test();
?>
        <div class="classTopHeading">
        <div class="container">
            <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Welcome <span class="nameHolder">Admin</span></h1>
            </div>
        </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
        <div id="msg" class="container"></div>


<!--Add user start from here-->
<div class="container" id="addUser" name="addUser">
  <div class="row">
     <div class="col-lg-3 col-md-3">
       <?php
        include 'admin-menus.php';
       ?>
        <!-- /.panel-heading -->
    </div>
    <div class="col-lg-9 col-md-9">
      <div class="" id="div_add">
<a href="javascript:void(0);" class="btn btn-large btn-info" id="add"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add New Test</a>
</div>

<div class="clearfix"></div><br />
<!--Main div where content get loaded-->
<div class="" id="loadUser" name="loadUser">

</div>    
<!--Paging div will get content soon-->
<div class="" id="pagination">
    <table class='table table-bordered table-responsive'>
    <tr>
        <td colspan="7" align="center">
            <div class="pagination-wrap">
                <?php 
                    $page_no = '';
                    $query = "SELECT * FROM tests";       
                    $records_per_page=10;
                    $redirect = "testController.php";
                    $test->pagingLink( $query, $records_per_page, $page_no ,$redirect );//$crud->paginglink($query,$records_per_page); 
                ?>
            </div>
        </td>
    </tr>
    </table>    
</div>
     

<!--Add user start from here-->
<div class="" id="addUser" name="addUser">

<form method='post' id="form_add_user" action="testController.php">
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Test Name</td>
            <input type='hidden' name='id' id="id" class='form-control' value="">
            <td><input type='text' name='test_name' id='test_name' class='form-control' value=""required ></td>
        </tr>
     <?php $data = $test->_db->runQuery("SELECT 
                                                    `id`,
                                                    `category_name`
                                                 FROM 
                                                    `test_categories`
                                            ");
     ?>
        <tr>
            <td>Test Category</td>
            <td> <select class="form-control" id="test_cat_id" name="test_cat_id">
                <?php foreach($data as  $option) { ?>
                <option value="<?php echo $option['id'] ?>"> 
                <?php echo $option['category_name'] ?>
                </option>
                <?php }?>
                </select></td>
        </tr>
 
        <tr>
            <td>Test Charge</td>
            <td><input type='number' name='test_charge' id='test_charge' class='form-control' value="" required></td>
        </tr>
 
        
        <tr>
       
        <td colspan="2">
            <div id="submituser" class="pull-left">
               <button type="button" class="btn btn-primary" name="btn-save" id="submit">
                   <span class="glyphicon glyphicon-plus"></span> Create New Record
               </button>  
               <a href="test.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to index</a>
            </div>
            <div id="edituser" class="pull-left">
                &nbsp;<button type="button" class="btn btn-primary" name="btn-update" id="btn-update">
          <span class="glyphicon glyphicon-edit"></span>  Update this Record
                </button>
                <a href="test.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </div> 
        
        </td>
            
         
         
        </tr>
 
    </table>
</form>
</div>
<!--Add user ends here-->
<script>
    $(document).ready(function(){
       var page_no = 1; 
     $("#addUser").show();
     $("#div_add").show();
     
       $.ajax({
                type:"POST",
                url:'testController.php',
                data:"action=pg",//only input
                success: function(response){
                    $("#loadUser").html(response); 
                }
            });
     
     
    /*************************
     * To add new user
     */
    $("#add").click( function() {
        $("#loadUser").hide(); $("#pagination").hide();
        $("#addUser").show();
        $("#div_add").hide();
        $("#edituser").hide();
        $('#form_add_user').trigger("reset");
        $("#edituser").hide();
    }); 
    /***************************
     * Submit new user data
     */      
    $("#submit").on( "click", function(){
        var form=$("#form_add_user");
        $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+"&action=add",//only input
                success: function(response){
                    if( response == 1 ){
                        $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="test.php">HOME</a>!</div>');
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });
        });
    
    
    /********
    * For updation of user to load current user content into form
     */
     $(document).on("click", ".js-edituser", function(){
         var $this = $(this);
         page_no = $this.data('page_no');
        
       $.ajax({
                type:"POST",
                url:'testController.php',
                data:$this.data('value'),//only input
                success: function(response){
                    var obj = $.parseJSON( response );
                    $("#id").val(obj.id);
                    $("#test_name").val(obj.test_name);
                    $("#test_cat_id").val(obj.test_cat_id);
                    $("#test_charge").val(obj.test_charge);
                    $("#addUser").show();
                    $("#submituser").hide();
                    $("#edituser").show();
                    $("#loadUser").hide(); $("#pagination").hide();
                    $("#add").hide();
                    
                    
                }
            });
        
    });
    
    /******
    * Actual updation of content starts here
     */
    $("#btn-update").on("click", function(){
            var form=$("#form_add_user");
            $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize()+"&action=upd",//only input
                    success: function(response){
                        if( response == 1 ){
                            $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was updated successfully <a href="test.php?page_no='+ page_no +'">HOME</a>!</div>');
                            $('#msg').show();
                        } else {
                            $('#msg').html("<div class='alert alert-warning'><strong>SORRY!</strong> ERROR while updating record !</div>");
                            $('#msg').show();

                        }
                    }
            });
        });
    /******
    * updateis done here
     */
     
     /******
    * deletion is start from here 
     */ 
     $(document).on("click",".js-deleteuser", function(){
            $("#add").hide();
            $("#pagination").hide();
            $('#msg').html('<div class="alert alert-danger"><strong>Sure !</strong> to remove the following record ? </div>');
            var $this = $(this);
            page_no = $this.data('page_no');
                $.ajax({
                         type:"POST",
                         url:'testController.php',
                         data:$this.data('value'),//only input
                         success: function(response){
                             $("#loadUser").html(response);                  
                            }
                });

    });
    
    $(".js-pageDisplay").on("click",function(){
        $('.js-pageDisplay').removeClass("redColor");
        $(this).addClass("redColor");
        var $this = $(this);
         $.ajax({
                type:"POST",
                url:$this.data('url'),
                data:$this.data('param'),//only input
                success: function(response){
                     $("#loadUser").html(response);                  
                   }
            });
    });
 });
</script>

   </div>  
  </div>      
</div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>
