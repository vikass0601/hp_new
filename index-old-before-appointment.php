<?php
include('base-header.php');
include('page-header.php');
$aAllCat=getAllCategories();
$aAllTests=getAllTests();
$aAllLoc=getAllLocations();
?>
<script type="text/javascript">
            // When the document is ready
      $(document).ready(function () {
        //var datepicker = $.fn.datepicker.noConflict();
          $('#idAppointmentDate').datepicker({
              format: 'mm/dd/yyyy',
              startDate: '+1d'
          }); 
      
      });
  </script>
<script type="text/javascript">
$(document).ready(function(){
  $('.loginid').click(function(){   
        $("#idSignUpModal").modal('hide');
        $("#idLoginModal").modal('show');
         $("#idForgetPasswordModal").modal('hide');

  });
  $('.signupid').click(function(){   
        $("#idSignUpModal").modal('show');
        $("#idLoginModal").modal('hide');
         $("#idForgetPasswordModal").modal('hide');
         $('.modal').css("overflow","scroll");
  });
   $('.forgetPasswordid').click(function(){   
        $("#idForgetPasswordModal").modal('show');
        $("#idSignUpModal").modal('hide');
        $("#idLoginModal").modal('hide');
  });

});
</script>        
<script>
    $(document).ready(function(){
   
    /***************************
     * Submit new user data
     */      
     $("#idLoginSubmit").on( "click", function(){
        var form=$("#form_login");
        var form=$("#form_appointment");
        $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+'&action=login',//only input
                success: function(response){
                    if( response == 1 ){
                       window.location = 'lab.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });
        });
    
    
 });
</script>
<script>
    $(document).ready(function(){
   
    /***************************
     * Submit new user data
     */      
    $("#idRegisterSubmit").on( "click", function(){
        var form=$("#form_register");
       
         $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+'&action=register',//only input
                success: function(response){
                    if( response == 1 ){
                        $.ajax({
                                type:"POST",
                                url:"mail.php?name="+$("#name").val()+"&mailid="+$("#username").val(),
                                success: function(response){
                                    if( response ){

                                      $('#msg').html('<div class="alert alert-info"><strong>Hi!</strong> Mail sent successfully <a href="login.php">Login Now</a>!</div>');
                                     
                                    } else {
                                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                                    }
                                }
                            });
                      $('#msg').html('<div class="alert alert-info"><strong>Confirmation!</strong> Mail will send you shortly !</div>');
                      $('#form_register').trigger("reset");
                      // window.location = 'lab.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });
        });
    
    
 });
</script>
        <div class="classSliderHolder">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                      <div class="classHAbtTextWrapper">                         
                      <h1 class="classSBigFont">
                        Healthplease.in <br/>Your convenience is our priority...
                      </h1>
                      <div class="classSMediumFont">And how do we do it? <br/>We provide Laboratory testing right at your doorstep.<br/>
                        <div class="classAppointmentButton">Book your Appointment Now&nbsp;&nbsp;&nbsp;<i class="fa fa-hand-o-right"></i></div>
                      </div>
                       
                      </div>                     
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <!-- Form Div Start-->
                        <div class="auth">
                            <div id="big-form" class="well auth-box">
                              <form>
                                <fieldset>

                                  <!-- Form Name -->
                                  <legend><i class="fa fa-calendar"></i> Book Your Appointment</legend>
                                  <!-- Select Basic -->
                                  <div class="form-group">
                                    <label class=" control-label" for="selectbasic">Test</label>
                                    <div class="">
                                    
                                      <select id="idTest" name="test" class="form-control selectpicker">
                                        <option value='select'>Select Your Test</option>
                                        <?php
                                          foreach ($aAllTests as $key => $value) {
                                            echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class=" control-label" for="selectbasic">Your Location</label>
                                    <div class="">
                                      <select id="idTest" name="location" class="form-control">
                                        <option value='select'>Select Your Location</option>
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="form-group col-md-6">
                                            <label class=" control-label" for="appointmentDate">Appointment Date</label>  
                                            <div class="">
                                              <input id="idAppointmentDate" name="appointmentDate" placeholder="dd/mm/yyyy" class="form-control input-md" type="text">
                                            </div>
                                        </div> 
                                      <div class="form-group col-md-6">
                                        <label class=" control-label" for="appointmentTime">Appointment Time</label>
                                        <div class="">
                                         <input id="textinput" name="appointmentTime" placeholder="" class="form-control input-md" type="text" value="Between 7:00 am to 12:00pm" readonly/>
                                        </div>
                                      </div>
                                        
                                    </div>

                                                                  
                                 
                                  <!-- Button (Double) -->
                                  <!-- <div class="form-group">
                                    <label class=" control-label" for="button1id">Appointment Time</label>
                                    <div class="">
                                      <button id="idBookedSlot" name="bookedSlotss" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                       <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>                                      
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-success">Good Button</button>
                                      <button id="idBookedSlot" name="bookedSlot" class="btn btn-danger">Scary Button</button>
                                    </div>
                                  </div> -->

                                  <div class="form-group">
                                    <div class="">
                                      <a href="#idLoginModal" data-toggle="modal" data-keyboard="false" data-target="#idLoginModal"><input type="submit" id="idBookAppointment" name="bookAppointment" value="Book Appointment" class=" btn btn-success"></inputs></a>
                                      <span class="help-block">help</span>  
                                    </div>
                                  </div>

                                </fieldset>
                              </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Form Div End-->
                    </div>
                </div>    
            </div>
        </div>
        <div class="classGoldenDivider"></div>
        <div id="page-wrappera" class="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Click To Request A Check Up</h3>
                    <a href="#"><img src="images/1.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Importance Of Check Ups</h3>
                    <a href="#"><img src="images/2.jpg" width="100%"/></a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <h3 class="page-header classHeaderText">Learn More</h3>
                    <a href="#"><img src="images/3.jpg" width="100%"/></a>

                </div>
                <!-- /.col-lg-12 -->
            </div>
<br/><br/><br/>
            <!-- /.row -->             
        </div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>
