<?php
require_once 'core/init.php';
if( !empty( Input::get('action') )){

    switch ( Input::get('action') ) {
    
        case 'add'  :
                        if ( Input::exists() ) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    'lab_name' => array(
                                                        'require' => true,
                                                        'min' => 4,
                                                        'max' => 20,
                                                        'unique' => 'labs'
                                                     ),
                                                   'lab_location_id' => array(
                                                        'require' => true
                                                        
                                                     ),
                                                   'lab_details' => array(
                                                        'require' => true
                                                        
                                                     )
                                    )); 

                                   if( $validation->passed() ){
                                       $user = new Lab();
                                       try{
                                          $user_status = $user->createLab( array( 
                                                   'lab_name' => Input::get('lab_name'),
                                                   'lab_location_id' => Input::get('lab_location_id'),
                                                   'lab_details' => Input::get('lab_details'),
                                                   'created_by' => 1,
                                                   'created_date' => date("Y-m-d h:i:sa"),
                                                   'updated_by' => 1,
                                                   'updated_date' => date("Y-m-d h:i:sa")
                                                   ));

                                            if( $user_status ){
                                               echo 1;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
            break;
            case 'upd': $user = new Lab();
                    if (Input::exists()) {

                                    $validate = new Validate();
                                   $validation = $validate->check($_POST, array(
                                                    'lab_name' => array(
                                                        'require' => true,
                                                        'min' => 4,
                                                        'max' => 20,
                                                    ),
                                                   'lab_location_id' => array(
                                                        'require' => true
                                                        
                                                     ),
                                                   'lab_details' => array(
                                                        'require' => true
                                                        
                                                     )
                                    )); 

                                   if( $validation->passed() ){
                                       $user = new Lab();
                                       try{
                                          $user_status = $user->updateLab( Input::get('id'), array( 
                                                    'lab_name' => Input::get('lab_name'),
                                                    'lab_location_id' => Input::get('lab_location_id'),
                                                    'lab_details' => Input::get('lab_details'),
                                                    'updated_by' => 3,
                                                    'updated_date' => date("Y-m-d h:i:sa")
                                                   ));

                                            if( $user_status ){
                                               echo 1;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
                    
            
        break;
        
        case 'del': $user =new Lab();
                  if (Input::exists()) {
                                            $user = new Lab();
                                                try{
                                                   $user_status = $user->deleteLab( Input::get('id'));

                                                     if( $user_status ){
                                                        echo 1;
                                                     }

                                                } catch ( Exception $e ) {
                                                    echo $e;
                                                }
                    }
            break;
            
        case 'getlab': $user =new Lab();
                            if (Input::exists()) {
                                                      $user->_db->get('labs',array( 'id', '=', Input::get('id') ));
                                                      $data = ( array )$user->_db->results();// print_r($data);
                                                      echo json_encode($data[0]);	
                              }
        break;
           
        case 'deletelab': $user =new Lab();
                           $html ='';
                  if (Input::exists()) {
                              $html =  "<div class='container'><table class='table table-bordered'>
                                        <tr>
                                        <th>#</th>
                                        <th>Lab Name</th>
                                        
                                        </tr>";
                              $user->_db->fetchData( "SELECT * FROM labs WHERE id=".Input::get('delete_id') );
                              $row = $user->_db->results();
                              
                                for($i=0; $i< $user->_db->count();$i++ ) {

                                      $html.='<tr><td>'.$row[$i]['id'].'</td>
                                       <td>'.$row[$i]['lab_name'].'</td>
                                       </table></div><div class="container"><p>';
                                      
                                }
               
        if( Input::get( 'delete_id' ) ) {                       
            $html .='<form method="post" id="delUser" action="labController.php">
                    <input type="hidden" name="id" value="'.Input::get('delete_id').'" />
                    <button class="btn btn-large btn-primary" type="button" name="btn-del" id="btn-del">
                    <i class="glyphicon glyphicon-trash"></i> &nbsp; YES</button>
                    <a href="lab.php?page_no='. Input::get('page_no') .'" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>
                    </form></p></div> ';
         } else {
             $html.='<a href="lab.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp;</p></div> Back to index</a>';
         }
                                
          
         $html.='<script>
            $("#btn-del").click(function(){
                    var form=$("#delUser");
                    $.ajax({
                            type:"POST",
                            url:form.attr("action"),
                            data:form.serialize()+"&action=del",//only input
                            success: function(response){
                                if( response == 1 ){
                                    $(\'#msg\').html(\' <div class="alert alert-success"><strong>Success!</strong> record was deleted... </div>\');
                                    window.location = "lab.php?page_no='. Input::get('page_no') .'";
                                   // $(\'#msg\').show();
                                } else {
                                    $(\'#msg\').html(\'<div class="alert alert-danger"><strong>Problem!</strong>\'+ response +\'... </div>\');
                                    //$(\'#msg\').show();

                                }
                            }
                            });
            });
            </script>';
         }
         echo $html;
                    
           break;   
           
        case 'pg':   $user = new Lab();
            $page_no = (!empty( Input::get('page_no') ))? Input::get('page_no'): 1;
            echo '<table class="table table-bordered table-responsive">
                    <tr>
                    <th>#</th>
                    <th>Lab Name</th>
                    <th>Lab Location</th>
                    <th>Lab Details</th>
                    <th colspan="2" align="center">Actions</th>
                    </tr>';
                     $user->viewLabs($page_no);       
    
             break;        
        default: 
        break;
    
}
}//end of if
