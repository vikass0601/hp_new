/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){



  $('.loginid').click(function(){           
        $("#idLoginModal").modal('show');
         $("#idForgetPasswordModal").modal('hide');         

  });

   $('.forgetPasswordid').click(function(){   
        $("#idForgetPasswordModal").modal('show');
        
        $("#idLoginModal").modal('hide');
  });
  
  $("#idBookAppointment").click(function(){
    $("#idBookAppFlag").val("1");
  });
  
    /**
     * Submit new user data
     */

     $("#form_login").on( "submit", function () {
             var form=$("#form_login");
                 if ( false == form.valid() ) exit;
	
            var sTestName = $("#idTest").val();
            var sLocationName = $("#idLocation").val();
            var appointmentDate = $("#idAppointmentDate").val();
            var appTime  = $("#idAppTime").val();
        
            var appointmentForm=$("#on_form_appointment");
            $("#idLoginSubmit").html("<img src='images/loader.gif' width='25px'/>&nbsp;&nbsp;Loading...");

            $.ajax({
                           type:"POST",
                           url:form.attr("action"),
                           data:form.serialize()+'&action=login',//only input
                           success: function(response){
                               if( response == 1 ){
                                  var bookAppFlag = $("#idBookAppFlag").val();
                                  if(bookAppFlag == "1"){
                                       window.location = "confirmAppointment.php?testName="+sTestName+"&locName="+sLocationName+"&appDate="+appointmentDate+"&appTime="+appTime;
                                   }else {     
                                         window.location = 'index.php';                                
                                                      }
                                 } else {
                                   alert('try again! Invalid Credential...');
                                   $("#idLoginSubmit").html('<span class="glyphicon glyphicon-plus"></span> Login');
                               }
                           }


            });
        return false;
        });

  /**
   * User REgistration js code
   */
    $('#name').keydown(function (e) {
          if (e.ctrlKey || e.altKey) {
          e.preventDefault();
          } else {
          var key = e.keyCode;
          if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key == 9))) {
          e.preventDefault();
          }
          }
      });
        
      
     
          function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
      };



      /***************************
     * Submit new user data
     */      
    $("#idRegisterSubmit").on( "click", function(){

        var bookAppFlag = $("#idBookAppFlag").val();
        if(bookAppFlag == "1"){
             var sTestName = $("#idTest").val();
             var sLocationName = $("#idLocation").val();
             
            if(sTestName == "select"){
                
              $("#idTestErr").fadeIn(1500);
              $("#idTestErr").html("<label style='color:red;'>Select Your Test</label>");
              $("#idTestErr").fadeOut(1500);
              // exit;
              // $('#idLoginModal').modal('lock');
              // return e.preventDefault();
              return false;
          }
           if(sLocationName == "select"){
              $("#idLocationErr").fadeIn(1500);
              $("#idLocationErr").html("<label style='color:red;'>Select Location</label>");
              $("#idLocationErr").fadeOut(1500);
              // exit;
              // return e.preventDefault();
              return false;
          }
         } 
        
         var appointmentForm = $("#on_form_appointment");

        var form = $("#form_register");
         if ( false == form.valid() ) return;
       
       $("#idRegisterSubmit").html("<img src='images/loader.gif'  width='25px'/>&nbsp;&nbsp;Loading...");
         
         $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+'&action=register',//only input
                success: function(response){
                  
                    if( response == 1 ){
                                $('#msg').html('<div class="alert alert-info"><strong>Confirmation!</strong> Mail will send you shortly !</div>');
                            $.ajax({
                                type:"POST",
                                url:"mail.php?name="+$("#name").val()+"&mailid="+$("#idUsername").val(),
                                success: function(response){
                                    if( response ){
									
                                    $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="index.php">Please Login</a>!</div>');
                                     $("#idRegisterSubmit").html('<span class="glyphicon glyphicon-edit"></span>  Register');
                                      alert("You are successfully Register");
                                       // $.dialog.alert("Hii You are successfully Register");

                                       window.location.href = window.location.href;
                                    } else {
                                      $("#idRegisterSubmit").html('<span class="glyphicon glyphicon-edit"></span>  Register');
                                       $('#msg').html('<div class="alert alert-danger"><strong>!Cant Send you mail at this moment</strong></div>');
                                    }
                                }
                            });
                          //alert("You are successfully Register");
                          // $.dialog.alert("Hii You are successfully Register");
                        //  window.location = 'confirmAppointment.php';
                     // $('#msg').html('<div class="alert alert-info"><strong>Confirmation!</strong> Mail will send you shortly !</div>');
                       $('#form_register').trigger("reset");
                       $("#idSignUpModal").hide();
                       
                      // window.location = 'lab.php';// $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                    } else {
                         $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                         $("#idRegisterSubmit").html('<span class="glyphicon glyphicon-edit"></span>  Register');
                    }
                }
            });
           return false; 
 });
   
  
    
 });

            // When the document is ready
      $(document).ready(function () {
        // var datepicker = $.fn.datepicker.noConflict();
          $('#idAppointmentDate').datepicker({
              format: 'dd-mm-yyyy',
              startDate: '+1d',
              minDate:'1',
              maxDate:'+2m',
              viewMode: "months", 
              changeMonth: true,
               autoclose: true
          }); 
    
     /**
      * Code Added by vikas to select combobox runtime
      */  
          $( "#idTest" ).select2( {
                placeholder: "Select Your Test.."
          });
          
          
          $( ".classTestSelect" ).select2( {
                placeholder: "Select Your Test.."
          });

           $( ".classLocationSelect" ).select2( {
                placeholder: "Select Your Test..",
          });

           

          $( "#idLocation" ).select2( {
                placeholder: "Select Your Location..",
          });
          
 
          $(".classdob").datepicker({
             format: 'dd-mm-yyyy',
            defaultDate:'1989-01-01',
            maxDate: 0,
            changeMonth: true, 
            changeYear: true,
             autoclose: true,
             viewMode: "months", 
            /*yearRange: '1900:' + new Date().getFullYear()*/
            yearRange: "c-100:c+25"
          });

            $('#idSpanDob').click(function(){
                $(this).closest('.input-group').find('.hasDatepicker').focus();
            }).css('cursor','pointer');


          function generateDate( date ) {
            var tempDate = date.split( '/' );
            return tempDate[2] + '-' + tempDate[0] + '-' + tempDate[1];
          }

          //Calculate age ...
               $('#dob').change(function(){
                   var tempAge = generateDate($(this).val());
                   var dob = new Date( tempAge );
                   var today = new Date();
                   var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                   $('#age').val(age);

              });   
      });