<?php
include('base-header.php');
include('page-header.php');
$user = new User();
?>
        <div class="classTopHeading">
        <div class="classFullContainer">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h1 class="page-header">Welcome <span class="nameHolder">Admin</span></h1>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-thumbs-up fa-2x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" id="total"></div>
                                            <div>Total Appointments</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="panel panel-today">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-tasks fa-2x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" id="today"></div>
                                            <div>Today Appointment</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="panel panel-pending">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-thumbs-o-down fa-2x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" id="pending"></div>
                                            <div>Pending Appointment</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="panel panel-cancel">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-thumbs-down fa-2x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge" id="cancel"></div>
                                            <div>Cancelled Appointment</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
       
<!--Add user start from here-->
<div class="classFullContainer" id="addUser" name="addUser">
    <div class="row">
     <div class="col-lg-3 col-md-3">
       <?php
        include 'admin-menus.php';
       ?>
        <!-- /.panel-heading -->
    </div>
    <div class="col-lg-9 col-md-9">
<script type="text/javascript">
            // When the document is ready
      $(document).ready(function () {
          
          $('.classDates').datepicker({
              format: "dd-mm-yyyy"
          });  
      
      });
  </script>
  
  
   <table class='table table-bordered table-responsive'>
  <div id="msg" class="container"></div>
        <tr>
            <td colspan="2">Reports</td>
             <td colspan="3">
                <!--<a id="get_pdf" value="0" href="javascript:void(0)" class="btn btn-info btn-xs js-gen_report"><span class="glyphicon glyphicon-download-alt"></span> Download PDF</a>-->
                <a id="get_excel" value="1" href="javascript:void(0)" class="btn btn-info btn-xs js-gen_report"><span class="glyphicon glyphicon-download-alt"></span> Download Excel</a>
             <td>
             
        </tr>
        <tr>
            <td>
                <div class='input-group date'>
                    <input type='text' id='fromdate' class="classDates form-control" placeholder="From" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </td>
            <td>
                <div class='input-group date'>
                <input type='text' id='todate' class="classDates form-control"  placeholder="To" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                </div>
            </td>
            <td>
                <button type="button" class="btn btn-primary js-gen_report" name="btn-save" data-value="1">
                   <span class="glyphicon glyphicon-plus"></span> Generate Report
                </button>
            
                <button type="button" class="btn btn-primary js-gen_report" name="btn-save" data-value="2">
                   <span class="glyphicon glyphicon-plus"></span> Today Listing
                </button>
            </td>
           
        </tr>
 
    </table>    
  <div id="showReportData"></div>
  

        </div>
      </div>
   </div>  
  </div>      
</div>
        <!-- /#page-wrapper -->
<script>
    $(document).ready(function(){
var user =<?php echo Session::get( $user->_sessionName);?>;
       $.ajax({
                type:"POST",
                url:'userController.php',
                data:{ action:'viewprofile',
                       id: user
                    
                     },//only input
                success: function(response){
                    var obj = $.parseJSON( response );
                    $("#id").val(obj.id);
                    $("#username").val(obj.username);
//                    $("#password").val(obj.password);
//                    $("#password_again").val(obj.password);
                    $("#name").val(obj.name);
                    $(".nameHolder").html(obj.name);
                    $("#contact").val(obj.contact);
                    $("#age").val(obj.age);
                    $("#dob").val(obj.dob);
                    if(obj.gender){  $('input:radio[name=gender]')[0].checked = true; } else { $('input:radio[name=gender]')[1].checked = true;  }
                   
                }
            });
            
            /**
             * Load Summary for admin
             */
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();

            var output = d.getFullYear() + '-' +
                ((''+month).length<2 ? '0' : '') + month + '-' +
                ((''+day).length<2 ? '0' : '') + day;

            $.ajax({
                type:"POST",
                url:'reportsController.php',
                data: {
                    action:'load_summary',
                    date : output
                },
                success: function(response){
                     var json = $.parseJSON( response );
                     $('#total').html( json[0].total );
                     $('#pending').html( json[0].pen );
                     $('#cancel').html( json[0].can );
                     $('#today').html( json[0].today );
                }
            });
       
            
    /******
    * Actual updation of content starts here
     */
    $("#btn-upd").on("click", function(){
            var form=$("#form_upd_user");
            $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize()+"&action=updprofile",//only input
                    success: function(response){
                        if( response == 1 ){
                            $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was updated successfully <a href="login.php">HOME</a>!</div>');
                            $('#msg').show();
                             $('#form_upd_user').trigger("reset");
                        } else {
                            $('#msg').html("<div class='alert alert-danger'><strong>SORRY!</strong> ERROR while updating record !<br>"+ response  +"</div>");
                            $('#msg').show();

                        }
                    }
            });
        });
        
    /***************************
     * Submit new Appointment data
     ******/      
    $("#add-app").on( "click", function(){
        var form=$("#form_add_app");
        $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize()+"&action=addapp",//only input
                success: function(response){
                    if( response == 1 ){
                        $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was inserted successfully <a href="user.php">HOME</a>!</div>');
                        $("#form_add_app").trigger("reset");
                    } else {
                        $('#msg').html('<div class="alert alert-danger">'+response+'</div>');
                    }
                }
            });
        });
        
           
           /****
            * Load fix time from times table
            */
          $.ajax({
                    type:"POST",
                    url:'appoinController.php',
                    data:"action=gettime",//only input
                    success: function(response){
                       // alert(response);
                       var text = '';
                       var i =0;
                        var json = $.parseJSON( response );
                        for (i = 0; i < json.length; i++) {
                            text += '<div class="checkbox"><label><input type="checkbox" name="time[]" value="' + json[i].id + '">'+ json[i].time +'</input></label></div>'+ "<br>";
                        }
                        $('#laodTime').html(text);

                    }
            });
            
            /**
            * Added by vikas on 24-may-2015
            * to generate reports
             */
             
          $( ".js-gen_report" ).on( "click", function() {
                var type_of_report;
                var $this = $( this );
                var sdate = $('#fromdate').datepicker("getDate");
                var edate = $('#todate').datepicker("getDate");
               
                if (sdate > edate) {
                   $('#msg').html('<div class="alert alert-danger" style="width:600px">! Problem with date range</div>'); 
                   exit;
                }
                
                var $this = $( this );
                var fromdate = generateDate( $( '#fromdate' ).val() );
                var todate = generateDate( $( '#todate' ).val() );
                var type = 0;
                
                if( 2 === $this.data( "value" ) ) {
                    type = 1;
                    fromdate = 'n/a';
                    var fullDate = new Date()
                    //convert month to 2 digits
                    var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
                    todate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate() ;
                }
              
          if( 0 == $this.attr( 'value' ) ) {  type_of_report = 'get_pdf'; }
          if( 1 == $this.attr( 'value' ) ) {  type_of_report = 'get_excel'; }
                    
        $.ajax({
                        type: "POST",
                        url : "reportsController.php",
                        data: { 
                              fromdate : fromdate,
                              todate : todate,
                              type :type,
                              action: 'gen_report',
                              type_of_report:type_of_report
                              },//only input
                        success: function(response){
                              
                                $( '#showReportData' ).html( response );
                                if( 'get_excel' == type_of_report ) {
                                    window.location.href = 'downloads/HealthPlease_<?php echo date('Y-m-d'); ?>.xls';
                                }
                        }
                    });
          }); 
          
          function generateDate( date ) {
            var tempDate = date.split( '/' );
            return tempDate[2] + '-' + tempDate[0] + '-' + tempDate[1];
          }
        
           /**
    * To process appotnment by admin side 
     */
     /**
            * Added by vikas on 14-june-2015
            * to generate reports
             */
             
          $( document ).on( "click", ".js-processapp",   function() {
                
                var $this = $( this );
                $(this).html('<img src="images/loader.gif" id="idLoaderGif" class="classLoaderImg">..');
                 $.ajax({
                        type: "POST",
                        url : "reportsController.php",
                        data: { 
                              action : 'processapp',
                              id : $this.data( 'value' ),
                              status : $this.data( 'status' )
                             
                              },//only input
                        success: function(response){
                            //alert(response);
                              if( response ) {
                     
                                    var fromdate = generateDate( $( '#fromdate' ).val() );
                                    var todate = generateDate( $( '#todate' ).val() );

                                    $.ajax({      
                                        type: "POST",
                                        url : "reportsController.php",
                                        data: { 
                                            fromdate : fromdate,
                                            todate : todate,
                                            action: 'gen_report'

                                        },//only input
                                        success: function(response){

                                                    $( '#showReportData' ).html( response );
                                        }
                                     });
                            }
                        }
                });
          }); 
        
        
    });
    
 
          
                
          
            
   
</script>

<?php
include('page-footer.php');
include('base-footer.php');
?>
