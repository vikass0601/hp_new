<?php
include('base-header.php');
include('page-header.php');
$user = new User();
$aAllCat = getAllCategories();
$aAllTests = getAllTests();
$aAllLoc = getAllLocations();
$appStatus = getAllStatus(); 
?>

    <div class="classTopHeading" style="background:#29BAE9;height: 110px;">
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-3 col-md-3  hidden-sm hidden-xs">
              <div class="text-center classUImgHolder">
                <img src="images/default-user.png" class="classAvatar avatar img-circle img-thumbnail" alt="avatar">
                <div class="classIContolHolder">
                  <h3 class="nameHolder"></h3>
                  <h6>Upload photo...</h6>
                  <input type="file" class="text-center center-block well well-sm" style="width: 225px;">
                </div>     
              </div>
              <!-- /.panel-heading -->
          </div>
          <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-xs-12">
              <h1 class="page-header">Welcome &nbsp;<span class="nameHolder"><?php echo $fname; ?></span></h1>
            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="background:#29BAE9;border-radius: 5px 5px 0 0;">
              <li class="classTabColor active" id="idProfileTab"><a href="#profile" data-toggle="tab">Profile</a></li>
              <li class="classTabColor" id="idBookTab"><a href="#book" data-toggle="tab">Book</a></li>
              <li class="classTabColor" id="idHistoryTab"><a href="#history" data-toggle="tab">History</a></li>
            </ul>          
          </div>
          <div class="col-xl-2 col-lg-2 col-md-2  hidden-sm hidden-xs" style="margin-top: 2px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-flask fa-3x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge" id="idTestCount"></div>
                            <div>Pending Test!</div>
                        </div>
                    </div>
                </div>

                <a href="#history" id="idGoToHistory" data-toggle="tab">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    <div id="msg" class="container"></div>


<!--Add user start from here-->
<div class="classFullContainer" id="addUser" name="addUser"> 
  <div class="row">
     
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-xs-12">
      <div id="content">
          <div id="my-tab-content" class="tab-content">
              <div class="tab-pane active" id="profile">
                 <?php 
                    include('profile-form.php');
                  ?>
              </div>
              <div class="tab-pane" id="book">
                  <h1></h1>
                 <form method='post' id="on_form_appointment" action="userController.php">
                              
                                <fieldset>

                                  <!-- Form Name -->
                                  <legend><i class="fa fa-calendar"></i> Book Your Appointment</legend>
                                  <!-- Select Basic -->
                                  <div class="row">
                                  <div class="form-group  col-md-4">
                                    <label class=" control-label" for="selectbasic">Test</label>
                                   <div class="ui-widget classBookInProfile">
                                        <select id="idTest" name="test" class="form-control selectpicker">
                                          <option value="select" selected="">Select Your Test</option>
                                                  <?php
                                                foreach ($aAllTests as $key => $value) {
                                                  echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                                }
                                              ?>
                                        </select>
                                      </div>
                                    <div class="" id="idTestErr"></div>
                                  </div>

                                  <div class="form-group  col-md-3">
                                    <label class=" control-label" for="selectbasic">Your Location</label>
                                    <div class="ui-widget classBookInProfile">
                                      <select id="idLocation" name="location" class="form-control">
                                        <option value='select'>Select Your Location</option>
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                    <div class="" id="idLocationErr"></div>
                                  </div>
                                        <div class="form-group col-md-2">
                                            <label class=" control-label" for="appointmentDate">Appointment Date</label>  
                                            <div class="">
                                              <input id="idAppointmentDate" name="appointmentDate" placeholder="dd/mm/yyyy" class="form-control input-md" type="text">
                                            </div>
                                            <div class="" id="idAppDateErr"></div>
                                        </div> 
                                      <div class="form-group col-md-3">
                                        <label class=" control-label" for="appointmentTime">Appointment Time</label>
                                        <div class="">
                                         <input id="idAppTime" name="appointmentTime" placeholder="" class="form-control input-md" type="text" value="Between 7:00 am to 12:00pm" readonly/>
                                        </div>
                                      </div>   
                                    </div>                                     
                                    <div class="row">
                                      <div class="form-group col-md-2 pull-right">
                                         <label class=" control-label" ></label>
                                        <div class="">                                         
                                            <input type="button" id="bookFromUserProfile" name="bookFromUserProfile" value="Book Appointment" class=" btn btn-success"></inputs>                                          
                                        </div>
                                      </div>
                                  </div>
                                </fieldset>
                              </form>
              </div>
              <div class="tab-pane" id="history">
                  <h1>History</h1>
                  <div id="editAppointment">
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <!-- Form Div Start-->
                        <div class="auth">
                            <div id="big-form" class="well auth-box">
                            
                                <form method='post' id="on_form_appointment" action="userController.php">
                              
                                <fieldset>

                                  <!-- Form Name -->
                                  <legend><i class="fa fa-calendar"></i> Your Appointment Details </legend>
                                  <div class="alert alert-info"><strong>!You just Cancel your appointment here.</strong></div>
                                  <div id="info"></div>
                                  
                                  <!-- Select Basic -->
                                  <div class="form-group">
                                      <input type="hidden" id="app_id" value=""></input>
                                    <label class=" control-label" for="selectbasic">Test</label>
                                   <div class="ui-widget">
                                       <select id="test" name="test" class="form-control"  style="width:400px" disabled>
                                             <?php
                                                foreach ($aAllTests as $key => $value) {
                                                  echo "<option value={$value['id']}>►&nbsp;&nbsp;{$value['test_name']} &nbsp;&nbsp;|&nbsp;&nbsp;Fees Rs. {$value['test_charge']}</option>";
                                                }
                                              ?>
                                        </select>
                                      </div>
                                    <div class="" id="idTestErr"></div>
                                  </div>
                                  <div class="form-group">
                                    <label class=" control-label" for="selectbasic">Your Location</label>
                                    <div class="ui-widget">
                                        <select id="location" name="location" class="form-control" style="width:400px" disabled>
                                        <?php
                                          foreach ($aAllLoc as $key => $value) {
                                            echo "<option value={$key}>►&nbsp;&nbsp;{$value}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                    <div class="" id="idLocationErr"></div>
                                  </div>
                                  <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class=" control-label" for="appointmentDate">Appointment Date</label>  
                                            <div class="">
                                                <input id="idApp" name="appointmentDate" placeholder="" maxlength="10" value="" class="" style="width:400px" class="form-control" type="text" disabled>
                                            </div>
                                            <div class="" id="idAppDateErr"></div>
                                        </div> 
                                      
                                      <div class="form-group col-md-6">
                                        <label class=" control-label" for="appointmentTime">Appointment Time</label>
                                        <div class="">
                                         <input id="idAppTime" name="appointmentTime" placeholder="" class="form-control input-md" type="text" value="Between 7:00 am to 12:00pm" readonly/>
                                        </div>
                                      </div>
                                      
                                       <div class="form-group col-md-6">
                                            <label class=" control-label" for="appointmentTime">Status</label>
                                            <div class="">
                                             <select class='form-control' id="status" name="status">
                                              <?php foreach($appStatus as  $option) { if( 3 == $option['id']) continue;?>
                                              <option  value="<?php echo $option['id'] ?>"> 
                                              <?php echo $option['name'] ?>
                                              </option>
                                              <?php }?>
                                             </select>
                                            </div>
                                                                              
                                    </div>

                                                                  
                                 
                                  <div class="form-group">
                                    <div class="">
                                   
                                        <input type="button" id="cancelAppoin" name="cancelAppoin" value="Cancel Appointment" class=" btn btn-success"></inputs>
                                    </div>
                                      <span class="help-block">help</span>  
                                 </div>
                                  </div>

                                </fieldset>
                              </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Form Div End-->
                    </div>
                  </div>
                  <div id="user_history"></div>
              </div>
              
          </div>
      </div>
   </div> 
    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <h3>Recent CheckUps</h3>
          <span id="recent_check_up"></span>
<!--          <div class="classROItem" id="recent_check_up">xxxxx<div class="pull-right">xx-xx-xxxx</div></div>-->
          <hr/>
    </div> 
  </div>      
</div>
        <!-- /#page-wrapper -->
<script>
    $(document).ready(function(){
       $("#editAppointment").hide();
       $.ajax({
                type:"POST",
                url:'userController.php',
                data:{ action:'viewprofile',
                       id:<?php echo Session::get( $user->_sessionName);?>
                    
                     },//only input
                success: function(response){
                    var obj = $.parseJSON( response );
                    $("#id").val(obj.id);
                    $("#username").val(obj.username);
//                    $("#password").val(obj.password);
//                    $("#password_again").val(obj.password);
                    $("#fname").val(obj.fname);
                    $("#mname").val(obj.mname);
                    $("#lname").val(obj.lname);
                    $("#add_street").val(obj.add_street);
                    $("#add_area").val(obj.add_area);
                    $("#add_landmark").val(obj.add_landmark);
                    $("#add_city").val(obj.add_city);
                    $("#add_zipcode").val(obj.add_zipcode);
                    $(".nameHolder").html(obj.name);
                    $("#contact").val(obj.contact);
                    $("#age").val(obj.age);
                    $("#dob").val(obj.dob);
                    if(obj.gender == "M"){ $('input:radio[name=gender]')[0].checked = true; } else { $('input:radio[name=gender]')[1].checked = true;  }
                   
                }
            });
        /**
        * get user History apoointments
         */    
            
        $.ajax({
                type:"POST",
                url:'userController.php',
                data:{ action:'appointHistory',
                       user_id:<?php echo Session::get( $user->_sessionName);?>
                     },//only input
                success: function( response ){
                    $('#user_history').html( response );                    
                }
            });    
        
     /**
     * Get Recent check up information of user
     * added by vikas 03-jul-2015 
     */
     
     $.ajax({
                type:"POST",
                url:'userController.php',
                data:{ action:'getRecentCheckup',
                       user_id:<?php echo Session::get( $user->_sessionName);?>
                     },//only input
                success: function( response ){
                    $('#recent_check_up').html( response );                    
                }
            });
            
       $.ajax({
                type:"POST",
                url:'userController.php',
                data:{ action:'getPendingTestCount',
                       user_id:<?php echo Session::get( $user->_sessionName);?>
                     },//only input
                success: function( response ){
                    var obj = $.parseJSON( response );
                    $("#idTestCount").html(obj[0].cnt);                    
                }
            });      
     
       /**
        * Update Appointment from user
        */    
       
            $(document).on("click", ".js-editappointment", function(){
                var $this = $(this);
               
               //alert("hii");return;
              $.ajax({
                       type:"POST",
                       url:'appoinController.php',
                       data:$this.data('value'),//only input
                       success: function(response){
                        
                           var obj = $.parseJSON( response );
                           $( "#location" ).val( obj[0][0].loc_id );
                           $( "#idApp" ).val( obj[0][0].date );
                           $( "#app_id" ).val( obj[0][0].id );
                           $( "#status" ).val( obj[0][0].status_id );
                           $('#test option[value="' + obj[1][0].test_id + '"]').prop('selected', true);
                           if ( 3 == obj[0][0].status_id ) {
                               $("#cancelAppoin").attr("disabled","disabled");
                               $("#status").attr("disabled","disabled");
                           }    
                           
                           $("#editAppointment").show();
                           $("#user_history").show();
                           
                           
                       }
                   });

           });
           
        /**
        * Delete appointment from user side
         */   
        $(document).on("click", "#cancelAppoin", function(){
                if ( 1 == $( "#status" ).val() ) { alert( "You are not authorised to set this setting" ); return; }

              $.ajax({
                       type:"POST",
                       url:'appoinController.php?action=cancelapp&id='+ $( "#app_id" ).val() +'&status='+$( "#status" ).val(),
                       success: function(response){
                           if( 1 == response ){ 
                               $('#info').html('<div class="alert alert-info"><strong>! Your appointment is Cancelled successfully !</div>');
                               window.location.href = window.location.href;
                           } else $('#info').html('<div class="alert alert-danger"><strong>Sorry!</strong> Can\'t cancel your appointment right now. /div>');
                       }
                   });

           });   

    /******
    * Actual updation of content starts here
     */
    $("#btn-upd").on("click", function(){
            var form=$("#form_upd_user");

            if ( false == form.valid() ) return;
            $("#btn-upd").html('<img src="images/loader.gif" id="idLoaderGif" class="classLoaderImg"> updating..');

            $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize()+"&action=updprofile",//only input
                    success: function(response){
                        if( response == 1 ){
                            $('#msg').html('<div class="alert alert-info"><strong>WOW!</strong> Record was updated successfully <a href="login.php">HOME</a>!</div>');
                            $('#msg').show();
                            window.location.href = window.location.href;
                        } else {
                            $('#msg').html("<div class='alert alert-danger'><strong>SORRY!</strong> ERROR while updating record !<br>"+ response  +"</div>");
                            $('#msg').show();
                            $('#btn-upd').html('<span class="glyphicon glyphicon-edit"></span> Update');
                        }
                    }
            });
        });
        
       
/**
* to set appointment from user scrren
 */            
            $("#bookFromUserProfile").on( "click", function(){

          var sTestName = $("#idTest").val();
          var sLocationName = $("#idLocation").val();
          if(sTestName == "select"){
              $("#idTestErr").fadeIn(1500);
              $("#idTestErr").html("<label style='color:red;'>Please select your test</label>")
              $("#idTestErr").fadeOut(1500);
              return false;
          }
           if(sLocationName == "select"){
              $("#idLocationErr").fadeIn(1500);
              $("#idLocationErr").html("<label style='color:red;'>Please select location of test</label>")
              $("#idLocationErr").fadeOut(1500);
              return false;
          }

          appointmentDate = $("#idAppointmentDate").val();
          if(appointmentDate == ""){
            $("#idAppDateErr").fadeIn(1500);
              $("#idAppDateErr").html("<label style='color:red;'>Please select date</label>")
              $("#idAppDateErr").fadeOut(1500);
             return false; 
          }

          var appTime  = $("#idAppTime").val();
          var appDetails = "";
          window.location.href="confirmAppointment.php?testName="+sTestName+"&locName="+sLocationName+"&appDate="+appointmentDate+"&appTime="+appTime;

    
    });

        
        
        
    });
    
    
   
</script>

<!--TAB Script For History code start-->
<script type="text/javascript">
  $(document).ready(function(){
    $("#idGoToHistory").click(function(){
      $("#idProfileTab").removeClass('active');
      $("#idBookTab").removeClass('active');
      $("#idHistoryTab").addClass('active');
    });
  });
 
</script>
<!--TAB Script For History code end-->



<?php
include('page-footer.php');
include('base-footer.php');
?>
