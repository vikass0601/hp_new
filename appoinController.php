<?php
require_once 'core/init.php';
if( !empty( Input::get('action') )){

    switch ( Input::get('action') ) {
        
        case 'viewappointment':
        break;
    
        case 'gettest':
                            $test =new Test();
                            if (Input::exists()) {
                                                      $test->_db->get( 'tests',array( 'test_cat_id', '=', Input::get('test_cat_id') ));
                                                      $data = $test->_db->results();
                                                      echo json_encode($data);	
                              }
            break;
            
        case 'gettime':
                            $test =new Test();
                            if (Input::exists()) {
                                                      $data = $test->_db->runQuery("Select * from times");
                                                      echo json_encode($data);	
                              }
            break;   
            
        case 'editappYourself':
            $app = new Appointment();

                    if (Input::exists()) {

                                    $validate = new Validate();
                                    $validation = $validate->check($_POST, array(
                                                    //'test' => array(
                                                    //    'require' => true
                                                    // ),
                                                    'location' => array(
                                                        'require' => true
                                                    ),
                                                    'appointmentDate' => array(
                                                        'require' => true
                                                    )
                                    )); 

                                   if( $validation->passed() ){
                                    
                                       try{
                                         $update_status = $app->updateAppointment( Input::get('appid'), array( 
                                                   'loc_id' => Input::get('location'),
                                                   'lab_id' => "1",
                                                   'date' => trim ( trim( Input::get( 'appointmentDate' ) , 'undefined'), '-'),
                                                   'slot_id' => "1",
                                                   'doctor_name' => Input::get('docName'),
                                                   'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                                   'updated_date' => date("Y-m-d h:i:s")
                                                   ));
                                //delete previous test of users
                                if( $update_status ){
                                         $data = $app->_db->runQuery("
                                                              DELETE FROM
                                                                `appointment_tests` 
                                                              WHERE 
                                                                appointment_id = " .Input::get('appid') ) ;
                                              
                                         $test_status = $app->insertMultipleApppoinments( Input::get('appid'), Input::get('test'));
                                                  if( $test_status ) {
                                                      echo 1;
                                                  } else {
                                                      echo "Not UpdatedSuccessfully....";
                                                  }
                                              
                                              
                                }
                                            
                                } catch ( Exception $e ) {
                                           echo $e;
                                       }

                                   } else {
                                      // print_r( $validation->errors() );
                                        foreach ( $validate->errors() as $error) {
                                            echo $error.'<br>';
                                        }
                                   }
                                }
                    
            
        break;
            
        case 'editappOther': 
            $app = new Appointment();

                if (Input::exists()) {

                    $validate = new Validate();
                    $validation = $validate->check($_POST, array(
                        //'test' => array(
                        //    'require' => true
                        // ),
                        'r_location' => array(
                            'require' => true
                        ),
                        'appointmentDate' => array(
                            'require' => true
                        )
                        )); 

                            if( $validation->passed() ){

                                try{
                                  $update_status = $app->updateAppointment( Input::get('appidOther'), array( 
                                            'loc_id' => Input::get('r_location'),
                                            'lab_id' => "1",
                                            'date' => trim ( trim( Input::get( 'appointmentDate' ) , 'undefined'), '-'),
                                            'slot_id' => "1",
                                            'doctor_name' => Input::get('pDocName'),
                                            'updated_by' => Session::get( Config::get( 'session/session_name' ) ),
                                            'updated_date' => date("Y-m-d h:i:s")
                                            ));
                         //delete previous test of users
                         if( $update_status ){
                                //update Relation table
                                         $relationID = $app->_db->runQuery("
                                                              SELECT
                                                                   id
                                                              FROM
                                                                `relations` 
                                                              WHERE 
                                                                appointment_id = ".Input::get('appidOther'));
                                         
                                         $update_rel_status = $app->updateAppointmentOther( $relationID[0]['id'], array( 
                                                         'relation' => Input::get('pRelation'),
                                                         'r_name' => Input::get('pName'),
                                                         'r_email' => Input::get('pEmail'),
                                                         'r_phone' => Input::get('pContact'),
                                                         'r_gender' => Input::get('pGender'),
                                                         'r_doctor' => Input::get('pDocName'),
                                                         'add_street' => Input::get('add_street'),
                                                         'add_area' => Input::get('add_area'),
                                                         'add_landmark' => Input::get('add_landmark'),
                                                         'add_city' => Input::get('add_city'),
                                                         'add_zipcode' => Input::get('add_zipcode'),
                                                         'r_dob' => Input::get('pDob'),
                                                         'r_age' => Input::get('pAge'),                                               
                                                         'updted_by ' => Session::get( Config::get( 'session/session_name' ) ),
                                                         'updated_date ' => date("Y-m-d h:i:s")
                                            ));
                                
                                  $data = $app->_db->runQuery("
                                                       DELETE FROM
                                                         `appointment_tests` 
                                                       WHERE 
                                                         appointment_id = " .Input::get('appidOther') ) ;

                                  $test_status = $app->insertMultipleApppoinments( Input::get('appidOther'), Input::get('r_test'));
                                           if( $test_status ) {
                                               echo 1;
                                           } else {
                                               echo "Not UpdatedSuccessfully....";
                                           }


                         }

                         } catch ( Exception $e ) {
                                    echo $e;
                                }

                            } else {
                               // print_r( $validation->errors() );
                                 foreach ( $validate->errors() as $error) {
                                     echo $error.'<br>';
                                 }
                            }
                         }

            
        break;
            
            
        case 'cancelapp':
                            $app = new Appointment();
                            $user = new User();
                                     try{
                                          $status = $app->updateAppointment( Input::get('id'), array( 
                                                    'status_id' => Input::get('status'),
                                                    'updated_by' => Session::get( $user->_sessionName ),
                                                    'updated_date' => date("Y-m-d h:i:s")
                                                   ));

                                            if( $status ){
                                               echo 1;
                                            } else {
                                               echo 0;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }      
        break;
    }
}//end of if
