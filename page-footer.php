</div>
    <!-- /#wrapper -->
<div class="classFooterWrapper">
<div class="container">
 <footer>
    <div class="row">
    	<div class="col-lg-3">
			<div class="cuadro_intro_hover " style="background-color:#cccccc;">
				<p style="text-align:center; margin-top:20px;">
					<img src="images/before.jpg" class="img-responsive" alt="">
				</p>
				<div class="caption">
					<div class="blur"></div>
					<div class="caption-text">
						<h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Checkup Request</h3>
						<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor</p>
						<a class=" btn btn-default" href="#"><span class="glyphicon glyphicon-plus"> INFO</span></a>
					</div>
				</div>
			</div>				
	    </div>
       <div class="col-lg-3">
			<div class="cuadro_intro_hover " style="background-color:#cccccc;">
				<p style="text-align:center; margin-top:20px;">
					<img src="images/after.jpg" class="img-responsive" alt="">
				</p>
				<div class="caption">
					<div class="blur"></div>
					<div class="caption-text">
						<h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Checkup Report</h3>
						<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor</p>
						<a class=" btn btn-default" href="#"><span class="glyphicon glyphicon-plus"> INFO</span></a>
					</div>
				</div>
			</div>				
	    </div>
		
        <div class="col-lg-6 col-md-6">
            <h3>Contact:</h3>
            <p>We wish you a happy time here at Healthplease! In case you need help feel free to write us at <span style="background:#46b8da;padding:3px;">help@healthplease.com</span> or call us at <span style="background:#46b8da;padding:3px;">+91 8600080252</span>. We will be glad to help you.</p>
			
        	<h3>Follow:</h3>
			<a href="" id="gh" target="_blank" title="Twitter">
				<span class="fa-stack fa-lg">
				  <i class="fa fa-square-o fa-stack-2x"></i>
				  <i class="fa fa-twitter fa-stack-1x"></i>
			</span>
			Twitter</a>
			<a href="" id="gh" target="_blank" title="Twitter">
				<span class="fa-stack fa-lg">
				  <i class="fa fa-square-o fa-stack-2x"></i>
				  <i class="fa fa-facebook fa-stack-1x"></i>
			</span>
			Facebook</a>
			
		</div>
		</div>
    </footer>    
</div>
</div>    