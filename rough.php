<html>
<head>
<style type="text/css">
/*.cube{
  margin:0px auto;
  width:400px;
  height:100px;
  transform-style: preserve-3d;
  -webkit-transform-style: preserve-3d;
  transition:transform .5s ease-in;
  -webkit-transition:transform .5s ease-in;
}
.cube .design,.cube .development{
  box-shadow:0 0 10px rgba(0,0,0,0.5);
  -webkit-box-shadow:0 0 10px rgba(0,0,0,0.5);
  line-height:100px !important;
  color:#fff;
  font:400 40px 'oswald','fontAwesome',sans-serif;
  text-align:center;
  width:400px;
  height:100px;
}
.design{
  background:#00bfff;
  transform:translateZ(50px);
  -webkit-transform:translateZ(50px);
}
.development{
  background:#d30043;
  transform:rotateX(-90deg) translateZ(-50px);
  -webkit-transform:rotateX(-90deg) translateZ(-50px);
}
.cube:hover{
  transform:rotateX(-90deg);
  -webkit-transform:rotateX(-90deg);
}
*/

@import "lesshat";

@cube-width: 250px;
@cube-height: 100px;

/* CORE CSS */
body {
  .perspective(1000px);
}

/* Container box to set the sides relative to */
.cube {
	  width: @cube-width;
  	height: @cube-height;
	  .transition(all 250ms ease);
	  .transform-style(preserve-3d);
}

/* The two faces of the cube */
.default-state, .active-state {
  	height: @cube-height;
}

/* Position the faces */
.default-state {
	  .translateZ(@cube-height/2);
}

.flip-to-top .active-state {
	  .transform(rotateX(90deg) translateZ(@cube-height*1.5));
}

.flip-to-bottom .active-state {
	  .transform(rotateX(-90deg) translateZ(-50px));
}

/* Rotate the cube */
.cube.flip-to-top:hover {
	  .rotateX(-89deg);
}

.cube.flip-to-bottom:hover {
	  .rotateX(89deg);
}
/* END CORE CSS */


/* Demo styling */
body {
  font-family: 'Montserrat', sans-serif;
  font-weight: 400;
  margin: 70px;
  background: #f1f1f1;
}

h1 {
  font-size: 20px;
  text-align: center;
  margin-top: 40px;
}

.cube {
	  text-align: center;
  	margin: 0 auto;
}

.default-state, .active-state {
  background: #2ecc71;
  
  font-size: 16px;
  text-transform: uppercase;
  color: #fff;
  line-height: @cube-height;
  .transition(background 250ms ease);
}

.cube:hover .default-state {
  background: darken(#2ecc71, 7%);
}

.active-state {
  background: darken(#2ecc71, 7%);
}

#flipto {
  display: block;
  text-align: center;
  text-decoration: none;
  margin-top: 20px;
  color: #ccc;
}
</style>
</head>
<body>
	<div class="cube flip-to-top">
	  <div class="default-state">
		    <span>Hover</span>
	  </div>
	  <div class="active-state">
  		  <span>...and I flip</span>
  	</div>
</div><!-- 
<div class='cube'>
  <div class='design'>
   <i class='fa-paper-plane' style='margin-right:2px'></i> Design
  </div>
  <div class='development'>
    <i class='fa-code' style='margin-right:2px'></i>Development
  </div>
</div> -->
</body>