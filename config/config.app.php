 <?php



//! Session Time in Minutes
define('SESSION_TIME','10');

//! URL of the site.
define('SITE_URL','http://localhost:8080/healthplease/');

//! Name of the Brand of this package
define('CONFIG_BRAND_NAME', 'healthplease');


//! Name of the Organization 
define('CONFIG_ORG_NAME', 'Health Please');

//! Name of the Organization 
define('CONFIG_ORG_LOGO', '../images/LOGO-2.jpg');


//! A unique app key for multiple installation on same domain
define('CONFIG_APP_UNIQUE_KEY', " ");
//define('CONFIG_APP_UNIQUE_KEY', "leave");
//! Time for expiration of password reset link in Minutes
define('RESET_LINK_EXPIRATION_TIME', "120");

//! Time for expiration of "remember me" login in Days
define('REMEMBER_ME_EXPIRE_TIME', "7");

define('DATE_FORMAT', "d-m-Y");

define('EXTENDED_MODE', true);

/*
* Additional Constants concern to app.
*/

?>