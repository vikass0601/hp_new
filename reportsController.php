<?php
require_once 'core/init.php';
if( !empty( Input::get('action') )){

    switch ( Input::get('action') ) {
    
        case 'gen_report'  :
                        if ( Input::exists() ) {
                            
                            $validate = new Validate();
                            
                            if( 'n/a' != Input::get( 'fromdate' ) ) {
                                     $validation = $validate->check($_POST, array(
                                                    'todate' => array(
                                                          'require' => true
                                                                ),
                                                    'fromdate' => array(
                                                          'require' => true
                                                                ),
                                                ));                             
                                 
                            }
                            $validation = $validate->check( $_POST, array() );
                             if( $validation->passed() ){
                                       $report = new Reports();
                                       try{
                                          $report_status = $report->generateReportDateWise( Input::get( 'fromdate' ), Input::get( 'todate' ) );

                                     /**
                                      * Generation of report logic
                                      */    
                                          $report_html = '<table class="table table-bordered table-responsive">
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Email ID</th>
                                                                    <th>Contact no.</th>
                                                                    <th>Address</th>
                                                                    <th>Gender</th>
                                                                    <th>Test name</th>
                                                                    <th>Lab name</th>
                                                                    <th>Status</th>
                                                                    <th colspan="2" align="center">Action</th>
                                                                </tr>';
                                          
                                          if( false == $report_status ) {
                                              
                                             $report_html.= ' <tr><td colspan="6"> No Record Found .....     </td>      </tr></table>';
                                             echo $report_html;
                                             $report->_db->closeConnection();
                                             exit;
                                          } else {
                                                
                                               foreach( $report_status as $key => $value ) {
                                                   
                                                         $class  = ( 2 == $report_status[$key]['status_id'] ? 'alert alert-danger' : ( 3 == $report_status[$key]['status_id'] ?'alert alert-success': 'alert alert-info'));
                                                        
                                                         $report_html .= '<tr class="'. $class . '">';
                                                         $report_html .= '<td>'. $report_status[$key]['name'] .'</td>';
                                                         $report_html .= '<td>'. $report_status[$key]['username'] .'</td>';
                                                         $report_html .= '<td>'. $report_status[$key]['contact'] .'</td>';
                                                         $report_html .= '<td>'. $report_status[$key]['address'] .'</td>';
                                                         if( 1 == $report_status[$key]['gender']) $gender = 'Male'; else $gender = 'Female';
                                                         $report_html .= '<td>'. $gender  .'</td>';
                                                         $report_html .= '<td>'. $report_status[$key]['test_name']  .'</td>';
                                                         $report_html .= '<td>'. $report_status[$key]['lab_name'] .'</td>';    
                                                         $report_html .= '<td>'. $report_status[$key]['status'] .'</td>';                                                         
                                                         $report_html .= '<td colspan="2">'
                                                                 . '<a href="javascript:void(0)" class="btn btn-success btn-mini js-processapp" data-value="'.$report_status[$key]['id']. '" data-status="3"><i class="glyphicon glyphicon-ok"></i></a>'
                                                                 . '<a href="javascript:void(0)" class="btn btn-danger btn-mini js-processapp" data-value="'.$report_status[$key]['id']. '" data-status="2"><i class="glyphicon glyphicon-remove"></i></a>'
                                                                 . '</td></tr>';
//                                                   $report_status[$key]['name'];
                                               }
                                               $report_html .= "</table>";
                                               echo $report_html;
                                               $report->_db->closeConnection();
                                              //to generate pdf logic 
                                               if( 'get_excel' == Input::get( 'type_of_report' ) ) {
                                                    generateExcelReport( $report_status );
                                               }
                                            }
                                       } catch ( Exception $e ) {
                                           echo $e;
                                       }
                             }

                        }
                    break;
                    
        case 'processapp':
                            if ( Input::exists() ) {
                            $app = new Appointment();
                            $user = new User();
                                     try{
                                            $status = $app->updateAppointment( Input::get('id'), array( 
                                                    'status_id' => Input::get('status'),
                                                    'updated_by' => Session::get( $user->_sessionName ),
                                                    'updated_date' => date("Y-m-d h:i:s")
                                            ));

                                            if( $status ){
                                               echo 1;
                                               $app->_db->closeConnection();
                                            } else {
                                               echo 0;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       } 
                                
                            }
            break;
            
        case 'load_summary' : 
                            $app = new Appointment();
                            $user = new User();
                                     try{
                                         $strSql = 'SELECT 
                                                        status_id,
                                                        sum(a.pending) as pen,
                                                        sum(a.cancelled) as can, 
                                                        sum(a.completed) as cmp, 
                                                        sum(a.total) as total,
                                                        ( SELECT 
                                                               count(*)
                                                          FROM 
                                                               `appointments`
                                                          WHERE 
                                                               date = "' .Input::get('date'). '"
                                                          ORDER BY 
                                                                `id` DESC  ) as today
                                                    FROM 
                                                        (
                                                            SELECT 
                                                                status_id,
                                                                sum( case When status_id = 1 then 1 else 0 end) as pending,
                                                                sum( case when status_id = 2 then 1 else 0 end) as cancelled,
                                                                sum( case when status_id = 3 then 1 else 0 end) as completed,
                                                                sum( case when status_id IN ( 1, 2, 3 ) then 1 else 0 end) as total
                                                            FROM 
                                                                appointments
                                                            GROUP BY 
                                                                status_id
                                                        ) a
                                                    LIMIT 
                                                        1';
                                        $status = $app->_db->runQuery( $strSql );
                                            
                                            if( $status ){
                                               echo json_encode( $status );
                                               $app->_db->closeConnection();
                                            } else {
                                               echo 0;
                                            }

                                       } catch ( Exception $e ) {
                                           echo $e;
                                       } 
                           
            break;     
    }
    
} 
    function generateExcelReport( $reportData ) {
            spl_autoload_unregister('myAutoload');
            include dirname(__FILE__) . '/includes/library/external/excel/PHPExcel.php';
               // Create new PHPExcel object
               $objPHPExcel = new PHPExcel();

               // Set document properties
               $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                                                        ->setLastModifiedBy("Maarten Balliauw")
                                                                        ->setTitle("Office 2007 XLSX Test Document")
                                                                        ->setSubject("Office 2007 XLSX Test Document")
                                                                        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                                                        ->setKeywords("office 2007 openxml php")
                                                                        ->setCategory("Test result file");


               // Main HEading
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'HealthPlease');
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'List of appointments');
            
            //Data heading
            
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Name');
            $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Email ID');
            $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Contact No.');
            $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Address');
            $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Gender');
            $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Test Name');
            $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Test Charge');
            $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Lab Name');
            $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Status');
           
      // Set column widths
       
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

    // Set fonts and style

    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(25);
    $objPHPExcel->getActiveSheet()->getStyle('A5:I5')->getFont()->setBold(true)->setSize(10);
    //$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setSize(18);
    //$objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->setBold(true);

    //print excel data here
    $rowCount = 6;
    foreach( $reportData as $key => $value ) {
        
            $objPHPExcel->getActiveSheet()->setCellValue( 'A'.$rowCount, $reportData[$key]['name'] );
            $objPHPExcel->getActiveSheet()->setCellValue( 'B'.$rowCount, $reportData[$key]['username'] );
            $objPHPExcel->getActiveSheet()->setCellValue( 'C'.$rowCount, $reportData[$key]['contact'] );
            $objPHPExcel->getActiveSheet()->setCellValue( 'D'.$rowCount, $reportData[$key]['address'] );
            if( 1 == $reportData[$key]['gender']) $gender = 'Male'; else $gender = 'Female';
            $objPHPExcel->getActiveSheet()->setCellValue( 'E'.$rowCount, $gender );
            $objPHPExcel->getActiveSheet()->setCellValue( 'F'.$rowCount, $reportData[$key]['test_name'] );
            $objPHPExcel->getActiveSheet()->setCellValue( 'G'.$rowCount, $reportData[$key]['test_charge'] );
            $objPHPExcel->getActiveSheet()->setCellValue( 'H'.$rowCount, $reportData[$key]['lab_name'] );
            $objPHPExcel->getActiveSheet()->setCellValue( 'I'.$rowCount, $reportData[$key]['status'] );

                    $rowCount++;
    }
    // Set header and footer. When no different headers for odd/even are used, odd header is assumed.

    $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&BPersonal cash register&RPrinted on &D');
    $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');


    // Set page orientation and size

    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
    $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

            
               // Rename worksheet
               $objPHPExcel->getActiveSheet()->setTitle('HP_'.date('Y-m-d'));


               // Set active sheet index to the first sheet, so Excel opens this as the first sheet
               $objPHPExcel->setActiveSheetIndex(0);


               // Redirect output to a client’s web browser (Excel5)
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment;filename="vikas.xls"');
               header('Cache-Control: max-age=0');
               // If you're serving to IE 9, then the following may be needed
               header('Cache-Control: max-age=1');

               // If you're serving to IE over SSL, then the following may be needed
               header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
               header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
               header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
               header ('Pragma: public'); // HTTP/1.0

               $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
               $fileName = 'HealthPlease_'.date('Y-m-d').'.xls';
               $objWriter->save( 'downloads/'.$fileName );
               exit;

        
    }

?>